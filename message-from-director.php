<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Director's Message</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Director's Message</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                                    <div class="sec-title">
                        <h2>Mr. Vittal Bhandary </h2>
                    </div>
                    <div class="text">
                        <p>
                            <strong>Vittal had a vision of nurturing great values in children through a conscious,
                                harmonious and fun-filled early childhood education system. With that discernment and
                                passion, he created the Little Elly chain of preschools.
                            </strong>
                        </p>
                        <p>
                            Vittal and his team have spent twenty five years of relentless focus and concerted effort
                            in developing a profound understanding of the dynamics and diverse aspects of early
                            childhood education and care. Leading an accomplished team of professionals, he has
                            developed a successful, recognized and respected brand and organization in this market
                            segment in India. The vast and ever-growing number of preschools and childcare environments
                            operating under the Little Elly flagship bear testament to this fact.
                        </p>
                        <p>
                            Vittal’s enterprising ideas, perseverance and steadfast belief in this concept, has led to
                            successful diversifications of the business into areas of children’s publications, creation
                            of learning aids and providing pedagogic support, early schooling, daycare and corporate
                            affiliate programs.
                        </p>
                    </div>
                    
                </div>
            </div>

            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1 no-hover"><img src="images/resource/vital.jpg" alt=""></figure>
                        <figure class="image-1 on-hover"><img src="images/resource/vital-1.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="sec-title">
                        <h2><span> (Founder And Managing Director)</span></h2>
                    </div>
            </div>
        </div>
    </div>
    <div class="auto-container pad-top">
        <div class="row clearfix">
            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1 no-hover"><img src="images/resource/preeti.jpg" alt=""></figure>
                        <figure class="image-1 on-hover"><img src="images/resource/preeti-1.jpg" alt=""></figure>
                    </div>
                </div>
                                    <div class="sec-title">
                        <h2><span> (Co-Founder And Curriculum Director)</span></h2>
                    </div>
            </div>
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2>Mrs. Preeti Bhandary</h2>
                    </div>
                    <div class="text">
                        <p>
                            <strong>
                                Preeti is a veteran educationist and an entrepreneur. She has over 18 years of
                                experience in early childhood education and care. Her views on working with young minds
                                are radical, progressive and dynamic. Preeti considers each child as an individual.
                                She has over 18 years of experience in early childhood education and care. Her views on
                                working with young minds
                                are radical, progressive and dynamic.
                            </strong>
                        </p>
                        <p>
                            Preeti considers each child as an individual. She is passionate about unearthing and
                            cultivating a child’s potential through creative, stimulating and exclusive channels. She
                            has spent a large part of her career evolving unrivaled curriculums to foster the
                            individual needs of a child to mould him/her into an exclusive person. In this way, she has
                            guided the growth of thousands of children with distinguished determination and
                            effectuation. Her eminent standards of excellence have ensured that Little Elly is graded
                            among the best in pre-schools in India.
                        </p>
                        <p>
                            Preeti is also highly respected and regarded in the industry for being a patient and
                            encouraging mentor to those who share her insight of the vision, mission and philosophy of
                            Little Elly.
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>