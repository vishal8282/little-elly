<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Holiday Camp</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Holiday Camp</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Toller Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="image-box">
            <div class="image wow fadeIn"><img src="images/resource/course-5.jpg" alt=""></div>
        </div>
        <div class="lower-content">
            <h3>The Holiday Camp program is suitable for children of 3-5 years.</h3>

            <div class="text">
                <p>
                    School holidays are a time for fun and frolic. But, when parents are working or otherwise occupied,
                    it can be quite a task at hand to keep the child constructively engaged. A good set of interesting,
                    extra-curricular activities making the best use of the child’s time gives him/her a meaningful
                    pursuit in the holiday season.
                </p>
                <p>
                    Little Elly offers holiday camps for children from 2 years to10 years. These programs are designed
                    in a way that the child explores, interacts with others and makes friends. This exposure may also
                    initiate him/her into a hobby or skill that can be explored beyond the camp.
                </p>
                <p>
                    Every child is unique and possesses varied interests. The camps provide a platform for him/her to
                    grow and to be nurtured. Different activities including craft, dancing, storytelling, science
                    projects and sports are covered. To help in the child’s overall development and for better
                    interaction and learning, theme based activities are designed around diverse interests. All
                    exercises are carried out in small age-appropriate groups.
                    For more information or list of activities planned for a holiday camp, please contact your nearest
                    Little Elly centre or refer to our website.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>