<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Playgroup</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Playgroup</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Toller Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="image-box">
            <div class="image wow fadeIn"><img src="images/resource/course-2.jpg" alt=""></div>
        </div>
        <div class="lower-content">
            <h3>The Playgroup program is suitable for children of 2+ years.</h3>

            <div class="text">
                <p>
                    Little Elly’s playgroup program is an extension of home. Often, this is the first time the child
                    comes away from the comfort of familiar people and surroundings at home and steps into an unknown
                    world. At Little Elly, we do our best to make the transition smooth and hassle-free. We strive to
                    provide the child with an environment that will jump-start his/her developmental process. We
                    succeed by using age-appropriate materials and help the child acquire knowledge and understanding
                    of practical concepts through innovative learning and play. This program helps the child develop
                    his/her latent creative, language and maths skills.
                </p>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span> Our Approach</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Ensuring that the child cultivates a life-long passion for learning</li>
                                <li><i class="fa fa-angle-left"></i>Building a sense of confidence, self esteem and strong life-skills</li>
                                <li><i class="fa fa-angle-left"></i>Encouraging and supporting social and communication skills</li>
                                <li><i class="fa fa-angle-left"></i>Developing a lasting love of reading and writing</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Key areas covered in the Playgroup
                            curriculum</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Learning by play and exploration</li>
                                <li><i class="fa fa-angle-left"></i>Building communication and social skills</li>
                                <li><i class="fa fa-angle-left"></i>Encouraging positive peer interaction</li>
                                <li><i class="fa fa-angle-left"></i>Theme based learning</li>
                                <li><i class="fa fa-angle-left"></i>Understanding of mathematical concepts</li>
                                <li><i class="fa fa-angle-left"></i>Equipped with Language Skills</li>
                                <li><i class="fa fa-angle-left"></i>Equipping with language skills</li>
                                <li><i class="fa fa-angle-left"></i>Stimulating fine and gross motor co-ordination skills</li>
                                <li><i class="fa fa-angle-left"></i>Promoting cognitive development</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Activities Involved</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Creative Play</li>
                                <li><i class="fa fa-angle-left"></i>Sensory Play</li>
                                <li><i class="fa fa-angle-left"></i>Reading</li>
                                <li><i class="fa fa-angle-left"></i>Water Play , Sand Play</li>
                                <li><i class="fa fa-angle-left"></i>Festivals and special day celebration.</li>
                                <li><i class="fa fa-angle-left"></i>Field Trips to the park, supermarket etc</li>
                                <li><i class="fa fa-angle-left"></i>Puppet shows and skits</li>
                                <li><i class="fa fa-angle-left"></i>Free Play with Technology tools</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>