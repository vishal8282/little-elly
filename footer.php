<!-- Main Footer -->
<footer class="main-footer">
    <!-- Footer upper -->
    <div class="footer-upper">
        <div class="inner-container">
            <!--Widgets Section-->
            <div class="widgets-section container">
                <div class="row clearfix">
                    <!--Footer Column-->
                    <div class="footer-column col-lg-3 col-md-12 col-sm-12">
                        <div class="footer-widget post-widget">
                            <div class="logo-box">
                                <figure><img src="images/logo.png" alt="footer logo"></figure>
                            </div>
                            <div class="widget-content">
                                <!-- Recent Post -->
                                <article class="post">
                                    <h4>
                                        <a href="blog-single-2.html">
                                            Little Elly is built around the strong belief that the childhood of
                                            a little one is a precious.
                                        </a>
                                    </h4>
                                    <ul class="social-links">
                                        <li class="facebook">
                                            <a href="#">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li class="google-plus">
                                            <a href="">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </li>
                                        <li class="linkedin">
                                            <a href="">
                                                <span class="fa fa-linkedin"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </article>
                            </div>
                        </div>
                    </div>

                    <!--Footer Column-->
                    <div class="footer-column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget post-widget">
                            <h2 class="widget-title">Quick Links</h2>
                            <div class="widget-content">
                                <!-- Recent Post -->
                                <article class="post quick_links">
                                    <ul>
                                        <li>
                                            <a href="blog-single-2.html">Assessment</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Child Behaviour Policy</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Curriculum Framework</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Franchise Opportunity</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Faq</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Wiziq Login</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">ERP Login</a>
                                        </li>
                                    </ul>
                                </article>
                            </div>
                        </div>
                    </div>

                    <!--Footer Column-->
                    <div class="footer-column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget post-widget">
                            <h2 class="widget-title">Other Links</h2>
                            <div class="widget-content">
                                <!-- Recent Post -->
                                <article class="post quick_links">
                                    <ul>
                                        <li>
                                            <a href="blog-single-2.html">Teacher Application Form</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Admission Enquiry</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Admission Procedure</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Facilities & Faculty</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Careers</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Locate Centers</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Enquire Now</a>
                                        </li>
                                    </ul>
                                </article>
                            </div>
                        </div>
                    </div>

                    <!-- Footer Column -->
                    <div class="footer-column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget post-widget">
                            <h2 class="widget-title">About Us</h2>
                            <div class="widget-content">
                                <!-- Recent Post -->
                                <article class="post quick_links">
                                    <ul>
                                        <li>
                                            <a href="blog-single-2.html">Origin & History</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Our Vision & Mission</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Philosophy</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Awards & recognition</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Little Elly Ventures</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Corporate Social Responsibility</a>
                                        </li>
                                        <li>
                                            <a href="blog-single-2.html">Our Directors</a>
                                        </li>
                                    </ul>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!-- end -->
                </div>
            </div>
            <!-- Footer Bottom -->
            <div class="auto-container">
                <div class="copyright-text clearfix">
                    <div class="text">© Copyright 2018 <a href="index.html">Little Elly</a>. All Rights
                        Reserved</div>
                    <div class="link">
                        <a href="#"> Digital Partner : 3m Digital</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
<!-- End Main Footer -->
</div>
<!--End pagewrapper-->
<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!--Revolution Slider-->
<script src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="js/main-slider-script.js"></script>
<!--End Revolution Slider-->
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/countdown.js"></script>
<script src="js/isotope.js"></script>
<script src="js/validate.js"></script>
<script src="js/appear.js"></script>
<script src="js/script.js"></script>
<!--Google Map APi Key-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDEDCvigaVo8FsnxCnH0-vCcWrirGaoKBA"></script>
<script src="js/map-script.js"></script>
<!--End Google Map APi-->
</body>

</html>