<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Child Behaviour Policy</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Child Behaviour Policy</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2>The following general behaviour management strategies are frequently <span>used at
                                Little Elly:</span></h2>
                    </div>
                    <ul class="list-style-two">
                        <li>Positive statements are made to encourage the child to do the right thing. For
                            example:“Turn the pages carefully,” rather than “Don’t tear the book!”</li>
                        <li>Positive redirection is used to clarify when and where certain behaviour is
                            acceptable. i.e., “We will walk “instead of “No running inside!”</li>
                        <li>Feelings are validated. Children are guided to socially acceptable means of
                            articulating anger and frustration such as verbal expression, pounding play dough
                            or a pillow.</li>
                        <li>The “deed” is separated from the “doer”. This relays the message that “I like you
                            and accept you unconditionally. However, I do not like what you did.”
                        </li>
                        <li>Good behaviour that we want to see continuously is reinforced. Examples of positive
                            reinforcers include a smile, sticker charts, expressing thank you and other words
                            of encouragement, such as “Let’s try it together.”</li>
                    </ul>

                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/cbp.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>