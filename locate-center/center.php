<?php
// see index.php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Play School in Bangalore, Top Preschools, Nursery, Kindergarten – Little Elly</title>
    <!-- Stylesheets -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../plugins/revolution/css/settings.css" rel="stylesheet" type="text/css">
    <link href="../plugins/revolution/css/layers.css" rel="stylesheet" type="text/css">
    <link href="../plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link rel="shortcut icon" href="../images/favicon.png" type="image/x-icon">
    <link rel="icon" href="../images/favicon.png" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>

<style>
    .mapouter{
        width: 100% !important;
        /*height: 100px !important;*/
    }
    .gmap_canvas, .gmap_canvas iframe{
        width: 100% !important;
    }
    
</style>


<body>
    <div class="page-wrapper">
        <!-- Preloader -->
        <div class="preloader"></div>

        <!-- Main Header-->
        <header class="main-header">
            <!--Header Top-->
            <div class="header-top">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <div class="top-left">
                            <ul class="contact-list clearfix">
                                <li><a href="#">help@littleelly.com</a></li>
                                <li>Call Us : 7676 38 0000</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Header Top -->

            <!-- Header Upper -->
            <div class="header-upper">
                <div class="auto-container">
                    <div class="clearfix">

                        <div class="logo-outer">
                            <div class="logo"><a href="index.php"><img src="../images/logo.png" alt="logo" title=""></a></div>
                        </div>

                        <div class="upper-right clearfix">
                            <ul>
                                <li class="current">
                                    <a class="theme-btn btn-style-one" href="#about-us">About us</a>
                                </li>
                                <li>
                                    <a class="theme-btn btn-style-one" href="#our-program">Our Program</a>
                                </li>
                                <li>
                                    <a class="theme-btn btn-style-one" href="#our-gallery">Our Gallery</a>
                                </li>
                                <li>
                                    <a class="theme-btn btn-style-one" href="#testimonail">Testimonial</a>
                                </li>
                                <li>
                                    <a class="theme-btn btn-style-one" href="#center-head">Center Head</a>
                                </li>
                                <li>
                                    <a class="theme-btn btn-style-one" href="#locate-us">Locate Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!--End Header Upper-->
            <!-- Sticky Header -->
            <div class="sticky-header">
                <div class="auto-container clearfix">
                    <!--Logo-->
                    <div class="logo pull-left">
                        <a href="index.php" title=""><img src="../images/logo-small.png" alt="" title=""></a>
                    </div>
                    <!--Right Col-->
                    <div class="pull-right">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-collapse show collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li class="current">
                                        <a href="#about-us">About us</a>
                                    </li>
                                    <li>
                                        <a href="#our-program">Our Program</a>
                                    </li>
                                    <li>
                                        <a href="#our-gallery">Our Gallery</a>
                                    </li>
                                    <li>
                                        <a href="#testimonail">Testimonial</a>
                                    </li>
                                    <li>
                                        <a href="#center-head">Center Head</a>
                                    </li>
                                    <li>
                                        <a href="#locate-us">Locate Us</a>
                                    </li>
                                </ul>
                            </div>
                        </nav><!-- Main Menu End-->
                    </div>
                </div>
            </div>
        </header>
        <!--End Main Header -->
        <!--Main Slider-->
        <section class="main-slider style-two">
            <div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_two_wrapper" data-source="gallery">
                <div class="rev_slider fullwidthabanner" id="rev_slider_two" data-version="5.4.1" data-height="600">
                    <ul>
                        <!-- Slide 1 -->
                        <li data-description="Slide Description" data-easein="default" data-easeout="default"
                            data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0"
                            data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1=""
                            data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                            data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off"
                            data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title"
                            data-transition="parallaxvertical">

                            <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center"
                                data-bgrepeat="no-repeat" data-no-retina="" src="/uploads/<?php echo $center->image1 ?>">

                            <div class="tp-caption tp-resizeme" data-paddingbottom="[12,0,0,0]" data-paddingleft="[12,0,0,0]"
                                data-paddingright="[12,0,0,0]" data-paddingtop="[12,0,0,0]" data-responsive_offset="on"
                                data-type="text" data-textalign="center" data-height="none" data-whitespace="nowrap"
                                data-width="none" data-hoffset="['0','0','0','0']" data-voffset="['70','0','0','0']"
                                data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']"
                                data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                                <div class="content-box">
                                    <h3>Welcome To Our Little Elly</h3>
                                    <h1><?php echo $center->name ?></h1>
                                    <a class="theme-btn btn-style-one enroll-button">EnRoll Now</a>
                                </div>
                            </div>
                        </li>

                        <!-- Slide 2 -->
                       <!--  <li data-description="Slide Description" data-easein="default" data-easeout="default"
                            data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0"
                            data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1=""
                            data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                            data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off"
                            data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title"
                            data-transition="parallaxvertical">

                            <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center"
                                data-bgrepeat="no-repeat" data-no-retina="" src="../images/main-slider/image-4.jpg">

                            <div class="tp-caption tp-resizeme" data-paddingbottom="[12,0,0,0]" data-paddingleft="[12,0,0,0]"
                                data-paddingright="[12,0,0,0]" data-paddingtop="[12,0,0,0]" data-responsive_offset="on"
                                data-type="text" data-textalign="center" data-height="none" data-whitespace="nowrap"
                                data-width="none" data-hoffset="['0','0','0','0']" data-voffset="['70','0','0','0']"
                                data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']"
                                data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                                <div class="content-box">
                                    <h3>The Best Theme For</h3>
                                    <h1>Education</h1>
                                    <a href="about.html" class="theme-btn btn-style-one">EnRoll Now</a>
                                </div>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </section>
        <!--End Main Slider-->

        <!-- Intro Section -->
        <section class="intro-section" id="about-us">
            <div class="auto-container">
                <div class="content-box">
                    <h2>About Us</h2>
                    <div class="text"><?php echo $center->description ?></div>
                </div>
            </div>
        </section>
        <!-- Intro Section -->

        <!-- Services Section -->
        <section class="services-section style-two" id="our-program">
            <div class="auto-container">
                <div class="sec-title text-center blue-devider">
                    <h2>Our <span>Programs</span></h2>
                </div>
                <div class="row clearfix">
                    <!-- Service Block -->
                    <?php if(in_array('todler', $programs)){ ?>
                    <div class="service-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                        <div class="inner-box">
                            <a href="../toddler.php">
                                <div class="icon-box"><img src="../images/resource/event-1.jpg" alt=""></div>
                                <h3><a href="../toddler.php">Toddler</a></h3>
                                <div class="text">The Toddler Program is suitable for children of 1+ years</div>
                                <div class="link-box"><a href="../toddler.php">Read More</a></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>

                    <!-- Service Block -->
                    <?php if(in_array('playgroup', $programs)){ ?>
                    <div class="service-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms">
                        <div class="inner-box">
                            <a href="../playgroup.php">
                                <div class="icon-box"><img src="../images/resource/event-2.jpg" alt=""></div>
                                <h3><a href="../playgroup.php">Playgroup</a></h3>
                                <div class="text">The Playgroup Program is suitable for children of 2+ years</div>
                                <div class="link-box"><a href="../playgroup.php">Read More</a></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>

                    <!-- Service Block -->
                    <?php if(in_array('nursery', $programs)){ ?>
                    <div class="service-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                        <div class="inner-box">
                            <a href="../nursery.php">
                                <div class="icon-box"><img src="../images/resource/event-3.jpg" alt=""></div>
                                <h3><a href="../nursery.php">Nursery</a></h3>
                                <div class="text"> The Nursery Program is suitable for children of 3+ years</div>
                                <div class="link-box"><a href="../nursery.php">Read More</a></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>

                    <!-- Service Block -->
                    <?php if(in_array('kindergarten', $programs)){ ?>
                    <div class="service-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                        <div class="inner-box">
                            <a href="../kindergarten.php">
                                <div class="icon-box"><img src="../images/resource/event-2.jpg" alt=""></div>
                                <h3><a href="../kindergarten.php">Kindergarten</a></h3>
                                <div class="text">The Kindergarten program is suitable for children of 3-5 years</div>
                                <div class="link-box"><a href="../kindergarten.php">Read More</a></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>

                    <!-- Service Block -->
                    <?php if(in_array('holiday_camps', $programs)){ ?>
                    <div class="service-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms">
                        <div class="inner-box">
                            <a href="../holiday-camp.php">
                                <div class="icon-box"><img src="../images/resource/event-5.jpg" alt=""></div>
                                <h3><a href="../holiday-camp.php">Holiday Camps</a></h3>
                                <div class="text">Holiday Camps for children from 2 years to 10 years</div>
                                <div class="link-box"><a href="../holiday-camp.php">Read More</a></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </section>
        <!--End Services Section -->

        <!--Gallery Section-->
        <section class="gallery-section" id="our-gallery">
            <div class="auto-container">
                <div class="sec-title text-center pink-devider">
                    <h2>Our <span>Gallery</span></h2>
                </div>
                <!--Galery-->
                <!-- <div class="sortable-masonry">
                    <div class="filters clearfix">
                        <ul class="filter-tabs filter-btns clearfix">
                            <li class="active filter" data-role="button" data-filter=".all">All</li>
                            <li class="filter" data-role="button" data-filter=".activities">Activities</li>
                            <li class="filter" data-role="button" data-filter=".annualday">Annual Day</li>
                            <li class="filter" data-role="button" data-filter=".celebration">Celebration</li>
                            <li class="filter" data-role="button" data-filter=".classroom">Classroom</li>
                            <li class="filter" data-role="button" data-filter=".events">Events</li>
                            <li class="filter" data-role="button" data-filter=".franchise">Franchise Meet</li>
                            <li class="filter" data-role="button" data-filter=".health">Health camp</li>
                            <li class="filter" data-role="button" data-filter=".playgound">Playgound</li>
                        </ul>
                    </div> -->

                    <div class="items-container row clearfix">

                        <!--Project Block Two-->
                        <div class="project-block masonry-item game annualday franchise classroom events celebration all col-lg-4 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="image">
                                    <img onerror="this.style.display='none'"  src="/uploads/<?php echo $center->image6 ?>" alt="" />
                                    <div class="overlay-box"><a href="/uploads/<?php echo $center->image6 ?>" style="background-image: url(/uploads/<?php echo $center->image6 ?>);"
                                            class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                                </div>
                            </div>
                        </div>
                        <!--Project Block Two-->
                        <div class="project-block masonry-item game annualday health events activities classroom all col-lg-4 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="image">
                                    <img onerror="this.style.display='none'" src="/uploads/<?php echo $center->image7 ?>" alt="" />
                                    <div class="overlay-box"><a href="/uploads/<?php echo $center->image7 ?>" style="background-image: url(/uploads/<?php echo $center->image7 ?>);"
                                            class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                                </div>
                            </div>
                        </div>
                        <!--Project Block Two-->
                        <div class="project-block masonry-item classroom franchise annualday celebration game all col-lg-4 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="image">
                                    <img onerror="this.style.display='none'" src="/uploads/<?php echo $center->image8 ?>" alt="" />
                                    <div class="overlay-box"><a href="/uploads/<?php echo $center->image8 ?>" style="background-image: url(/uploads/<?php echo $center->image8 ?>);"
                                            class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                                </div>
                            </div>
                        </div>
                        <!--Project Block Two-->
                        <div class="project-block masonry-item events game annualday Playgound activities events all col-lg-8 col-md-12 col-sm-12">
                            <div class="inner-box">
                                <div class="image">
                                    <img onerror="this.style.display='none'" src="/uploads/<?php echo $center->image9 ?>" alt="" />
                                    <div class="overlay-box"><a href="/uploads/<?php echo $center->image9 ?>" style="background-image: url(/uploads/<?php echo $center->image9 ?>);"
                                            class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Gallery Section-->



        <!-- Testimonial Section -->
        <section class="testimonial-section" style="background-image: url(../images/background/5.jpg);" id="testimonial">
            <div class="video-block">
                <video width="100%" height="100%" autoplay="autoplay" loop="loop" muted="">
                    <source src="../images/testimonial.mp4" type="video/webm">
                </video>
                <div class="video-overlay"></div>
            </div>
            <div class="auto-container">
                <div class="inner-container">
                    <div class="single-item-carousel owl-carousel owl-theme">
                        <!-- Testimonial Block Two -->
                        <div class="testimonial-block-two">
                            <div class="inner-box">
                                <div class="thumb"><img onerror="this.style.display='none'" src="/uploads/<?php echo $center->image4 ?>" alt=""></div>
                                <span class="icon flaticon-right-quotes-symbol"></span>
                                <div class="text"><?php echo $center->test1description ?></div>
                                <div class="info">
                                    <div class="name"><?php echo $center->test1name ?>
                                    <span class="designation"></span></div>
                                </div>
                            </div>
                        </div>

                        <!-- Testimonial Block Two -->
                        <div class="testimonial-block-two">
                            <div class="inner-box">
                                <div class="thumb"><img onerror="this.style.display='none'" src="/uploads/<?php echo $center->image5 ?>" alt=""></div>
                                <span class="icon flaticon-right-quotes-symbol"></span>
                                <div class="text"><?php echo $center->test2description ?></div>
                                <div class="info">
                                    <div class="name"><?php echo $center->test2name ?>
                                    <span class="designation">  </span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Testimonial Section -->

        <!-- Pricing Section -->
        <section class="pricing-section" id="center-head">
            <div class="auto-container">
                <div class="sec-title text-center blue-devider">
                    <h2>Center <span>Head</span></h2>
                </div>
                <div class="row clearfix">
                    <!-- Pricing Table -->
                    <div class="pricing-table col-lg-12 col-md-12 col-sm-12 wow fadeIn">
                        <div class="inner-box">
                            <div class="price-titles">
                                <div class="title">
                                    <h3>Center Head</h3>
                                    <span><?php echo $center->name ?></span>
                                </div>
                                <div class="price">
                                    <h2><?php echo $center->center_head ?></h2>
                                </div>
                            </div>
                            <div class="price-image">
                                <img src="/uploads/<?php echo $center->image2 ?>" alt="">
                            </div>
                            <div class="table-content">
                                <!-- <h4>Mrs. Jyoti Khanna</h4> -->
                                <ul>
                                    <?php echo $center->center_head_description ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Pricing Table -->
                    <div class="pricing-table col-lg-12 col-md-12 col-sm-12 wow fadeIn" data-wow-delay="600ms">
                        <div class="inner-box">
                            <div class="price-titles">
                                <div class="title">
                                    <h3>Coordinator </h3>
                                    <span><?php echo $center->name ?></span>
                                </div>
                                <div class="price">
                                    <h2><?php echo $center->center_coordinator ?></h2>
                                </div>
                            </div>
                            <div class="price-image">
                                <img src="/uploads/<?php echo $center->image3 ?>" alt="">
                            </div>
                            <div class="table-content">
                                <!-- <h4>Multimedia Class</h4> -->
                                <ul>
                                    <li><?php echo $center->center_coordinator_description ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--End Pricing Section -->

        <!-- Call To Action -->
        <section class="call-to-action style-two">
            <div class="anim-icons">
                <span class="icon icon-cloud wow shake"></span>
            </div>
            <div class="auto-container address-us-sect">
                <div class="title-column">
                    <h2>Our Address</h2>
                </div>
            </div>

            <div class="auto-container address-us">
                <div class="outer-box clearfix">
                    <div class="title-column">
                        <!-- <h2>Our Address</h2> -->
                        <h3><?php echo $center->address ?></h3>
                    </div>

                    <div class="btn-column">
                        <div class="btn-box">
                            <a href="contact.html" class="theme-btn btn-style-six">Call us - <?php echo $center->phone ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Call To Action -->

        <!-- Locate us center -->
        <section class="map-section pb-5">
<!--             <div class="map-outer">
                <div class="map-canvas" data-zoom="12" data-lat="12.912403" data-lng="77.637714" data-type="roadmap"
                    data-hue="#ffc400" data-title="Little Elly" data-icon-path="images/icons/map-marker.png"
                    data-content="Learning Edge India Pvt. Ltd.<br><a href='mailto:support@littleelly.com'> Support@littleelly.com</a>">
                </div>
            </div>
 -->            

        <?php echo $center->map ?>

        </section>

        <!-- End of locate us -->

    </div>
    <!--End pagewrapper-->
    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>
    <script src="../js/jquery.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!--Revolution Slider-->
    <script src="../plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="../plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="../plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="../js/main-slider-script.js"></script>
    <!--End Revolution Slider-->
    <script src="../js/jquery.fancybox.js"></script>
    <script src="../js/owl.js"></script>
    <script src="../js/wow.js"></script>
    <script src="../js/countdown.js"></script>
    <script src="../js/isotope.js"></script>
    <script src="../js/validate.js"></script>
    <script src="../js/appear.js"></script>
    <script src="../js/script.js"></script>
    <!--Google Map APi Key-->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyDEDCvigaVo8FsnxCnH0-vCcWrirGaoKBA"></script>
    <script src="../js/map-script.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {

                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),

                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)

        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');



        ga('create', 'UA-68948635-1', 'auto');

        ga('send', 'pageview');
    </script>
    <!--End Google Map APi-->

    <script type="text/javascript">
        $('.enroll-button').click(function () {
            $('html, body').animate({scrollTop:6000}, 'slow');
            return false;
        });
    </script>
</body>



</html>