<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Awards & Recognition</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Awards & Recognition</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Indian Education Award 2015</a></h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Innovation In Early Learning/ Child Development</li>
                            </ul>
                            <div class="text">
                                The award recognizes early learning schools that have implemented programs/curriculum
                                to advance quality early educational opportunities for children.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Top 100 Franchise in India</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>2014 & 2015</li>
                            </ul>
                            <div class="text">
                                This award is to honor the achievements and the entrepreneurial spirit of Indian
                                franchisors.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Silicon India 2014</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Top 5 Most Promising Preschool In India</li>
                            </ul>
                            <div class="text">
                                This award is in recognition for exemplary contribution that has enhanced student
                                learning and achievement.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Indian Education Award 2014</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Best Preschool in South</li>
                            </ul>
                            <div class="text">
                                The award aims to recognize a regional pre-school chain for incorporating outstanding
                                quality in education delivery, childcare and hygiene.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>The Franchise World 2012 and 2013</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Top 100 franchisee preschools in India</li>
                            </ul>
                            <div class="text">
                                The award aims to recognize achievers and innovators who have contributed significantly
                                towards the excellence and growth of the education sector.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Silicon India 2012</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>op 10 Preschools In India</li>
                            </ul>
                            <div class="text">
                                The award recognizes the most sought after preschool in India. It recognizes the
                                preschool that has incorporated outstanding education quality.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>