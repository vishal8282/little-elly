<?php

require_once "config/db-connect.php";

$sql = "SELECT * FROM locate_centers";
$result = $conn->query($sql);

$centers = [];
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $centers[] = (object)$row;
    }
} else {
    echo "0 results";
}
$conn->close();

// print_r($centers);die;

?>

<?php include 'header.php';?>

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Locate Center</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Locate Center</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!--Gallery Section-->
<section class="gallery-section alternate">
    <div class="auto-container">
        <!--Galery-->
        <div class="sortable-masonry">
            <!--Filter-->
            <div class="filters clearfix">
                <!-- <ul class="filter-tabs filter-btns clearfix"> -->
                  <ul class="nav nav-tabs ">
                    <!-- <li class="active filter" data-role="button" data-filter=".All">All</li>
                    <li class="filter" data-role="button" data-filter=".Bangalore">Bangalore</li>
                    <li class="filter" data-role="button" data-filter=".Mangalore">Mangalore</li>
                    <li class="filter" data-role="button" data-filter=".Mysore">Mysore</li>
                    <li class="filter" data-role="button" data-filter=".Chennai">Chennai</li>
                    <li class="filter" data-role="button" data-filter=".Hyderabad">Hyderabad</li>
                    <li class="filter" data-role="button" data-filter=".Pune">Pune</li>
                    <li class="filter" data-role="button" data-filter=".Surat">Surat</li>
                    <li class="filter" data-role="button" data-filter=".Others">Others</li> -->
                     <li class="active"><a data-toggle="tab" href="#home">All</a></li>

                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Banglore
                            <!-- <span class="caret"></span> -->
                        </a>
                            <ul class="dropdown-menu">
                                <li><a data-toggle="tab" href="#BangaloreCenter">Bangalore Center</a></li>
                                <li><a data-toggle="tab" href="#BangaloreEast">Bangalore East</a></li>
                                <li><a data-toggle="tab" href="#BangaloreWest">Bangalore West</a></li>
                                <li><a data-toggle="tab" href="#BangaloreNorth">Bangalore North</a></li>
                                <li><a data-toggle="tab" href="#BangaloreSouth">Bangalore South</a></li>
                            </ul>
                      </li>

                    <li><a data-toggle="tab" href="#Mangalore">Mangalore</a></li>
                    <li><a data-toggle="tab" href="#Mysore">Mysore</a></li>
                    <li><a data-toggle="tab" href="#Chennai">Chennai</a></li>
                    <li><a data-toggle="tab" href="#Hyderabad">Hyderabad</a></li>
                    <li><a data-toggle="tab" href="#Surat">Surat</a></li>
                    <li><a data-toggle="tab" href="#Pune">Pune</a></li>
                    <li><a data-toggle="tab" href="#Others">Others</a></li>
                </ul>
            </div>
<!-- 
            <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div> -->

            <div class="items-container row clearfix">

                <!-- Course Single Section -->
                <section class="course-single">
                    <div class="anim-icons">
                        <span class="icon icon-flower wow zoomIn"></span>
                        <span class="icon icon-flower-2 wow zoomIn"></span>
                        <span class="icon icon-flower-3 wow zoomIn"></span>
                    </div>

                    <div class="auto-container">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->name ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                        </div>

                        <div id="BangaloreCenter" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'BangaloreCenter'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>


                        <div id="BangaloreEast" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'BangaloreEast'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>



                        <div id="BangaloreWest" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'BangaloreWest'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>


                        <div id="BangaloreNorth" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'BangaloreNorth'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>


                        <div id="BangaloreSouth" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'BangaloreSouth'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>


                        <div id="Mangalore" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'Mangalore'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>

                        <div id="Mysore" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'Mysore'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>

                        <div id="Chennai" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'Chennai'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>

                        <div id="Hyderabad" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'Hyderabad'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>

                     <div id="Surat" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'Surat'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                      
                        </div>

                        <div id="Pune" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'Pune'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                      
                        </div>


                    <div id="Others" class="tab-pane fade in active">
                            <?php foreach($centers as $center){ ?>
                            <?php if($center->branch == 'Others'){ ?>
                            <div class="lower-content material-bg">
                                <h3> <?php echo $center->name ?></h3>
                                <div class="text">
                                    <ul class="list-style-two All">
                                        <li>
                                            <strong>Address :</strong>
                                            <?php echo $center->address ?>
                                        </li>
                                        <li>
                                            <strong>Center Head :</strong>
                                            <!-- Jyothi -->
                                            <?php echo $center->center_head ?>
                                        </li>
                                        <li>
                                            <strong>Center Coordinator :</strong>
                                            <!-- Mamatha -->
                                            <?php echo $center->center_coordinator ?>
                                        </li>
                                        <li>
                                            <strong>Center Email ID :</strong>
                                            <!-- abhyudaylearning@littleelly.com -->
                                            <?php echo $center->email ?>
                                        </li>
                                        <li>
                                            <strong>Center Ph.No :</strong>
                                            <!-- 7259307233 -->
                                            <?php echo $center->phone ?>
                                        </li>
                                    </ul>
                                </div>
                                <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="theme-btn btn-style-four">Read More</a>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>

                    </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<!--End Gallery Section-->

<!-- Main Footer -->
<?php include 'footer.php';?>