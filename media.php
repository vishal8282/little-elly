<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Media</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Media</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Blog Grid -->
<section class="blog-grid">
    <div class="auto-container">
        <div class="masonry-items-container row clearfix">
            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-1.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">08 <span>Apr</span></div>
                            <ul class="info">
                                <li>News</li>
                                <li>little Elly Pre-school </li>
                            </ul>
                            <h3><a href="blog-single-1.html">Little Elly to launch ...</a></h3>
                        </div>
                        <div class="text">
                            Little Elly Pre-school headquartered in Bangalore is soon going to start its
                            international endeavor with a centre in UAE. Little Elly has esta...
                        </div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image-box">
                            <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/126631485?color=ffffff&portrait=0"
                                    style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">29 <span>Sep</span></div>
                            <ul class="info">
                                <li>News</li>
                                <li>Little Elly, has come up </li>
                            </ul>
                            <h3><a href="blog-single-1.html">Little Elly opens up i...</a></h3>
                        </div>
                        <div class="text">
                            Little Elly, has come up with its 76th centre in Bengaluru, in AGS Layout. They
                            reached their milestone of 100 pre-schools in the year 2013 a...
                        </div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image">
                            <a href="blog-single-1.html"><img src="images/resource/blog-grid-3.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">31 <span>May</span></div>
                            <ul class="info">
                                <li>News</li>
                                <li>Raising a happy, healthy </li>
                            </ul>
                            <h3><a href="blog-single-1.html">An Inspired Approach To Education</a></h3>
                        </div>
                        <div class="text">
                            Raising a happy, healthy child is one of the most challenging jobs a parent can
                            have — and also one of the most rewarding. Yet many of us...
                        </div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-4.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">31 <span>May</span></div>
                            <ul class="info">
                                <li>By Admin</li>
                                <li>01 comment</li>
                            </ul>
                            <h3><a href="blog-single-1.html">Play Is Our Brain's Favorite Way Of Learning</a></h3>
                        </div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-5.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">31 <span>May</span></div>
                            <ul class="info">
                                <li>By Admin</li>
                                <li>01 comment</li>
                            </ul>
                            <h3><a href="blog-single-1.html">Where Well Rounded Starts with Well Educated</a></h3>
                        </div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="single-item-carousel owl-carousel owl-theme">
                            <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-6.jpg"
                                        alt=""></a></div>
                            <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-5.jpg"
                                        alt=""></a></div>
                            <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-4.jpg"
                                        alt=""></a></div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">31 <span>May</span></div>
                            <ul class="info">
                                <li>By Admin</li>
                                <li>01 comment</li>
                            </ul>
                            <h3><a href="blog-single-1.html">Where Well Rounded Starts with Well Educated</a></h3>
                        </div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <blockquote><span class="icon flaticon-right-quotes-symbol"></span>Capitalize on low hanging
                    fruit to identify a ballpark value added activity to beta test. Override the digital</blockquote>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-2.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">31 <span>May</span></div>
                            <ul class="info">
                                <li>By Admin</li>
                                <li>01 comment</li>
                            </ul>
                            <h3><a href="blog-single-1.html">Where Well Rounded Starts with Well Educated</a></h3>
                        </div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block masonry-item col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/blog-grid-7.jpg" alt=""></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Blog Grid -->

<!-- Main Footer -->
<?php include 'footer.php';?>