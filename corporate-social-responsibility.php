<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Corporate Social Responsibility</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Corporate Social Responsibility</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column accordion-column col-lg-12 col-md-12 col-sm-12 col-offset-5" style="margin-bottom: 50px;">
                <div>
                    <h3>
                        Education is the most powerful weapon which you can use to change the world.
                    </h3>
                    <h2 class="nelson-title"><span>-</span>Nelson Mandela</h2>

                    <div class="text">
                        One of the most noteworthy values of social progress is education. This also plays a pivotal
                        role for a society to achieve competency and equal development.

                        Corporate social responsibility (CSR) is a form of corporate self-regulation integrated into
                        business models.

                        Little Elly leads its CSR initiative through the school project “Priyadarshini” in Bangalore.
                        The aim of this project is to improve the quality of education in rural schools in Bangalore.

                        The Little Elly corporate arranges teacher training and delivers curriculum for quality
                        enhancement of education in the targeted rural schools.

                        As part of the project, a curriculum based on textbook syllabus has been developed. The aim is
                        to help the vulnerable and disadvantaged children at these schools acquire fundamental skills
                        that are required for success in today’s world.
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>