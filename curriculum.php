<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Curriculum Framework</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Curriculum Framework</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Single Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="image-box">
            <div class="image wow fadeIn"><img src="images/resource/framework.jpg" alt=""></div>
        </div>
        <div class="lower-content material-bg">
            <h3>Personal & Social Development</h3>
            <div class="text">
                <p>
                    <strong>
                        By the end of the preschool years, the child will:
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Demonstrate a sense of self as a learner.
                    </li>
                    <li>
                        Demonstrate a sense of responsibility to oneself and others.
                    </li>
                    <li>
                        Demonstrate effective functioning, individually and as the member of a group.
                    </li>
                </ul>
            </div>
        </div>
        <div class="lower-content material-bg">
            <h3>Cognitive Development</h3>
            <div class="text">
                <p>
                    <strong>
                        By the end of the preschool years, the child will:
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Demonstrate the ability to think, reason, question and remember;
                    </li>
                    <li>
                        Engage in problem solving;
                    </li>
                    <li>
                        Use language to communicate, convey and interpret meaning
                    </li>
                    <li>
                        Establish contacts with an understanding of the physical and social world.
                    </li>
                </ul>
            </div>
        </div>
        <div class="lower-content material-bg">
            <h3>Creative Expression & Aesthetic Development</h3>
            <div class="text">
                <p>
                    <strong>
                        By the end of the preschool years, the child will:
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Exhibit curiosity and explore functioning of materials.
                    </li>
                    <li>
                        Use art forms as an instrument for creative expression and representation
                    </li>
                    <li>
                        Develop an appreciation for various forms of art.
                    </li>
                </ul>
            </div>
        </div>
        <div class="lower-content material-bg">
            <h3>Physical Development</h3>
            <div class="text">
                <p>
                    <strong>
                        By the end of the preschool years, the child will:
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Demonstrate control, balance, strength and coordination in gross motor tasks
                    </li>
                    <li>
                        Demonstrate coordination and strength in fine motor tasks
                    </li>
                    <li>
                        Participate in healthy physical activity
                    </li>
                    <li>
                        Practice appropriate eating habits, hygiene and self-help skills
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End Course Single Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>