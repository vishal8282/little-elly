<?php

require_once "../config/db-connect.php";

$sql = "SELECT * FROM locate_centers";
$result = $conn->query($sql);

$centers = [];
if ($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {
		$centers[] = (object)$row;
	}
} else {
	echo "0 results";
}
$conn->close();

// $centers = (object)$centers;

// print_r($centers);
// foreach($centers as $center){
// 	print_r($center->name);
// }
// die;

?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"><style>
		.custom
		 {
			padding: 19px;
		    text-align: left;
		    min-height: 250px;
		    /* max-width: 275px; */
		    background-color: beige;
		    border: 1px solid #753475;
		    margin-bottom: 20px;
          }
          .custom1
          {
          	    display: inline-block;
			    width: 70%;
			    height: 45px !important;
			    padding: 5px 20px !important;
			    font-size: 16px;
          }
          .adi
          {
          	    padding: 10px 40px;
			    background: #FFFFFF;
			    color: #A65BC6;
			    border: 1px solid #A65BC6;
          }
          .aadi2
          {
          	margin-bottom: 40px !important;
          }
	</style>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="UTF-8">
		<title>Locate Center -</title>
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="http://www.littleelly.com/xmlrpc.php">
		<!--[if lt IE 9]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
		<style type="text/css"  scoped="scoped">
					.main-section h4, .main-section h4 a{
color:#fff !important;
}

		</style> 
<link rel="shortcut icon" href="http://www.littleelly.com/new/wp-content/uploads/2015/10/littleelly-favicon.png">

		<style type="text/css">
			@import url(http://fonts.googleapis.com/css?family=Roboto);@import url(http://fonts.googleapis.com/css?family=Roboto);@import url(http://fonts.googleapis.com/css?family=Roboto);@import url(http://fonts.googleapis.com/css?family=Roboto);	body,.main-section p {
		font:  15px 'Roboto', sans-serif; 		color:#333333;
	}
	
	header .logo {
		margin:6px 0px !important;
   	}
	.nav li a,header .btn-style1,.footer-nav ul li a {
		font:  14px 'Roboto', sans-serif !important;	}
	.cs-section-title h2{
		font:  24px 'Roboto', sans-serif !important;	}
 	h1{
	font:  60px 'Roboto', sans-serif !important;}
	h2{
	font:  20px 'Roboto', sans-serif !important;}
	h3{
	font:  18px 'Roboto', sans-serif !important;}
	h4{
	font:  16px 'Roboto', sans-serif !important;}
	h5{
	font:  14px 'Roboto', sans-serif !important;}
	h6{
	font:  12px 'Roboto', sans-serif !important;}
	
	.main-section h1, .main-section h1 a {color: #b1457f !important;}
	.main-section h2, .main-section h2 a{color: #262626 !important;}
	.main-section h3, .main-section h3 a{color: #262626 !important;}
	.main-section h4, .main-section h4 a{color: #262626 !important;}
	.main-section h5, .main-section h5 a{color: #262626 !important;}
	.main-section h6, .main-section h6 a{color: #262626 !important;}
	.widget .widget-section-title h2{
		font:  15px 'Roboto', sans-serif !important;	}
	.top-bar,#lang_sel ul ul {background-color:#753475;}
	#lang_sel ul ul:before { border-bottom-color: #753475; }
	.top-bar p{color:#fff !important;}
	.top-bar a,.top-bar i,.cs-users i{color:#fff !important;}
 	.logo-section,.main-head{background: !important;}
	.main-navbar {background:#ffffff !important;}
	.navbar-nav > li > a,li.parentIcon a:after,.navigation ul .sub-dropdown li a i {color:#753475 !important;}
	.navbar-nav > li > .dropdown-menu,.navbar-nav > li > .dropdown-menu > li > .dropdown-menu,.mega-grid,.sub-dropdown { background-color:#fff !important;}
	.navbar-nav .sub-menu .dropdown-menu li a, .sub-dropdown li a {color:#753475 !important;}
	.navigation ul .sub-dropdown > li:hover > a,.navbar-nav .sub-menu .dropdown-menu > li:hover > a,ul ul li.current-menu-ancestor.parentIcon > a:after,ul ul li.parentIcon:hover > a:after,.navigation ul .sub-dropdown >  li:hover > a > i {border-bottom-color:#fff;color:#fff !important;}
	.navigation .navbar-nav > li.current-menu-item > a,.navigation .navbar-nav > li.current-menu-ancestor > a,.navigation .navbar-nav > li:hover > a,li.current-menu-ancestor.parentIcon > a:after,li.parentIcon:hover > a:after {color:#b378d3 !important;}
	.navigation .navbar-nav > .active > a:before, .navigation .navbar-nav > li > a:before{ border-bottom-color:#b378d3 !important; }
	.cs-user,.cs-user-login { border-color:#b378d3 !important; }
	.navigation ul .sub-dropdown > li:hover > a { background-color:#b378d3 !important; }
	{
		box-shadow: 0 4px 0 #753475 inset !important;
	}
	.header_2 .nav > li:hover > a,.header_2 .nav > li.current-menu-ancestor > a {
       
	}
	</style>
<style type="text/css">
        footer#footer-sec, footer#footer-sec:before {
            background-color:#f8f8f8 !important;
        }
		#footer-sec {
            background-image:url(http://www.littleelly.com/new/wp-content/themes/peachclub-theme/assets/images/bg-footer.png) !important;
        }
                #copyright p {
            color:#333 !important;
        }
		footer#footer-sec a,footer#footer-sec .widget-form ul li input[type="submit"],footer#footer-sec .tagcloud a,footer#footer-sec .widget ul li a{
            color: !important;
        }
        footer#footer-sec .widget h2, footer#footer-sec .widget h5,footer#footer-sec h2,footer#footer-sec h3,footer#footer-sec h4,footer#footer-sec h5,footer#footer-sec h6 {
            color:#b378d3 !important;
        }
        footer#footer-sec .widget ul li,footer#footer-sec .widget p, footer#footer-sec .widget_calendar tr td,footer#footer-sec,footer#footer-sec .col-md-3 p,footer#footer-sec .widget_latest_post .post-options li,footer#footer-sec .widget i,.widget-form ul li i,footer#footer-sec .widget_rss li,footer#footer-sec .widget_recent_comments span {
            color: !important;
        }
		#newslatter-sec{
			background-color:#753475 !important;
			background-image:url(http://www.littleelly.com/new/wp-content/themes/peachclub-theme/assets/images/paralex.png) !important;
		}
		
    </style>
<style type="text/css">
/*!
/*============================================ Orange Color ( Color 1 ) ==============================================*/

.cs-color, .cs-colorhover:hover, .cs-user-tabs .borderless ul li.active a,.widget_tag_cloud ul li a,#newslatter-sec .sg-socialmedia ul li a i:hover,.widget.twitter_widget a, .cs-classic-form h1,.widget.widget-recent-blog.widget_latest_post p,.cs-classic-form h2, .cs-section-title h2, .classes-medium-view .text h2 a, .rich_editor_text .sg-socialmedia ul li a i, .classdetail-options li.cs-price-sec strong, .class-sidebar .classdetail-options li.cs-price-sec small, .simple .panel-heading .collapsed:before,.csdefault .panel-heading .collapsed:before, .box .panel-heading .collapsed:before , .box .panel-heading a,.cs-plain-form.cs_form_styling form label i, .box .panel-heading a , .csdefault .panel-heading a:before, .box .panel-heading a:before,.read-more,.widget-section-title h2,.table-spinner,.cs-heading h2,.widget_nav_menu ul li i,.cs-blog:hover h2 a,.cs-team:hover h2 a,.widget h2 a:hover,.widget_blog .infotext a:hover,.cs-price-table.pr-simple .features li i {
 color:#a65bc6 !important;
}

.cs-bgcolor, .cs-bgcolorhover:hover, .cs-user-tabs .borderless ul li.active a span, .upload-file input[type="submit"] ,.cs-comment,.widget_contact,.widget_tag_cloud ul li:hover,.widget_calendar thead,#wp-calendar tbody tr td#today,.cs-bgcolor, .cs-bgcolorhover:hover, .cs-bg-color, .woocommerce #content input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce-page #content input.button:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button:hover, .woocommerce-page input.button:hover, .cs-attachment ul li a:hover, .thumblist .comment-reply-link:hover, .nxt-prv .owl-nav div:hover, .cs-tags ul li:hover a, .widget_archive ul li:hover, .widget_categories ul > li:hover > a, .widget_pages ul > li:hover > a, .widget_meta ul li:hover, .widget_recent_comments ul li:hover, .widget_recent_entries ul li:hover, .widget_nav_menu ul > li:hover > a, .widget_tag_cloud .tagcloud a:hover, form p.form-submit input[type="submit"], .team-detail .Profile-btn, .time_line .owl-nav div:hover::before, .blog-hover i:hover,.cs-search input.btn,.date-time time span,.pagination li:hover,.widget-form ul.group li input.cs-submit-btn, .class-sidebar .classdetail-options li.cs-class-btn, .testimonial .question-mark,.blog-grid:hover .read-more,.blog-grid figure figcaption a,.cs-icon-plus i, .simple .panel-heading a:before,.cs-tabs.modren-view .nav-tabs li a,.prev-next-post .prev a:hover,.prev-next-post .next a:hover,.class-pattren li i,.class-pattren li:after,.flexslider.cs-pattern-slider .flex-direction-nav a,.pagination li a.active,.blog-grid:hover .read-more,#process_newsletter_1,.panel-heading .collapse.collapsed:hover:before,.cs-tabs.vertical .nav-tabs li a,.cs-bg-color,.simple .panel-heading .collapsed:hover:before,.filter_nav ul li a:hover,.widget_archive ul li:hover,.widget_pages ul > li > a:hover, .widget_meta ul li:hover, .widget_recent_entries ul li:hover {
	background-color:#a65bc6 !important;
}

.textborder:before,.rich_editor_text p:first-child:before,.prev-next-post .prev a, .prev-next-post .next a {
 border-color:#a65bc6 !important;
}

.hd-search .dropdown-menu {
 border-bottom-color:#a65bc6 !important;
}

/*============================================ Purple Color ( Color 2 ) ==============================================*/

.infotext a,.cs-blog h2 a,.navigation ul > li > a,.widget.widget_recent_comments .comment-author-link,.widget_latest_post h2 a, .classes-grid-view .text h2 a, .cs-post-title a, .rich_editor_text h4, .teacher-information h3,
.main-section .panel-heading a, .main-section .related-blogs ul li h4 a, .dropcap-one:first-letter, .dropcap-one p:first-letter, .dropcap-two:first-letter, .dropcap-two p:first-letter, .box .panel-heading a.collapsed {
	color:#753475 !important;
}
.widget_btn,#wp-calendar, .widget_calendar caption, #newslatter-sec,.breadcrumb-sec .breadcrumbs ul li,.blog-editor .cs-classic-form form input[type="submit"],.cs-plain-form.cs_form_styling form label input[type="submit"],
.cs-tabs.modren-view .nav-tabs li.active a,.cs-tabs.vertical .nav-tabs li.active a {
	background-color: #753475 !important;
}
.cs-tabs.modren-view .nav-tabs li.active a:before {
	border-color: #753475 !important;
}
.cs-tabs.modren-view .nav-tabs li.active a:after,.cs-tabs.vertical .nav-tabs li.active a:before {
	border-top-color: #753475 !important;
}

		.breadcrumb-sec {
			border-top: 1px solid #dddddd;
			
		}
		.breadcrumb-sec {
			border-bottom: 1px solid #dddddd;
		}
	
</style>
<title>Locate Center -</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- This site is optimized with the Yoast SEO plugin v3.2.5 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="http://www.littleelly.com/locate-center/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Locate Center -" />
<meta property="og:url" content="http://www.littleelly.com/locate-center/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Locate Center -" />
<!-- / Yoast SEO plugin. -->

		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.littleelly.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.4.17"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;return g&&g.fillText?(g.textBaseline="top",g.font="600 32px Arial","flag"===a?(g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3):"diversity"===a?(g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e):("simple"===a?g.fillText(h(55357,56835),0,0):g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='contact-form-7-css'  href='http://www.littleelly.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='style2-os-css-css'  href='http://www.littleelly.com/wp-content/plugins/gallery-images/style/style2-os.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='lightbox-css-css'  href='http://www.littleelly.com/wp-content/plugins/gallery-images/style/lightbox.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='filterMediank-css'  href='http://www.littleelly.com/wp-content/plugins/responsive-filterable-portfolio/css/filterMediank.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='filterMediank-lbox-css'  href='http://www.littleelly.com/wp-content/plugins/responsive-filterable-portfolio/css/filterMediank-lbox.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://www.littleelly.com/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=4.6.5' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link rel='stylesheet' id='wpsl-styles-css'  href='http://www.littleelly.com/wp-content/plugins/wp-store-locator/css/styles.min.css?ver=2.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap.min_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/assets/css/bootstrap.min.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='responsive_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/assets/css/responsive.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='style_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/style.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='iconmoon_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/assets/css/iconmoon.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='widget_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/assets/css/widget.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/assets/css/flexslider.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='prettyPhoto_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/assets/css/prettyphoto.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='owl.carousel_css-css'  href='http://www.littleelly.com/wp-content/themes/peachclub-theme/assets/css/owl.carousel.css?ver=4.4.17' type='text/css' media='all' />
<link rel='stylesheet' id='sticky_popup-style-css'  href='http://www.littleelly.com/wp-content/plugins/sticky-popup/css/sticky-popup.css?ver=1.2' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.4.17' type='text/css' media='all' />
<script type='text/javascript' src='http://www.littleelly.com/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://www.littleelly.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://www.littleelly.com/wp-content/plugins/responsive-filterable-portfolio/js/filterMediank.js?ver=4.4.17'></script>
<script type='text/javascript' src='http://www.littleelly.com/wp-content/plugins/responsive-filterable-portfolio/js/filterMediank-lbox-js.js?ver=4.4.17'></script>
<script type='text/javascript' src='http://www.littleelly.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?ver=4.6.5'></script>
<script type='text/javascript' src='http://www.littleelly.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?ver=4.6.5'></script>
<script type='text/javascript' src='http://www.littleelly.com/wp-content/plugins/sticky-popup/js/modernizr.custom.js?ver=1.2'></script>
<link rel='https://api.w.org/' href='http://www.littleelly.com/wp-json/' />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.littleelly.com/wp-includes/wlwmanifest.xml" /> 
<link rel='shortlink' href='http://www.littleelly.com/?p=4420' />
<link rel="alternate" type="application/json+oembed" href="http://www.littleelly.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.littleelly.com%2Flocate-center%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://www.littleelly.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.littleelly.com%2Flocate-center%2F&#038;format=xml" />
		<script type="text/javascript">
			jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				var ajaxRevslider = function(obj) {
				
					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
					
					var content = "";

					data = {};
					
					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = 'a6fd520050';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;
					
					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"http://www.littleelly.com/wp-admin/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;								
						},
						error: function(e) {
							console.log(e);
						}
					});
					
					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;						 
				};
				
				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
		</script>
				<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<style type="text/css" id="custom-background-css">
body.custom-background { background-image: url('http://www.littleelly.com/new/wp-content/uploads/2016/04/BG_Pattern.png'); background-repeat: repeat; background-position: top left; background-attachment: scroll; }
</style>
		<style type="text/css">
			.sticky-popup .popup-header
			{
				
				background-color : #ec7f24;		
							
				border-color : #ed7512;		
						
		}
		.popup-title
		{
							color : #ffffff;		
					}
					.sticky-popup-right, .sticky-popup-left
			{
									top : 25%;		
							}

				</style>
				<style>
	        .gallery_{
	            min-height: 400px !important;
	        }
	    </style>
	</head>
		<body class="page page-id-4420 page-template page-template-SLocateCenter page-template-SLocateCenter-php custom-background"  >
				<!-- Wrapper Start -->
		<div class="wrapper header-relative wrapper_full_width">
			<!-- Header Start -->
			
		<header id="main-header" class="clearfix ">

				<!--//  TOPBAAR //-->

				
<!-- Top Strip -->




    <div class="top-bar"> 

      <!-- Container -->

      
      <div class="container"> 

      
     	<!-- Left Side -->

        <aside class="left-side">

        
          		<p>
									<i class="icon-phone8"></i>
									Call Us : 7676 38 0000
								</p>
								<p>
									<i class="icon-paperplane3"></i>
									<a href="#">E - Mail Us : support@littleelly.com</a>
								</p>
          
        </aside>

        <!-- Right Side -->

        <aside class="right-side">

        	<div class="cs-login-sec">

              	<ul>
              		<li><a target="_blank" href="http://www.littleelly.com/locate-center/">Locate Centers</a></li>
                  	<li><a target="_blank" href="http://www.littleelly.com/age-appropriate-program/">Age Appropriate Program</a></li>

                  	<li><a target="_blank" href="http://www.littleelly.com/admission-enquiry/">Admission Enquiry</a></li>

                  	<li><a target="_blank" href="http://www.littleelly.com/franchisee-enquiry/">Franchisee Enquiry</a></li>

              	</ul>

          	</div>

			 <div class="sg-socialmedia"><ul >
						
          

        		            <li><a style="color:#fff;" title="" href="https://www.facebook.com/littleellypreschool" data-original-title="Facebook" data-placement="top"  class="colrhover"  target="_blank"> 

								 

                                 <i class="icon-facebook8 "></i>

						</a></li>
						
          

        		            <li><a style="color:#fff;" title="" href="https://twitter.com/le_littleelly" data-original-title="Twitter" data-placement="top"  class="colrhover"  target="_blank"> 

								 

                                 <i class="icon-twitter7 "></i>

						</a></li>
						
          

        		            <li><a style="color:#fff;" title="" href="https://plus.google.com/+Littleellyindia" data-original-title="Google Plus" data-placement="top"  class="colrhover"  target="_blank"> 

								 

                                 <i class="icon-google-plus "></i>

						</a></li>
						
          

        		            <li><a style="color:#eee;" title="" href="https://www.youtube.com/user/PreschoolLittleElly?sub_confirmation=1" data-original-title="Youtube" data-placement="top"  class="colrhover"  target="_blank"> 

								 

                                 <i class="icon-youtube-play "></i>

						</a></li></ul></div>
        </aside>



        <!-- Right Section -->

 	 <!-- Container -->

      
      </div>

      
      <!-- Container --> 

    </div>

<!-- Top Strip -->


				<!--//  MainNavBaar //-->

				<div class="main-navbar">

					<div class="container">

						<div class="logo">
		<a href="http://www.littleelly.com">	

			<img src="http://www.littleelly.com/new/wp-content/uploads/2015/10/littleelly-e1446016393604.png" style="width:160px; height: autopx;" alt="">

        </a>

	</div>

							<nav class="navbar navigation ">

			  <div class="navbar-header">

				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-94">

				  <span class="sr-only">Toggle navigation</span>

				  <span class="icon-bar"></span>

				  <span class="icon-bar"></span>

				  <span class="icon-bar"></span>

				</button>

			  </div>

			  <div class="collapse navbar-collapse" id="navbar-collapse-94"><ul class="navbar-nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3934"><a href="http://www.littleelly.com/about/"><i class="icon-child"></i>About Us</a>
<ul class="sub-dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4033"><a href="http://www.littleelly.com/philosophy/">Philosophy</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4032"><a href="http://www.littleelly.com/our-vision-and-mission/">Our Vision &#038; Mission</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3861"><a href="http://www.littleelly.com/origin-and-history/">Origin &#038; History</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3860"><a href="http://www.littleelly.com/message-from-director/">Directors&#8217; Profile</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3859"><a href="http://www.littleelly.com/little-elly-ventures/">Little Elly Ventures</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3857"><a href="http://www.littleelly.com/awards-recognition/">Awards</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3858"><a href="http://www.littleelly.com/corporate-social-responsibility/">Corporate Social Responsibility</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3864"><a href="http://www.littleelly.com/the-school/"><i class="icon-home"></i>The School</a>
<ul class="sub-dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3869"><a href="http://www.littleelly.com/programs/">Programs</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3867"><a href="http://www.littleelly.com/curriculum/">Curriculum Framework</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3868"><a href="http://www.littleelly.com/facilities-faculty/">Facilities &#038; Faculty</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3997"><a href="http://www.littleelly.com/testimonial/">Testimonials</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3865"><a href="http://www.littleelly.com/assessment/">Assessment</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3866"><a href="http://www.littleelly.com/child-behaviour-policy/">Child Behaviour Policy</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3872"><a href="http://www.littleelly.com/our-approach/"><i class="icon-cone2"></i>Our Approach</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3876"><a href="http://www.littleelly.com/gallery/"><i class="icon-newspaper3"></i>Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3875"><a href="http://www.littleelly.com/in-media/"><i class="icon-calendar10"></i>In Media</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3877"><a href="http://www.littleelly.com/franchise-opportunity/"><i class="icon-users5"></i>Franchise Opportunity</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-4477"><a href="#"><i class="icon-star-o"></i>Contact Us</a>
<ul class="sub-dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4479"><a href="http://www.littleelly.com/india/">India</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4478"><a href="http://www.littleelly.com/international/">International</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4420 current_page_item menu-item-4422"><a href="http://www.littleelly.com/locate-center/">Locate Centers</a></li>
</ul>
</li>
</ul></div>

          </nav>
                            <a href="#" onclick="cs_open_close_toggle('http://www.littleelly.com/wp-admin/admin-ajax.php')" class="striptoggle cs-bg-color">

                            <i class="icon-plus8"></i>

                            </a>

					</div>

				</div>



			</header>

<!-- Header 2 End -->

			<script type="text/javascript">
				jQuery(document).ready(function(){
					console.log(jQuery('#main-header').scrollToFixed());
					// var marginTopValue = jQuery('#main-header').height();
					// jQuery(".breadcrumb-sec").css({
					// 	'margin-top': marginTopValue+'px'
					// });
				});
			</script>
						<div class="clear"></div>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1484116905174170', {
em: 'insert_email_variable,'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1484116905174170&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

			
<div class="breadcrumb-sec " style="background: #f6921f; min-height:auto!important;    " > 

  

  <!-- Container --> 

  
  <div class="container">

    <div class="cs-table">

      <div class="cs-tablerow"> 

        <!-- PageInfo -->

        <div class="pageinfo page-title-align-left" ><h1 style="color:#fff !important">Locate Center</h1></div>
        <!-- PageInfo -->
<!-- BreadCrumb -->

<div class="breadcrumbs page-title-align-left">

  
  					<style scoped="scoped">

						.breadcrumb-sec, .breadcrumb ul li a,.breadcrumb ul li.active,.breadcrumb ul li:first-child:after {

							color : #fff !important;

						}	

					</style>

  
  <ul><li><a href="http://www.littleelly.com/">Home</a></li><li class="active"><span>Locate Center</span></li></ul>
</div>

<div class="clear"></div>



<!-- BreadCrumb -->


        

      </div>

    </div>

  </div>

   

  <!-- Container --> 

</div>



			<!-- Breadcrumb SecTion -->
			<!-- Main Content Section -->
			<main id="main-content">
			<!-- Main Section Start -->
			<div class="main-section"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,900,700italic,900italic);@import url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css);@import url(table.css);@import url(tabs.css);body{font-family:'Roboto', sans-serif}h2{color:#8f6436 !important}a{cursor:pointer}.slider #owl-demo .item{height:auto}.slider #owl-demo .item img{display:block;width:100%;height:500px}.headerstrip{background-color:#09005b;color:#fff}.headerstrip .text{display:inline-block;padding-top:10px}.headerstrip a{display:inline-block;padding:10px 0px}.headerstrip a i{color:#fff}.header .searchform input{border-radius:0px}.header .searchform .searching button{border-radius:0px}.header .searchform .gcnumber button{color:#fff;background-color:#c4c4c4;border-color:#adadad;border-radius:0px}.header .navbar-default{background-color:#ffffff;border-color:#ffffff;margin-bottom:15px;margin-top:15px}.header .navbar-default .navbar-header .navbar-brand{padding:0px}.header .navbar-default .navbar-nav a{color:#333;line-height:8px !important}.header .navbar-default .navbar-nav .active a{color:#fff;background-color:#db261f}.header .navbar-default .navbar-nav .active a:hover{color:#fff;background-color:#db261f}.reachus{padding:30px 0px}.reachus .mapbg{background:url(../img/mapbg.png);background-size:cover;max-width:100%}.reachus .mapbg .address{max-width:330px;margin:10px auto}.reachus .formbg{background:url(../img/formbg.png);background-size:cover;max-width:100%}.reachus .formbg .formsection{background-color:rgba(51,51,51,0.5)}.reachus .formbg .formsection form{padding-top:20px}.reachus .formbg .formsection form legend{color:#fff}.reachus .formbg .formsection form input,.reachus .formbg .formsection form textarea{border-radius:0px}.reachus .formbg .formsection form .submit-group{padding-bottom:20px}.reachus .formbg .formsection form .submit-group .btn-dlr{border:1px solid #fff;background-color:#fff;color:#333;border-radius:0px}.reachus .formbg .formsection form .submit-group .btn-reset{border:1px solid #fff;color:#fff;border-radius:0px;background-color:transparent}.welcome{padding:30px 0px}.welcome .content h2{color:#8f6436}.welcome .content .para{font-size:15px;line-height:1.7em}.welcome .welcomeimg img{float:right;margin:50px auto}.visionmission{padding:30px 0px}.visionmission .card{-webkit-box-shadow:0px 5px 10px 1px rgba(50,50,50,0.52);-moz-box-shadow:0px 5px 10px 1px rgba(50,50,50,0.52);box-shadow:0px 5px 10px 1px rgba(50,50,50,0.52);min-height:320px;padding:30px}.visionmission .card:hover{-webkit-box-shadow:0px 5px 10px 1px #896543;-moz-box-shadow:0px 5px 10px 1px #896543;box-shadow:0px 5px 10px 1px #896543}.visionmission .card:hover .icon{border:1px solid #896543}.visionmission .card .icon{width:70px;height:70px;margin:auto;border:1px solid #c0c0c0;border-radius:50%}.visionmission .card .icon i{margin:10px auto}.visionmission .card p{font-size:15px;line-height:1.7em}.team{padding:30px 0px}.team h2{color:#8f6436}.footerbg{background:url(../img/banners/logistics-banner.jpg);background-size:cover;color:#fff}.footerbg .footer{background-color:rgba(0,0,0,0.75)}.footerbg .footer p{padding:20px}.footerbg .footer p a{color:#fff}.footerbg .footer p a:hover{text-decoration:none}.not-found{padding:85px}.copyright{background-color:#000;color:#fff}.aboutbg{background:url(../img/banners/about-us-banner.jpg);height:290px}.clientbg{background:url(../img/banners/client-banner.jpg);height:290px}.servicebg{background:url(../img/banners/services-banner.jpg);height:290px}.customercarebg{background:url(../img/banners/services-banner.jpg);height:290px}.contactbg{background:url(../img/banners/contact-banner.jpg);height:290px}.aboutus p{font-size:15px;line-height:1.7em}.aboutus ul li{list-style:none;font-size:15px}.aboutus ul li:before{content:"";border-color:transparent #ec2327;border-style:solid;border-width:0.35em 0 0.35em 0.45em;display:block;height:0;width:0;left:-1em;top:0.9em;position:relative}.aboutus .iconic p{color:#09005b}.aboutus .infra{padding-bottom:30px}.clients{padding:30px 0px}.clients .client-grid{margin:15px auto}.clients .client-grid .col-5{width:19%;display:inline-block;margin:3px}.clients .client-grid .col-5 .image{border:1px solid #c4c4c4}.servicetab{padding:30px 0px}.servicetab .tabsection .nav-tabs li.active{opacity:1}.servicetab .tabsection .nav-tabs li.active a{background-color:transparent;border:0px}.servicetab .tabsection .nav-tabs li.active a:hover,.servicetab .tabsection .nav-tabs li.active a:focus{background-color:transparent !important;border-bottom:0px;text-decoration:none}.servicetab .tabsection .nav-tabs .first{padding-top:10px !important}.servicetab .tabsection .nav-tabs li{padding:0px 5px;opacity:0.5}.servicetab .tabsection .nav-tabs li .tabbg{background:url("../img/tab-bg.png") no-repeat 0px;height:60px;max-width:100%;border:1px solid #db261f}.servicetab .tabsection .nav-tabs li .tabbg a{border-bottom:0px;border-radius:0px;padding-left:132px;color:#333;font-weight:500;text-transform:capitalize;font-size:15px;padding-top:18px;display:inline-block;padding-top:10px !important}.servicetab .tabsection .nav-tabs li .tabbg a:hover{background-color:transparent;border-color:transparent;text-decoration:none}.servicetab .tabsection .tab-content .tab-pane{padding:30px 0px}.servicetab .tab-img{position:absolute;padding-top:5px}.customercare{padding:30px 0px}label.error{color:#fff;font-weight:300}.networks h2{color:#1c1362 !important}.networks form .form-group .checkbox{padding:0px 20px}.networks .tabsection{color:#1c1362}.networks .tabsection .nav-stacked{border-bottom:0px}.networks .tabsection .nav-stacked li a{text-align:center;font-weight:500;font-size:16px;color:#333;border:1px solid #c4c4c4;margin:5px auto}.networks .tabsection .nav-stacked li.active a{background-color:#753475;color:#fff}.networks .tabsection .tab-content .tab-pane .addresscard{border:1px solid #c4c4c4;min-height:290px;margin:0px auto;word-break: break-all;}.searchstyle{margin-left:95px}.tollmain{border-radius:20px 0 20px 0;overflow:hidden;border:2px solid #2a1471;float:left;margin-bottom:20px}.tollmain a{color:#0066CC;text-decoration:none}.tollmain .number{float:left;color:#00a2d2;padding:0px 15px 0 5px;font-size:24px;font-weight:bold;line-height:19px}.tollmain .toll{float:left;padding:1px 34px 9px 31px;font-size:24px;font-weight:bold;background:rgba(219,38,31,0.72);color:#ffffff;line-height:19px}.tollmain .toll .subsmall{display:block;font-size:11px}.searchform .btn-primary1{margin-left:13px;padding:12px 30px;color:#e5625d;background:#fff;border:1px solid #2a1471;font-weight:bold;font-size:15px;border-radius:0}.searchform .btn-primary1:hover{background:#e5625d;color:#fff}ul .dropdown-menu{padding:10px}li a .dropdown-menu{padding:10px;font-size:15px;line-height:7px}ul .dropdown-menu li>a:hover{color:#262626;text-decoration:none}.dropdown-menu>li>a{padding:9px 20px}.downloads{margin-top:60px;margin-bottom:60px}.downloads a{background:#df2522;color:#fff;padding:11px 36px;font-size:17px}.downloads a:hover{background:#fff;color:#df2522}.newshead h1{margin-top:50px;margin-bottom:50px;color:#8f6436}.news{margin-bottom:30px}.awards{margin-top:60px}.phone{font-weight:bold}.banner-filter{    background-image: url("http://www.littleelly.com/wp-content/uploads/2015/06/city-street-bokeh-1400x875.jpg");background-size: 100%;width: 100%;height: 200px;background-position: 0 32%;background-repeat: no-repeat; margin: 0 auto 40px auto;box-shadow: 0px 1px 5px #753475;}.banner-input{    width: 100% !important;border-radius: 20px;border: solid #f2f2f2 2px;outline: 0;font-family: inherit;height: 40px;font-weight: normal;background: transparent;color: #080808;}.banner-filter h2{padding:50px 0;}.banner-filter h3{color:#333 !important;}.banner-input::-webkit-input-placeholder { /* WebKit, Blink, Edge */color:    #fff;}.banner-input:-moz-placeholder /* Mozilla Firefox 4 to 18 */{color:#fff;opacity:  1;}.banner-input::-moz-placeholder { /* Mozilla Firefox 19+ */color:#fff;opacity:  1;}.banner-input:-ms-input-placeholder { /* Internet Explorer 10-11 */color:#fff;}.banner-input:focus{outline:0;}.addresscard h5{margin-bottom:3px;}.addresscard p{margin-bottom:15px;}.service-container p{text-align:center;font-size:17px;font-weight:500}.service-container .col-5{padding:20px;display:inline-block}.service-container .col-5 .thumbnail{padding-top:45px;height:170px;width:181px;background-color:#8ea1ad;border:10px solid #aecadb;border-radius:22px}.service-container .col-5 .thumbnail:hover{transition:all 0.5s ease-in;background-color:#db261f !important;border:10px solid #ed938f}.service-container .col-5 .thumbnail img{width:60px}

	.breadcrumb-sec{
		margin: 0px 0px 0px 0px !important;
	}
	.btn-theme{
		color: #fff;background-color: #753475;border-color: #753475;border-radius: 2px;font-size: 13px;padding: 5px 6px;
	}
	
	#locateCenterLIL .addresscard {
	  	background: #fff !important;
	  	padding: 20px 4px !important;
	  	box-shadow: 0px 2px 4px -2px rgba(0, 0, 0, 0.4) !important;
	  	min-height: 215px;
	}
	#locateCenterLIL .addresscard p {
	  	margin-bottom: 15px !important;
	  	font-size: 12px !important;
	  	text-align: left !important;
	  	font-weight: 500 !important;
	}
	#locateCenterLIL .addresscard h5 {
	  	font-weight: 500 !important;
	}
	span.ng-binding {
    font-weight: normal !important;
}



</style>
	<div class="clearfix"></div>
	<div class="clearfix networks" ng-app="SearchApp">
		<!-- 
		<div class="col-md-12 banner-filter">
		
			<div class="container ">
				<div class="row">
				<div class="col-md-offset-4 material-bg-white col-md-4">
					<div class="row text-center clearfix">	   
						<h4><b>To Locate Nearest Little Elly Center Just SMS<b></h4>
						<h4>&lt;First Three Letter Of Your City&gt;&lt;Space&gt;</h4>
						<h4>&lt;First Three Letter Of Your Area&gt;&lt;Space&gt;</h4>
						<h4>&lt;Your Message To 9243000900&gt;</h4>
					</div>
				</div>
				</div>
			</div>
		
		</div> -->
		<div class="container" ng-controller="SearchCtrl">
		<div class="col-md-offset-4 material-bg-white">
			<!-- <form action="" method="POST" class="form-inline" role="form">
							<div class="form-group">

								<h3 class="text-center">Relax , finding a Little Elly is easier now</h3>
								<input class="form-control  banner-input" ng-model="searchText" placeholder="Search for Branches">
							</div>
						</form> -->
		</div>
		
			<div class="row">
				<div class="tabsection">
					<div role="tabpanel">
						<div class="col-md-3">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs nav-stacked" role="tablist">
								<h2 class="text-center">Branches</h2>
								<li role="presentation" id="all" class="active" check-if-active>
									<a ng-click="filterState('')">All</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Bangalore Central')" >Bangalore Center</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Bangalore East')">Bangalore East</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Bangalore West')">Bangalore West</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Bangalore North')">Bangalore North</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Bangalore South')">Bangalore South</a>
								</li>
								
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Mangalore')">Mangalore</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Mysore')">Mysore</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Chennai')">Chennai</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Hyderabad')">Hyderabad</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Pune')">Pune</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('Surat')">Surat</a>
								</li>
								<li role="presentation" check-if-active>
									<a ng-click="filterState('others')">Others</a>
								</li>
								<!-- <li role="presentation" check-if-active>
									<a ng-click="filterState('Delhi')">Delhi</a>
								</li> -->
							</ul>
						</div>
						<div class="col-md-9">
							<!-- Tab panes -->
							<div class="tab-content" id="locateCenterLIL">
								<div role="tabpanel" class="tab-pane active" id="all">
									<div class="">
										<div class="row clearfix">
										<?php foreach($centers as $center){ ?>
											<div class="col-md-6 clearfix " ng-repeat="branch in branches| filter:searchText | filter:searchState">

											<div class="material-bg">

												<div class="addresscard clearfix row" ng-if="branch">
													<div class="col-md-12 clearfix">
														<h5>Center Name : <?php echo $center->name ?><span ng-bind="branch.location"></span></h5>
														<h5>Address : <?php echo $center->address ?><span ng-bind="branch.address"></span></h5>
														
														<h5>Center Head : <?php echo $center->center_head ?><span ng-bind="branch.hdName"></span></h5>
														
														<h5>Center Coordinator : <?php echo $center->center_coordinator ?><span ng-bind="branch.cdName"></span></h5>
														
														<h5>Center Email ID : <?php echo $center->email ?><span ng-bind="branch.email"></span></h5>
														
														<h5>Center Ph.No : <?php echo $center->phone ?><span ng-bind="branch.mobile"></span></h5>
														
													</div>
													
													<div class="col-md-4 clearfix">

													</div>
													<div class="col-md-4 clearfix">
     													<span ng-if="branch.url">
													        <a target="_blank" href="<?php echo '/locate-center/' . $center->seo_url ?>" class="btn btn-theme btn-info center-block" type="submit">View More</a>
													    </span>
													</div>	
												</div>

												</div>

											</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script> -->
<!-- 		<script>
	angular.module('SearchApp', [])
	.controller('SearchCtrl', function($scope, $http) {

			$scope.branches = [{"locationId":"2","location":"Wind Tunnel","cdName":"Mamatha","hdName":"Jyothi","email":"abhyudaylearning@littleelly.com","mobile":"7259307233","zone":"Bangalore Central","url":"http:\/\/littleelly.com\/littleelly-windtunnel-road\/","address":"# 39, NAL Wind Tunnel Road, SMV Sunlight, Murugesh Palya, Bangalore-560017"},{"locationId":"3","location":"Indiranagar","cdName":"Divya & Judith ","hdName":"Divya P Shetty & Judith V Mammen","email":"indiranagar@littleelly.com","mobile":"9591530084, 9901614888","zone":"Bangalore Central","url":"http:\/\/www.littleelly.com\/pre-school-in-indiranagar\/","address":"# 289, 6th Main, 4th Cross, HAL 3rd Stage, Indiranagar(Near Dr.Balliga\\'s Children\\'s Clinic), Bangalore-560075"},{"locationId":"4","location":"Tavarekere","cdName":"Shagufta","hdName":"Usha Gudi","email":"stjohns@littleelly.com","mobile":"9611833411","zone":"Bangalore Central","url":"http:\/\/www.littleelly.com\/pre-school-in-taverekere\/","address":"#56, Cauvery Layout, (Nr St. Johns Woods Apts), Tavarekere Main Road, Bangalore-560029"},{"locationId":"5","location":"Koramangala","cdName":"Shagufta","hdName":"Usha Gudi ","email":"le_koramangala@littleelly.com","mobile":"9742381677 , 080-41283222","zone":"Bangalore Central","url":"","address":"#742, 7th Cross, Near BDA Complex, 3rd Block Koramangala, Bangalore-560034"},{"locationId":"6","location":"AECS Layout","cdName":"Gaargi","hdName":"Nirmala Vishwanathan","email":"firstline@littleelly.com","mobile":"9886024239","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-aecs-layout\/","address":"338, B Block, AECS Layout, Kundalahalli,Land Mark: Lane between Nilgiris & Sansar supermarket, Opp Krishna Brookefields Appts, Bangalore-560037"},{"locationId":"7","location":"Agara Lake","cdName":"Ms. Chaitra","hdName":"Mr. Prathyush A.V","email":"agara@littleelly.com","mobile":"9686936852","zone":"Bangalore East","url":"","address":" # 215, Sarjapura Road, Near HDFC Bank, Agara, Bangalore 560102"},{"locationId":"8","location":"Bellandur","cdName":"Saroja","hdName":"Saroja","email":"priyadarshini@littleelly.com","mobile":"7338651909 \/ 080-41532420","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-bellandur","address":"Opp. Trinity Meadows, Near Sarjapur Ring Road, Bellandur Post Office, Bangalore-560103"},{"locationId":"9","location":"C.V.Raman Nagar","cdName":"Ruby Sankar","hdName":"Ruby Sankar","email":"cvramannagar@littleelly.com","mobile":"9148046607","zone":"Bangalore East","url":"","address":"29th cross, Balaji Layout ,Kaggadaspura ,CV Raman nagar ,Bangalore - 560093"},{"locationId":"10","location":"Sarjapur - Outer Ring Road","cdName":"Kavitha","hdName":"Surbhi kapadia","email":"sreejaneducation@littleelly.com","mobile":"9686037281 \/ 080- 40946827","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-sarjapur-orr\/","address":"Next to Sobha Jasmine, Green Glen Layout, Sarjapur - Outer Ring Road Junction, Bangalore-560103"},{"locationId":"11","location":"Yamare","cdName":"Ms. Sumiti Malviya","hdName":"Ms. Sumiti Malviya","email":"yamare@littleelly.com","mobile":"7829663000 \/ 7619368999","zone":"Bangalore East","url":"","address":"# 34, Yamare Village, Dommasandra post, Anekal Taluk, Bangalore- 562125"},{"locationId":"12","location":"HSR Layout","cdName":"Supriya Caroline","hdName":"Supriya Caroline","email":"hsr@littleelly.com","mobile":"7760507038 \/ 7259916491","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-hsr-layout\/","address":"No.23, 18th Cross, Near BDA Complex HSR Layout 7th Sector, Bangalore-560102"},{"locationId":"13","location":"H.S.R Layout Extension","cdName":"Priya","hdName":"Rajee Kartik","email":"firststep@littleelly.com","mobile":"9740321352","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-hsr-extension\/","address":"86, S.No. 29\/2A, 29\/9, Haralakunte, Royal Placid Phase 3, Somasundarapalya, H.S.R Layout Extension, Bangalore 560102"},{"locationId":"14","location":"Marathahalli\/Karthik Nagar","cdName":"Nirmala Vishwanathan","hdName":"Nirmala Vishwanathan","email":"ascot@littleelly.com","mobile":"9148570140 \/ 080 65607268","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-karthik-nagar\/","address":" #48, LRDE Layout, Karthik Nagar, Outer Ring Road, Opposite HDFC Bank, Behind BAWARCHI, Marathahalli, Bangalore - 560037"},{"locationId":"15","location":"Whitefield","cdName":"Ranjitha","hdName":"Yagna","email":"whitefield@littleelly.com","mobile":"9901014000","zone":"Bangalore East","url":"","address":"A-110, Victorian View, Borewell Road, Nelurhalli, Whitefield, Bangalore-560066"},{"locationId":"16","location":"Kasavanahalli","cdName":"Priyanka","hdName":"Mouchhanda Mitra","email":"kasavanahalli@littleelly.com","mobile":"9886779756 \/ 9845316253","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-kasavanahalli\/","address":" #12\/A Tulsi Layout, Kasavanahalli Village Near La Casa Hotel , Bangalore 560035"},{"locationId":"17","location":"Pai Layout","cdName":"Poonam","hdName":"Poonam","email":"pjoshi@littleelly.com","mobile":"9740327207","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-pai-layout\/","address":"#444, 14th,A Cross, 1st Main, Pai Layout, Old Madras Road, K.R. Puram, Bangalore 560016"},{"locationId":"18","location":"Mahadevapura","cdName":"Ruchi","hdName":"Nazneen.N","email":"mahadevapura@littleelly.com","mobile":"8861515563","zone":"Bangalore East","url":"","address":"No 84, R.H.B Colony, 1st Main, Mahadevapura Post, Bangalore 560048"},{"locationId":"19","location":"Panathur","cdName":"Taizeen Roy","hdName":"Taizeen Roy","email":"riu@littleelly.com","mobile":"8971517765","zone":"Bangalore East","url":"","address":"#127, NISARGA 2nd Main, Panathur Main Road, Kadabisanahalli Marthahalli Outer Ring Road,Bellandur Post, Bangalore - 560 103"},{"locationId":"20","location":"Munnekolala","cdName":"Hima Bindu","hdName":"Yagna","email":"munnekolala@littleelly.com","mobile":"9972100117","zone":"Bangalore East","url":"","address":"No-42, Karthika, 13th Cross, Manjunatha Layout, Shirdi Sai Nagar, Munnekolala, Kundanhalli Gate (1st Parallel Road Before Sai Baba Temple),Bangalore-560037"},{"locationId":"21","location":"Basavanagar","cdName":"Ruby","hdName":"Ruby Sankar","email":"basavanagar@littleelly.com","mobile":"7259422111","zone":"Bangalore East","url":"","address":"13, 5th cross, Shankarappa Layout, Near Tata Sherwood, Basavanagar ,Bangalore - 560037"},{"locationId":"22","location":"K.R.Puram","cdName":"Ishwarya","hdName":"Prathap Yadav","email":"krpuram@littleelly.com","mobile":"9611943399","zone":"Bangalore East","url":"","address":"#33,Royal chambers,Devasandra Main Road,K.R.Puram,Bangalore-560036\r\n(Opp to New Manipal Hospital )"},{"locationId":"23","location":"Maruti Garden \/ Sarjapur Road - Wipro","cdName":"Leena Khemani","hdName":"Leena Khemani","email":"marutigarden@littleelly.com","mobile":"9845316253 \/ 9886779756","zone":"Bangalore East","url":"http:\/\/www.littleelly.com\/pre-school-in-maruti-garden\/","address":"No 5, Blessing Dale, Maruthi Garden, Opp Rainbow Residency, Sarjapur Road (Landmark WIPRO Gate, Sarjapur), Bangalore 560035"},{"locationId":"24","location":"B-Narayanapura","cdName":"sirisha.j","hdName":"sirisha.j","email":"bnpura@littleelly.com","mobile":"7829624553","zone":"Bangalore East","url":"","address":"# 4 SRR Layout, Behind Pragathi school, Near to JK cricket academy. B.Narayanapura Bangalore-560016"},{"locationId":"25","location":"Sadanandanagar","cdName":"Smija","hdName":"Smija","email":"ngef@littleelly.com","mobile":"9739453516","zone":"Bangalore East","url":"","address":"# 187 Sadanandanagar, 4th main, NGEF layout, Bangalore 560038"},{"locationId":"26","location":"Naganathapura","cdName":"Hema","hdName":"Durgambika","email":"naganathapura@littleelly.com","mobile":"9916097974","zone":"Bangalore East","url":"","address":"#39,Mookambika villa,4th main,Opp KSRP Venkateshwara layout, Naganathapura Bangalore 560100 .\r\n(Landmark:Near NEO supermarket)"},{"locationId":"27","location":"Kadugodi","cdName":"Swarna","hdName":"Swarna","email":"kadugodi@littleelly.com","mobile":"9900431987","zone":"Bangalore East","url":"","address":"# 30 A, Mother\\'s, belathur, Kadugodi, ( Adjacent to AAXIS Hospitals) Bangalore . Pin : 560067\r\n"},{"locationId":"28","location":"Varthur","cdName":"Gayathri","hdName":"Gayathri","email":"varthur@littleelly.com","mobile":"9611016699","zone":"Bangalore East","url":"","address":"#4\/5A, 10th Cross, Patel Layout, Balagere Road, Varthur, Bangalore -560087.Landmark: Near KK Silks"},{"locationId":"29","location":"Banaswadi","cdName":"Dhanalakshmi","hdName":"Dhanalakshmi Shekar","email":"banaswadi@littleelly.com","mobile":"9900251351 \/ 080 25454549","zone":"Bangalore North","url":"","address":"No.4, 9th Main, HRBR Layout, 1st Block, Near Banaswadi Fire Station, Banaswadi, Bangalore-560043"},{"locationId":"30","location":"HRBR Layout","cdName":"Leena","hdName":"Leena Nair","email":"le_hrbr@littleelly.com","mobile":"9972044900 \/ 080-25438442 ","zone":"Bangalore North","url":"","address":"314, 4th A Cross, 3rd Block, Near Jalavayu Vihar, HRBR Layout, Bangalore-560043"},{"locationId":"31","location":"Marappa Garden","cdName":"Ambareena","hdName":"Nausheen","email":"bensontown@littleelly.com","mobile":"8867614977 \/ 9901968047","zone":"Bangalore North","url":"","address":"1st cross, Marappa Garden, Behind Airtel Office, Benson Town post, Bangalore- 560046"},{"locationId":"32","location":"RMV Extension","cdName":"Vaishnavi Vivek","hdName":"Vaishnavi Vivek","email":"le_rmv@littleelly.com","mobile":"99721 88369 \/ 080 23602124","zone":"Bangalore North","url":"","address":"No. 34, ITI Layout,  RMV Extension 2nd Stage, New BEL Road,  Near MS Ramiah Hospital, Bangalore-560054"},{"locationId":"33","location":"LE Preschool kothanur(Gubbi Cross)","cdName":"Ms. Ambika","hdName":"Ms. Sujatha","email":"kothanur@littleelly.com","mobile":"7618781414 \/ 9535227535","zone":"Bangalore North","url":"","address":"#43\/2A, Divine Enclave, Opp. Gold Summit, Gubbi Cross, Kothanur, Bangalore - 560077"},{"locationId":"34","location":"Bhuvaneshwari Nagar","cdName":"Ms.Nethra","hdName":"Ms.Nethra","email":"bhuvaneshwari@littleelly.com","mobile":"9108521854 \/ 9663768359","zone":"Bangalore North","url":"","address":"#124, Basil , Behind Union Bank ATM, 2nd Cross, Bhuvaneshwari Nagar, Hebbal, Bangalore-560024"},{"locationId":"35","location":"RT Nagar","cdName":"Anjali","hdName":"Madhurima SK Karur","email":"rtnagar@littleelly.com","mobile":"7618773987\/9945530607","zone":"Bangalore North","url":"http:\/\/www.littleelly.com\/littleelly-RT-nagar\/","address":"Address : # 21, 3rd Main Road, SBM PVT Layout Anandnagar, (Next to State Bank of India),  Hebbal, Anandnagar Post, Bangalore-560032"},{"locationId":"36","location":"Sahakar Nagar","cdName":"Rukmini & Kavya","hdName":"Mrs. Rukmini H .V","email":"sahakar@littleelly.com","mobile":"9972173477","zone":"Bangalore North","url":"","address":"#1018, 12th Main,B, Block, Sahakar Nagar, Bangalore-560092"},{"locationId":"37","location":"Frazer Town","cdName":"Usha","hdName":"Usha Mathew","email":"infant@littleelly.com","mobile":"9632082933, 08043006785","zone":"Bangalore North","url":"","address":"#1\/2, Gover Road, (Near Bangalore South Indian Bank), Cox Town, Bangalore-560005"},{"locationId":"39","location":"Jakkur","cdName":"Ms.Richa","hdName":"Mrs.Sharadha Shivshankar","email":"jakkur@littleelly.com","mobile":"9980912211 \/ 7019085613","zone":"Bangalore North","url":"","address":"# 2\/1, 5th cross, Navya nagar, Jakkur, Bangalore - 560064"},{"locationId":"40","location":"Hennur","cdName":"Neha","hdName":"Neha","email":"hennur@littleelly.com","mobile":"9845061001","zone":"Bangalore North","url":"","address":"House No 23, Hennur Enclave, Geddalahalli, Hennur Road, (near Bangalore International School) Bangalore -560077"},{"locationId":"41","location":"HBR Layout","cdName":"Ummal","hdName":"","email":"hbr@littleelly.com","mobile":"8884894101","zone":"Bangalore North","url":"","address":"# 1282 Hbr 1st stage 36th Cross , 5th Block , HBR Layout \r\nBangalore \u2013 560043 \r\n"},{"locationId":"42","location":"Thanisandra","cdName":"Ramya Barna","hdName":"Ramya Barna","email":"thanisandra@littleelly.com","mobile":"8105848668","zone":"Bangalore North","url":"","address":"#6, 1st Main road, Shabari Nagar Layout, R K Hegde Nagar,Bengaluru, Karnataka 560077"},{"locationId":"43","location":"Jalahalli","cdName":"Jacintha","hdName":"Tresa","email":"jalahalli@littleelly.com","mobile":"9945390406","zone":"Bangalore North","url":"","address":"#16, Ashwini villa, Near Gangamma Temple, Jalahalli Bangalore 560013"},{"locationId":"44","location":"Tindlu","cdName":"Chandrakala","hdName":"Chandrakala","email":"tindlu@littleelly.com","mobile":"9986052233\/9986074348","zone":"Bangalore North","url":"","address":"#239, 1st Cross, Govindappa Building, Near Tindlu Circle, Tindlu, Vidyaranyapura Post, Near Harihara Medicals, Bangalore- 560097"},{"locationId":"45","location":"RBI Layout","cdName":"Kanchan","hdName":"Mrs.Veeradhi Kalpana","email":"rbilayout@littleelly.com","mobile":"9845414573","zone":"Bangalore South","url":"","address":"# 371, 7th main, RBI Layout, (opposite Anjanaya Temple), J.P.Nagar 7th Phase Bangalore \u2013 560078"},{"locationId":"46","location":"Gottigere","cdName":"Shilpa Kurian","hdName":"Shilpa Kurian","email":"gottigere@littleelly.com","mobile":"9844415830","zone":"Bangalore South","url":"","address":"27 South Avenue layout, Gottigere Post, B.G Road,Bangalore-560083"},{"locationId":"47","location":"LITTLE ELLY \u2013 AGS Layout (Uttarahalli)","cdName":"Sumalatha","hdName":"Sumalatha","email":"agslayout@littleelly.com","mobile":"8277106405","zone":"Bangalore South","url":"","address":"No 74,7th main, Sri sai nagar, Nanda Kumar layout, \r\nArehalli, near chaithanya techno school,Subramanyapura post ,Bangalore - 560061"},{"locationId":"48","location":"Electronic City","cdName":"Mrs Sarika Madhan","hdName":"K. Diana","email":"ecity@littleelly.com","mobile":"9036020060","zone":"Bangalore South","url":"","address":"#29, ORCHIDS PARK, Opposite Genesis Ecosphere ,Neeladri Road, Neeladri Nagar, Dodda Thogur, Electronic City Phase 1,Electronic city, Bangalore - 560100"},{"locationId":"49","location":"Banashankari 2nd Stage","cdName":"Vasundara Hegde","hdName":"Anusha.C.K","email":"bsk2ndstage@littleelly.com","mobile":"9632315123\/9611236099","zone":"Bangalore South","url":"","address":"#1774, 15th Main Road, Near Meghna & Shalini Apartments, Banashankari 2nd Stage, Bangalore-560070"},{"locationId":"50","location":"Banashankari 3rd Stage","cdName":"Smitha.R","hdName":"Kavana Madyasta","email":"bsk3rdstage@littleelly.com","mobile":"9113908945","zone":"Bangalore South","url":"http:\/\/www.littleelly.com\/preschool-in-banashankari\/ ","address":"# 362, 4th Main, 2nd Block, ( Opp to ks Narasimha Swamy Park) Banashankari 3rd stage Bangalore 560085"},{"locationId":"51","location":"BTM Layout","cdName":"Padma","hdName":"Nagashree Pattabhi","email":"shreeworld@littleelly.com","mobile":"9880956343","zone":"Bangalore South","url":"http:\/\/www.littleelly.com\/pre-school-in-btmlayout\/","address":"787\/ A, 4th Cross, 15th Main, BTM 2nd Stage, Near Udupi Garden, Bangalore-560046"},{"locationId":"52","location":"Nobo Nagar","cdName":"Ekta Mitra \/ Sreedevi Ravichand","hdName":"Ekta Mitra \/ Sreedevi Ravichand","email":"pragyaam@littleelly.com","mobile":"9945863404 \/  9945687117","zone":"Bangalore South","url":"http:\/\/www.littleelly.com\/pre-school-in-hulimavu\/","address":"No. 41, Kalena Agrahara,2nd Main, 2nd Sector, Nobonagar, Bannerghatta Road,Bangalore - 560076.Landmark : Near Meenakshi Temple"},{"locationId":"53","location":"Jayanagar","cdName":"Padma","hdName":"Sandhya Kadam","email":"neytij@littleelly.com","mobile":"9886747509","zone":"Bangalore South","url":"","address":"248\/17, 6th Cross, 2nd Block Jayanagar, Bangalore \u2013 560011"},{"locationId":"54","location":"Narayana Nagar","cdName":"Vidya DP","hdName":"Chandresta Maity","email":"narayananagar@littleelly.com","mobile":"7411675042","zone":"Bangalore South","url":"","address":"#441,100ft road,Narayana nagar 3rd block,Doddakallasandra(Near konankunte lake),Off Kanakapura Road,Bangalore-560062"},{"locationId":"55","location":"J.P Nagar 9th Phase","cdName":"Gowravi","hdName":"Mrs. Gowravi Keshav","email":"jpnagar9@littleelly.com","mobile":"9980139139","zone":"Bangalore South","url":"","address":"16, Behind RLF club, Royal lake Front Residency 3rd Phase , Raghavana Palya, J.P Nagar 9th Phase. Bangalore- 560062"},{"locationId":"57","location":"J.P. Nagar 5th Phase","cdName":"Payal","hdName":"Tejaswini Yogish","email":"marigold@littleelly.com","mobile":"9980100666","zone":"Bangalore South","url":"http:\/\/www.littleelly.com\/pre-school-in-jp-nagar-5th-phase\/","address":"#140\/2, 18th Main, 19th A cross, Narasimaiah Garden, J.P. Nagar 5th Phase, Bangalore-560078"},{"locationId":"58","location":"Kanakapura Road","cdName":"Gouri","hdName":"Gouri Datta","email":"kproad@littleelly.com","mobile":"9902071619","zone":"Bangalore South","url":"","address":"572 7th main , Balaji housing society ,Kanakapura road  , Bangalore 560062"},{"locationId":"59","location":"Vijaya Bank Layout","cdName":"Sneha","hdName":"Sapna","email":"wonderkids@littleelly.com","mobile":"9036148007","zone":"Bangalore South","url":"","address":"510 A, 5th Main, Near Sri Ayyappa Swami Temple, Vijaya Bank Layout, Bannerghatta Road, Bangalore-560076"},{"locationId":"61","location":"Banashankari 1st Stage","cdName":"Shobha","hdName":"Sobha Shivashankar Bhat","email":"peeta@littleelly.com","mobile":"8792771757","zone":"Bangalore South","url":"","address":"#723\/93, 8th main, 3rd cross, BSK 1st stage, 2nd Block,Hanumanthnagar, Near Webster school, 6th cross, Bangalore 560050"},{"locationId":"62","location":"Singasandra","cdName":"Rohini","hdName":"Uday","email":"singasandra@littleelly.com","mobile":"9886621947","zone":"Bangalore South","url":"","address":"#113\/1, Opp. Manipal County Club Singasandra, Bangalore-560068"},{"locationId":"63","location":"Akshaya Nagar","cdName":"Padma","hdName":"Padma R. Singh","email":"akshaya@littleelly.com","mobile":"9945127844","zone":"Bangalore South","url":"","address":"No.26, Akshaya Garden, Akshaya Nagar, Begur Hobli Road, Off Bannerghatta Road (Near DLF and Hiranandani Project), Bangalore-560068"},{"locationId":"64","location":"Begur Road","cdName":"Preeti Maneesh","hdName":"Preeti Maneesh","email":"begurroad@littleelly.com","mobile":"9620684335","zone":"Bangalore South","url":"http:\/\/littleelly.com\/littleelly-Begur-Center\/","address":"454, Karunya Dham, Opp SNN Raj serenity, Nobel Residency, Off Bannerghatta Road. Bangalore- 560076"},{"locationId":"66","location":"ISRO Layout","cdName":"Tinssy","hdName":"Tinssy","email":"isro@littleelly.com","mobile":"9620055816, 7899425064","zone":"Bangalore South","url":"","address":"No. 24 , 3rd Main road, Bikasipura, ISRO Layout, Off Kanakapura Road,Bangalore- 560078"},{"locationId":"67","location":"Bull Temple Road","cdName":"Sunitha","hdName":"Mrs. Vasundra","email":"bulltempleroad@littleelly.com","mobile":"9964393817","zone":"Bangalore South","url":"","address":"#119, Nagaraj Layout\r\nBull Temple Road Cross\r\nBengaluru -560019"},{"locationId":"69","location":"Gubbalala","cdName":"Ms. Priyadarshini","hdName":"Ms. Priyadarshini","email":"gubbalala@littleelly.com","mobile":"+916360818599 \/ 9886540578","zone":"Bangalore South","url":"","address":"#27, 2nd Cross, 2nd Main, Raghvendra Layout, Near Jain Swadesh Apartment, Gubbalala Main Road,Off Kanakapura Road, Bangalore - 61"},{"locationId":"70","location":"Arekere","cdName":"Sindhu M. Naidu ","hdName":"Ekta Mitra \/ Sreedevi Ravichand","email":"arekere@littleelly.com","mobile":"7619217778 \/  9945687117 \/ 080- 48521041","zone":"Bangalore South","url":"http:\/\/www.littleelly.com\/pre-school-in-l-t-south-city\/","address":"No.154,Mico Layout, 1st stage, 2nd cross,Bangalore - 560076.Landmark : Near Ganesha Temple"},{"locationId":"72","location":"Chandra Layout","cdName":"Kusum","hdName":"Kusum","email":"Chandralayout@littleelly.com","mobile":"7760532222","zone":"Bangalore WEST","url":"","address":"#1434, 2nd Cross, Kali Temple Road, Chandra Layout, Bangalore \u2013 560040"},{"locationId":"73","location":"Basaveshwarnagar","cdName":"Aishwarya","hdName":"Aishwarya.G","email":"basava@littleelly.com","mobile":"9731098877","zone":"Bangalore WEST","url":"","address":"#949, 8th C Main, 3rd Stage, 3rd Block, Basaveshwaranagar Bangalore - 560079"},{"locationId":"77","location":"Rajarajeshwari Nagar","cdName":"Veena","hdName":"Veena","email":"rrnagar@littleelly.com","mobile":"9972195622","zone":"Bangalore WEST","url":"","address":"# 129, Srishiti, 60 Feet Road, 4th main, BEML 5th stage, Rajarajeshwari Nagar, Bangalore 560098"},{"locationId":"78","location":"Little Elly Pre-School, Kolathur","cdName":"Radha","hdName":"Radha","email":"kolathur@littleelly.com","mobile":"+91 9677277789","zone":"Chennai","url":"","address":"4\/20,Kumaran Street,Vivekananda Main Road (Opposite to Reliance Fresh Shop), Kolathur,Chennai - 600099"},{"locationId":"81","location":"Little Elly Pre-School, Nolambur","cdName":"Kaleeshwari","hdName":"AKSHARA K.K","email":"nolambur@littleelly.com","mobile":" 9840374688","zone":"Chennai","url":"","address":"# 3, (Plot No.10), Jawaharlal Nehru Street, Opp. to Jains Sunderbans Apartment V.G.N Nagar, Phase-4, Nolambur, Mogappair West, Chennai - 600095"},{"locationId":"82","location":"Little Elly Pre-School, Porur","cdName":"Sowmya Raga","hdName":"Sowmya Raga","email":"porur@littleelly.com","mobile":"9791073441","zone":"Chennai","url":"","address":"Plot No 1, Ramachandra Nagar, Madanandapuram, Porur (Behind Ramachandra Commercial Complex), Chennai 600125"},{"locationId":"85","location":"Little Elly Pre-School, Velachery","cdName":"Mamta Rao","hdName":"Sowmya raga","email":"velachery@littleelly.com","mobile":"9962007111","zone":"Chennai","url":"","address":"No 105, Murugu nagar extension, Velachery, Chennai - 600042, Tamil Nadu"},{"locationId":"86","location":"Little Elly Pre-School, perungudi","cdName":"Deepika","hdName":"Sowmya raga","email":"perungudi@littleelly.com","mobile":"9884607111","zone":"Chennai","url":"","address":"20, 6th cross street, Kurunji Nagar, Near Green Acre Apartments, Perungudi Chennai- 600096"},{"locationId":"87","location":"Little Elly Preschool, Besant Nagar","cdName":"Dr.Rekha Pillai","hdName":"Dr.Rekha Pillai","email":"besantnagar@littleelly.com","mobile":"93846 99490","zone":"Chennai","url":"","address":"No 14, 29 th cross street, Kalashethra colony, Besant Nagar, Chennai - 600090"},{"locationId":"91","location":"Little Elly Pre-School, Ashok Nagar (Mangalore)","cdName":"Anitha Pinto","hdName":"Mrs. Rashmi Dheeraj","email":"Ashoknagar@littleelly.com","mobile":"8453277799","zone":"Mangalore","url":"","address":"# 34, Star House, Near St. Domnic Church, Ashok Nagar, Urwa, Mangalore - 575001"},{"locationId":"93","location":"Little Elly Pre-School, Bavdhan (pune)","cdName":"Pooja","hdName":"Vivek Shesh","email":"bavdhan.pune@littleelly.com","mobile":"8308331700","zone":"Pune","url":"","address":"#63, Geetai, Sagar Society, lane opp. Bank of Maharashtra, Bavdhan, Pune 411021"},{"locationId":"94","location":"Little Elly Pre-School, Coimbatore","cdName":"Pradeep","hdName":"N.Pradeep","email":"coimbatore@littleelly.com","mobile":"9578395000","zone":"others","url":"","address":"17, Sivasakthi Nagar, Thudiyalur Road, Saravanampatti, Between KGISL & KCT, Coimbatore-641035"},{"locationId":"95","location":"Little Elly Pre-School, Hosur (Tamil Nadu)","cdName":"Ms. Rajalakshmi","hdName":"Mr.Mahesh Krishna\/Ms.Bharati Krishna","email":"hosur@littleelly.com","mobile":"7200592719","zone":"others","url":"","address":"H - 57, 9th Phase, Housing Board, (Opp Railway Station),Krishnagiri Dist, Hosur 635109"},{"locationId":"97","location":"Little Elly Pre-School, kadri(mangalore)","cdName":"Spoorti Hegde","hdName":"Spoorti Hegde","email":"kadri@littleelly.com","mobile":"7829723586","zone":"Mangalore","url":"","address":"Blue Moon, Alvares Road Kadri Kaibattal, Mangalore-575002"},{"locationId":"98","location":"Little Elly Pre-School, Kaup (Udupi)","cdName":"Pavana Hegde","hdName":"Mrs.Vedita","email":"kaup@littleelly.com","mobile":"8884272233","zone":"Mangalore","url":"","address":"Padma Sundara, Near Kallilabeedu, Inanje Road, Kaup, Udupi district 574106"},{"locationId":"99","location":"Little Elly Pre-School, Tirupathi (Andhra pradesh)","cdName":"Vaishnavi","hdName":"Vaishnavi","email":"tirupathi@littleelly.com","mobile":"7675931166","zone":"others","url":"","address":"# 4\/74, ground floor, Opp panchayathi office, Padmavathi puram, Tiruchanur road, Tirupathi,Andhra pradesh-517503"},{"locationId":"100","location":"Little Elly Pre-School, Bharuch","cdName":"Richa Desai","hdName":"Richa Desai","email":"bharuch@littleelly.com","mobile":"7096884471","zone":"others","url":"","address":"2\/A, Gurukrupa Society Opp Rudraksh Residency Avdhutnagar- 2, Bholav Bharuch- 392001"},{"locationId":"101","location":"Little Elly Pre-School, Gokulam ( Mysore)","cdName":"Lakshmi","hdName":"Laxmi","email":"gokulam@littleelly.com","mobile":"7676662677","zone":"Mysore","url":"","address":"965, 7th main 3rd stage, Doctors Corner, Gokulam, (Opp to Mysore one Gokulam), Mysore- 570002"},{"locationId":"103","location":"Little Elly Pre-School, Vijayapur","cdName":"Shweta Patil","hdName":"Suresh \/ Akash \/ Manthesh \/ Shisher patil","email":"vijayapur@littleelly.com","mobile":"7899451117","zone":"others","url":"","address":"BLDE Engineering College,Anand Nagar,Ashram Road, Vijayapur 586103"},{"locationId":"108","location":"Little Elly Pre-School, Pallikaranai","cdName":"Venkata Lakshmi","hdName":"Venkata Lakshmi","email":"pallikaranai@littleelly.com","mobile":"75500 77040","zone":"Chennai","url":"","address":"Plot No. 73, Ram Nagar South Extension, 3rd Street, Pallikaranai, Tamil Nadu 600100"},{"locationId":"109","location":"Yelahanka New Town","cdName":"Sharmila","hdName":"Maheshwar. B","email":"yelahankant@littleelly.com","mobile":"9972039356 \/ 080-41219128","zone":"Bangalore North","url":"","address":"Little Elly, No. B4-119, SFS  208, 2nd cross, 2nd Main, 4th Phase, Yelahanka New Town, Bangalore-560064"},{"locationId":"110","location":"Little Elly Pre-School, Malakpet","cdName":"Fareesa","hdName":"Fareesa Roohi","email":"malakpet@littleelly.com","mobile":"9515447307 \/ 9515447321","zone":"Hyderabad","url":"","address":"H.No. 16-8-830 Near Hockey Ground, Azampura Bridge New Malakpet, Hyderabad TELANGANA - 500036"},{"locationId":"111","location":"Little Elly Pre-School, Bannimantap (Mysore)","cdName":"Saritha\/Seema","hdName":"Saritha\/Seema","email":"bannimantap@littleelly.com","mobile":"9886049595 \/ 9886039595","zone":"Mysore","url":"","address":"Star House, New Sayyaji Rao Road, Bannimantap, Mysore - 570001"},{"locationId":"114","location":"Little Elly Pre-School, Ramakrishna Nagar (Mysore)","cdName":"Shilpa","hdName":"Shilpa","email":"prajnaedu@littleelly.com","mobile":"919060157020 \/ 0821-4257020","zone":"Mysore","url":"","address":"HIG # 1\/ 18 A ( New -146), Udaya Ravi Road , Near Andolana Circle, E & F Block, Ramakrishna Nagar , Mysore - 570022"},{"locationId":"115","location":"Little Elly PreSchool, Pal,Surat (Gujarat)","cdName":"Ms. Ankita","hdName":"Ms. Shruthi","email":"adajan@littleelly.com","mobile":"+91 89800 09922","zone":"Surat","url":"","address":"12, Shanti Sagar Row House,Opp.Western Heights &GEB substation,Pal Road, Adajan,Surat-395009"},{"locationId":"119","location":"Ananth Nagar ( Electronic City )","cdName":"Diana. K","hdName":"Diana. K","email":"ananthnagar@littleelly.com","mobile":"9036020060","zone":"Bangalore South","url":"","address":"# 203, Saptagiri, 15th Cross 4th Main Ananth Nagar phase 1, Electronic City , Bangalore: 560100"},{"locationId":"122","location":"Little Elly Preschool-Vesu, Surat,Gujarat","cdName":"Drashti Desai","hdName":"Vinita Agrawal \/ Nehal Akbari","email":"vesu.surat@littleelly.com","mobile":"8980033380","zone":"Surat","url":"","address":"7\/Pratik bungalows, near sun chariot & nandanwan appt - 1, Vesu, Surat,Gujarat - 395007"},{"locationId":"123","location":" Little Elly- Pragathinagar","cdName":"Mrs. Dhanalaxmi","hdName":"Mrs. Dhanalaxmi","email":"pragathinagar.hyd@littleelly.com","mobile":"9666687557","zone":"Hyderabad","url":"","address":"Little Elly-Pragathinagar\r\nPlot  #565\r\nAbove \\'Jayashree Clinic\\'\r\nNear Gram Panchayat office\r\nPragathinagar \r\nHyderabad"},{"locationId":"124","location":"Little Elly Pre-school,Vanagaram","cdName":"Ms. Praveena","hdName":"Ms. Praveena","email":"vanagaram@littleelly.com","mobile":"8056120629","zone":"Chennai","url":"","address":" #17, Pillaiyar Koil st, Heritage Venkateshwara Nagar,\r\nVanagaram, Porur Gardens, Chennai-600116\r\n"},{"locationId":"126","location":"Neo Town-Electronic City","cdName":"Vijeta","hdName":"Vijeta","email":"neotown@littleelly.com","mobile":"7338365777","zone":"Bangalore South","url":"http:\/\/littleelly.com\/littleelly-Neotown-Center\/","address":"No 21\/3, Thirupalya Road, Electronic city phase 1, Near Neo Town, Next to GM Infinite E-City, Bangalore-560099"},{"locationId":"127","location":"Little Elly pre-school, Kuthar, (Mangalore)","cdName":"Kavitha B Das","hdName":"Kavitha B Das","email":"kuthar@littleelly.com","mobile":"8147848597","zone":"Mangalore ","url":"","address":"Near Ajjana Katte ,Kuthar ,Mangalore (575017)"},{"locationId":"128","location":"LIttle Elly Ambattur ","cdName":"Pratibha VR","hdName":"Pratibha VR","email":"ambattur@littleelly.com","mobile":"8939879977, Land Line :044-48679977","zone":"LIttle Elly Ambattur ","url":"http:\/\/littleelly.com\/littleelly-Ambattur-Center\/ ","address":"No 28, door no 1\/1A surya prakasham street krishnapuram Ambattur , chennai - 53"},{"locationId":"129","location":" Little elly Kengeri","cdName":"Haritha","hdName":"Haritha","email":"kengeri@littleelly.com","mobile":"9945733245","zone":" Bangalore North","url":"","address":"House # 1031, 2nd Parallel Railway Station Road, BDA Layout, Kengeri Satellite Town, Bangalore - 560060"},{"locationId":"130","location":"J.P. Nagar 4th Phase ","cdName":"Rupal","hdName":"Ms. Tejaswini Yogish","email":"marigold@littleelly.com","mobile":"9980100666","zone":"Bangalore South","url":"","address":"J.P. Nagar 4th Phase\r\n#157, Aishwarya, in front of chinmayya mission. 4th cross, JP nagar 4th phase. Bangalore-78"},{"locationId":"131","location":"IISc","cdName":"Jayashree","hdName":"Vaishnavi Vivek","email":"iisc@littleelly.com","mobile":"7338337868 \/ 997218836","zone":"Bangalore Central","url":"","address":"Bunglow No.10, Gulmohar Marg,\r\n Indian Institute of Science, Bangalore-560012"},{"locationId":"132","location":"JSW-Vidyanagar","cdName":"Rohini","hdName":"Rohini","email":"jsw_vidyanagar@littleelly.com","mobile":"9036065699","zone":"Bellary","url":"","address":"Vidyanagar Township,near health care center,foodcourt,Torangallu,Bellary-583275,Karnataka"},{"locationId":"133","location":"Vengaivasal","cdName":"Ms. Bindu Venkatesh","hdName":"Ms. Bindu Venkatesh","email":"vengaivasal@littleelly.com","mobile":"8807030200","zone":"chennai","url":"","address":"# 9\/325, Indra Nagar, 4th Cross Street, Vengaivasal, Chennai - 600126"},{"locationId":"134","location":"Little Elly Preschool Kathmandu","cdName":"","hdName":"Kamanathapa","email":"kathmandu@littleelly.com","mobile":"9851243838 \/ 01- 4015637","zone":"Nepal","url":"","address":"Annapurna Marg,Chappal Kharkhana,Kathmandu Nepal"},{"locationId":"136","location":"Palavakkam","cdName":"Suryavalli Mannepalli","hdName":"Suryavalli Mannepalli","email":"suryavalli2015@gmail.com & palavakkam@littleelly.com","mobile":"9840535288\/9840471288","zone":"Chennai","url":"","address":"No 1\/15, Kamber Street, Palavakkam,Chennai - 600041.Landmark :Near Palavakkam ( Madras ) University Arch."},{"locationId":"137","location":"Little Elly Bangalore Palace","cdName":"Ms. Vaishali","hdName":"Ms. Latha","email":"prasiddhi@littleelly.com","mobile":"9972527072","zone":"Bangalore","url":"http:\/\/www.littleelly.com\/locate-center\/","address":"Palace Compound, Palace Grounds Inner Rd, Vasanth Nagar, Bengaluru, Karnataka 560052"},{"locationId":"138","location":"Carmelaram","cdName":"Ms. Leena \/ Ms. Mouchhanda","hdName":"Ms. Leena \/ Ms. Mouchhanda","email":"carmelaram@littleelly.com","mobile":"9845316253 \/ 9886779756","zone":"Bangalore Central","url":"http:\/\/www.littleelly.com\/locate-center\/","address":" # 2 & # 3, Thomas Layout, Hadosiddapura, Sarjapura Road, Behind RGA Tech Park, Bangalore- 560035"}];

	        $scope.filterState = function(state, activeid) {
	            $scope.searchState = state;
	        }

	})
	.directive('checkIfActive', function() {
	    return {
	        restrict : 'A',
	        controller : 'SearchCtrl',
	        link : function(scope, element, attr) {
	            element.bind('click', function(event) {
	                $('.nav li').removeClass('active');
	                element.addClass('active');
	            });   
	        }
	    }
	});
	
	</script> -->
	