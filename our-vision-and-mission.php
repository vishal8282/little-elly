<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Our Vision & Mission</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Our Vision & Mission</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- About Us -->
<!-- History Section -->
<section class="history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <h2>Our <span>Vision</span></h2>
                    <div class="text">To be the trendsetter and path paver in early childhood education
                        We believe that learning is a continuous and lifelong process. Every child achieves it at his
                        or her own pace. The child’s ability to learn and grow is unique and special. We ensure that we
                        provide each child with proper guidance to grow in his/her own way, in a mutually accepting and
                        nurturing setting.
                        Each child is guided into building a strong foundation of knowledge and skills. We believe in
                        making teaching and learning a harmonious process. Our stimulating combination of academic,
                        artistic and practical activities, scaffolded by a caring framework, ensures that the child
                        learns without actually being aware of the coaching process.
                        We share the responsibility with parents, in equipping the young citizens of the country with
                        optimum values.</div>
                    <div class="image-box wow fadeInLeft">
                        <figure><img src="images/resource/vision.jpg" alt=""></figure>
                    </div>
                </div>
            </div>

            <!-- History Column -->

            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <h2>Our <span>Mision</span></h2>
                    <div class="text">To create an institution which fosters strong skills and everlasting passion for
                        learning as the child grows and imbibes in a dynamic and challenging environment.</div>
                    <div class="image-box wow fadeInLeft">
                        <figure><img src="images/resource/mission.jpg" alt=""></figure>
                    </div>
                </div>
            </div>

</section>
<!-- End History Section -->


<!-- Call To Action -->
<section class="call-to-action">
    <div class="anim-icons style-two">
        <span class="icon icon-cloud wow shake"></span>
        <span class="icon butterfly-1 wow rotateIn"></span>
    </div>

    <div class="auto-container">
        <div class="outer-box clearfix">
            <div class="title-column">
                <h3>Take Your First Step For Your Little One's </h3>
                <h2>Know more about Little Elly</h2>
            </div>

            <div class="btn-column">
                <div class="btn-box">
                    <a href="contact.html" class="theme-btn btn-style-one">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action -->

<!-- Main Footer -->
<?php include 'footer.php';?>