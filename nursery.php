<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Nursery</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Nursery</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Toller Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="image-box">
            <div class="image wow fadeIn"><img src="images/resource/course-4.jpg" alt=""></div>
        </div>
        <div class="lower-content">
            <h3>The Nursery program is suitable for children of 3+ years.</h3>

            <div class="text">
                <p>
                    Little Elly’s nursery program provides the foundation for learning of key essential skills. It
                    includes pre-writing, pre-reading, pre-maths, science and social skills. These are taught in a
                    natural and logical sequence; thereby encouraging the child to learn a step at a time.
                </p>
                <p>
                    Curriculum activities such as monthly thematic programs, specific weekly topics and daily learning,
                    keep the child focused on academic learning through exploration. These also encourage
                    self-initiatives, build curiosity and persistence in the child.
                </p>
                <p>
                    Extra-curricular activities include art and craft, dramatized plays, role plays, inter-personal and
                    social interaction. These ensure that the child explores his/her creativity, relates learning to
                    everyday life and builds confidence and self-esteem.
                </p>
                <p>
                    The nursery program is crafted around the age appropriate needs and skill sets of the child. This
                    encourages his/her overall development by offering a stimulating environment that combines learning
                    and play.
                </p>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span> Our Approach</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Focused, thematic activities and learning for
                                    developing key academic skills</li>
                                <li><i class="fa fa-angle-left"></i>Using creativity and play to develop social skills
                                    and build self-esteem
                                </li>
                                <li><i class="fa fa-angle-left"></i>Developing strong reading and writing for seamless
                                    transition to higher academic roles</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Key areas covered in the Nursery
                            curriculum</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Structured thematic learning and knowledge based
                                    activities</li>
                                <li><i class="fa fa-angle-left"></i>Learning with art, music and play</li>
                                <li><i class="fa fa-angle-left"></i>Building complex communication and social skills</li>
                                <li><i class="fa fa-angle-left"></i>Encouraging positive peer interaction</li>
                                <li><i class="fa fa-angle-left"></i>Understanding of mathematical concepts</li>
                                <li><i class="fa fa-angle-left"></i>Facilitating language skills</li>
                                <li><i class="fa fa-angle-left"></i>Enhancing fine and gross motor co-ordination skills</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Activities Involved</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Art, music, Drama, play and Sand play</li>
                                <li><i class="fa fa-angle-left"></i>Festivals and special day celebrations</li>
                                <li><i class="fa fa-angle-left"></i>Field trips to the local park, florist etc</li>
                                <li><i class="fa fa-angle-left"></i>Puppet shows and skits</li>
                                <li><i class="fa fa-angle-left"></i>Free Play with Technology tools</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>