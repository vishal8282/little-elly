<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Faculty and Facilities</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Faculty and Facilities</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Toller Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="lower-content">
            <h3>Faculty and Facilities</h3>

            <div class="text">
                <p>
                    The teaching, non-teaching and administrative staff of Little Elly are key to its success. They are
                    greatly valued and honoured. They play a very important role in the lives of every child who
                    experiences Little Elly.
                </p>
                <p>
                    Irrespective of the qualifications that old and new teaching and administrative staff bring on
                    board, it is mandatory for all to undergo the in-house training at the Little Elly Head Office, at
                    the start of the academic year. Other developmental opportunities, trainings and workshops are
                    arranged for the staff all through the year. Non-teaching staff are trained and supervised by the
                    centre co-ordinators and heads. All staff are continually motivated, appreciated and rewarded to
                    put in their best in the rich, stimulating and enjoyable work environment at Little Elly.
                </p>
            </div>
        </div>
        <div class="row clearfix facility-card">
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-1.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Outdoor Play Areas</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-2.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Sand Pit</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-3.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Stage for Music and Movement</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-4.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Themed Classrooms</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-5.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Montessori Space</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-6.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Early Learning Program (ELP) Room</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-7.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Reading Corner</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-8.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Neat and Child-Safe Pantry</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-9.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Clean Toilets</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-10.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">CCTV Cameras</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-11.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Daycare Facility</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-12.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Transport facility</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-column col-lg-3 col-md-4 col-sm-12">
                <div class="membar-block">
                    <div class="inner-box">
                        <div class="thumb">
                            <img src="images/resource/ff-13.jpg" alt="">
                        </div>
                        <div class="membar-info">
                            <h5 class="name">Holiday camps</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>