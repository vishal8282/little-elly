<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Programs</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Programs</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="course-block program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image">
                            <a href="toddler.php">
                                <img src="images/resource/course-1.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="other-info clearfix">
                        <div class="lower-content">
                            <h3>
                                <a href="toddler.php">Toddler</a>
                            </h3>
                            <a href="toddler.php" class="theme-btn btn-style-four">
                                Know More
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="course-block program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image">
                            <a href="playgroup.php">
                                <img src="images/resource/course-2.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="other-info clearfix">
                        <div class="lower-content">
                            <h3>
                                <a href="playgroup.php">Playgroup</a>
                            </h3>
                            <a href="playgroup.php" class="theme-btn btn-style-four">
                                Know More
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="course-block program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image">
                            <a href="nursery.php">
                                <img src="images/resource/course-4.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="other-info clearfix">
                        <div class="lower-content">
                            <h3>
                                <a href="nursery.php">Nursery</a>
                            </h3>
                            <a href="nursery.php" class="theme-btn btn-style-four">
                                Know More
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="course-block program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image">
                            <a href="kindergarten.php">
                                <img src="images/resource/course-3.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="other-info clearfix">
                        <div class="lower-content">
                            <h3>
                                <a href="kindergarten.php">Kindergarten</a>
                            </h3>
                            <a href="kindergarten.php" class="theme-btn btn-style-four">
                                Know More
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="course-block program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image">
                            <a href="holiday-camp-2.php">
                                <img src="images/resource/course-5.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="other-info clearfix">
                        <div class="lower-content">
                            <h3>
                                <a href="holiday-camp-2.php">Holiday Camp</a>
                            </h3>
                            <a href="holiday-camp-2.php" class="theme-btn btn-style-four">
                                Know More
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>