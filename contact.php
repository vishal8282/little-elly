<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Contact Us</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Contact Us</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Contact Info Section -->
<section class="contact-info-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Contact Info Block -->
            <div class="contact-info-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <h3>Resistered Office</h3>
                    <ul>
                        <li class="mr-bottom">#2, Honeydew Mansion, Above Pizza Hut, Near BDA Complex, HSR Layout, 7th
                            sector, Bangalore 560102 </li>
                        <li>Tel: +91 9243000900</li>
                        <li>Email; <a href="#">Support@littleelly.com</a></li>
                    </ul>
                </div>
            </div>

            <!-- Contact Info Block -->
            <div class="contact-info-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <h3>For Admission</h3>
                    <ul>
                        <li>Tel: +91 9243000900</li>
                        <li>Email; <a href="#">sales@littleelly.com</a></li>
                    </ul>
                </div>
            </div>

            <!-- Contact Info Block -->
            <div class="contact-info-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <h3>For Franchise</h3>
                    <ul>
                        <li>Tel: +91 9243000900</li>
                        <li>Email; <a href="#">Support@littleelly.com</a></li>
                    </ul>
                </div>
            </div>
            <!-- Contact Info Block -->
            <div class="contact-info-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <h3>For Job Another Enquiry</h3>
                    <ul>
                        <li>Tel: +91 9243000900</li>
                        <li>Email; <a href="#">Support@littleelly.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Contact Info Section -->

<!-- Contact Form Section -->
<section class="contact-form-section">
    <div class="auto-container">
        <div class="sec-title text-center light">
            <h2>Looking for the best <span>Pre School</span></h2>

            <div class="text">Our Life is all about communicating, converging and establishing a people-to-people
                connect. We believe that joining hands with the right people at the right places helps us develop new
                ideas, innovate and stay ahead of the curve. We would be more than excited to hear from you.</div>
        </div>

        <!-- Contact Form -->
        <div class="contact-form">
            <form method="post" action="sendemail.php" id="contact-form">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-xs-12 form-group">
                        <input type="text" name="username" placeholder="Name" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <input type="email" name="email" placeholder="Email" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <input type="text" name="phone" placeholder="Phone" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <input type="text" name="subject" placeholder="Website" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <textarea name="message" placeholder="Your Comments"></textarea>
                    </div>

                    <div class="col-lg-12 col-md-6 col-sm-12 form-group text-center">
                        <button class="theme-btn btn-style-two" type="submit" name="submit-form">Submit Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!--End Contact Form Section -->

<!-- Map Section -->
<section class="map-section">
    <div class="auto-container">
        <div class="map-outer">
            <!--Map Canvas-->
            <div class="map-canvas" data-zoom="12" data-lat="12.912403" data-lng="77.637714" data-type="roadmap"
                data-hue="#ffc400" data-title="Little Elly" data-icon-path="images/icons/map-marker.png" data-content="Learning Edge India Pvt. Ltd.<br><a href='mailto:support@littleelly.com'> Support@littleelly.com</a>">
            </div>
        </div>
    </div>
</section>
<!--End Map Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>