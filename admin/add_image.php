<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

ob_start();
session_start();

include "header.php";
include "../config/db-connect.php";

$id = $_GET['id'];

// echo '<pre>';
// print_r($_FILES);die;

if(isset($_FILES['file_to_upload'])){
	// echo '<pre>';
	// print_r($_FILES);die;
	$target_dir = "../uploads/";

	$total = count($_FILES['file_to_upload']['name']);

	for( $i=0 ; $i < $total ; $i++ ) {
		if(empty($_FILES["file_to_upload"]["name"][$i])){
			continue;
		}

		$image = 'image' . ($i + 1);
		$file_name = $_FILES["file_to_upload"]["name"][$i];

		$sql = "UPDATE `locate_centers` SET `$image` = '$file_name' WHERE `locate_centers`.`id` = $id";
		$result = $conn->query($sql);

		$target_file = $target_dir . basename($_FILES["file_to_upload"]["name"][$i]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES["file_to_upload"]["tmp_name"][$i]);
			if($check !== false) {
				$upload_message = "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				$upload_message = "File is not an image.";
				$uploadOk = 0;
			}
		}
		// Check if file already exists
		// if (file_exists($target_file)) {
		// 	$upload_message = "Sorry, file already exists.";
		// 	$uploadOk = 0;
		// }
		// Check file size
		if ($_FILES["file_to_upload"]["size"][$i] > 500000) {
			$upload_message = "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
			$upload_message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			$upload_message = "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES["file_to_upload"]["tmp_name"][$i], $target_file)) {
				$upload_message = "The file ". basename( $_FILES["file_to_upload"]["name"][$i]). " has been uploaded.";
			} else {
				$upload_message = "Sorry, there was an error uploading your file.";
			}
		}
	}
}

if(isset($_POST['name'])){
	extract($_POST);

	$sql = "INSERT INTO `locate_centers` (`id`, `name`, `address`, `center_head`, `center_coordinator`, `email`, `phone`, `description`, `center_head_description`, `center_coordinator_description`, `map`) VALUES (NULL, '$name', '$address', '$center_head', '$center_coordinator', '$email', '$phone', '$description', '$center_head_description', '$center_coordinator_description', '$map')";
	// echo $sql;die;

	if ($conn->query($sql) === TRUE) {
		$_SESSION['success_message'] = "New locate center added successfully";
	} else {
		$_SESSION['error_message'] = "Error: " . $sql . "<br>" . $conn->error;
	}

	$conn->close();
	header('Location: /admin');
}

?>

<div class="my-3 my-md-5">
	<div class="container">

    <?php if(isset($upload_message)){ ?>
    <div class="alert alert-success">
      <?php 
        echo $upload_message;
      ?>
    </div>
    <?php } ?>

		<div class="row">
			<div class="col-12">
				<form action="" method="post" class="card" enctype="multipart/form-data">
			          <div class="card-header">
			            <h3 class="card-title">Add Images</h3>
			          </div>
							<div class="card-body">
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<div class="form-group">
									<label>Top Banner</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
								<div class="form-group">
									<label>Center Head</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
								<div class="form-group">
									<label>Co-Ordinator</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
								<div class="form-group">
									<label>Testimonial 1</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
								<div class="form-group">
									<label>Testimonial 2</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
							</div>
							<div class="col-md-6 col-lg-6">
								<div class="form-group">
									<label>Gallery Image 1</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
								<div class="form-group">
									<label>Gallery Image 2</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
								<div class="form-group">
									<label>Gallery Image 3</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
								<div class="form-group">
									<label>Gallery Image 4</label>
									<input type="file" class="form-control" name="file_to_upload[]" placeholder="">
								</div>
							</div>
							<button type="submit" class="btn btn-primary ml-auto mr-auto mt-4">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<?php include 'footer.php' ?>