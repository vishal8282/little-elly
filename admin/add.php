<?php
ob_start();
session_start();

include "header.php";
include "../config/db-connect.php";

if(isset($_POST['name'])){
	foreach($_POST as $key => $value){
		$_POST[$key] = mysqli_real_escape_string($conn, $value);
	}
	extract($_POST);

	$programs = addslashes($programs);

	$sql = "INSERT INTO `locate_centers` (`id`, `name`, `address`, `center_head`, `center_coordinator`, `email`, `phone`, `description`, `center_head_description`, `center_coordinator_description`, `map`, `test1name`, `test1description`, `test2name`, `test2description`, `programs`, `seo_url`, `branch`) VALUES (NULL, '$name', '$address', '$center_head', '$center_coordinator', '$email', '$phone', '$description', '$center_head_description', '$center_coordinator_description', '$map', '$test1name', '$test1description', '$test2name', '$test2description', '$programs', '$seo_url', '$branch')";
	// echo $sql;die;

	if ($conn->query($sql) === TRUE) {
		$_SESSION['success_message'] = "New locate center added successfully";
	} else {
		// $_SESSION['error_message'] = "Error: " . $sql . "<br>" . $conn->error;
		$_SESSION['error_message'] = "Error: Something went wrong!";
	}

	$conn->close();
	header('Location: /admin/');
}

?>

<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<form action="" method="post" class="card">
			          <div class="card-header">
			            <h3 class="card-title">Add Center</h3>
			          </div>
							<div class="card-body">
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<div class="form-group">
									<input type="text" class="form-control" name="name" placeholder="Enter Center Name">
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="address" placeholder="Enter Center Location"></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="center_head" placeholder="Enter Center Head">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="center_coordinator" placeholder="Enter Center Co-Ordinator">
								</div>
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Enter Email">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="test1name" placeholder="Enter Testimonial 1 name">
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="test1description" placeholder="Enter Testimonial 1 description"></textarea>
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="programs" placeholder="Enter Available Programs (comma separated)"></textarea>
									<p class="text-muted">Available options: todler,playgroup,nursery,kindergarten,holiday_camps</p>
								</div>
							</div>
							<div class="col-md-6 col-lg-6">
								<div class="form-group">
									<input type="phone" class="form-control" name="phone" placeholder="Enter Phone">
								</div>
								<div class="form-group">
									<textarea class="form-control" name="description" placeholder="Enter Center Description"></textarea>
								</div>
								<div class="form-group">
									<textarea class="form-control" name="center_head_description" placeholder="Enter Center Head Description"></textarea>
								</div>
								<div class="form-group">
									<textarea class="form-control" name="center_coordinator_description" placeholder="Enter Center Co-Ordinator Description"></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="map" placeholder="Enter Map Url">
									<p class="text-muted help-block">https://www.embedgooglemap.net/ 1280x424</p>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="test2name" placeholder="Enter Testimonial 2 name">
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="test2description" placeholder="Enter Testimonial 2 description"></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="seo_url" placeholder="Enter SEO Url">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="branch" placeholder="Enter Branch">
								</div>
							</div>
							<button type="submit" class="btn btn-primary ml-auto mr-auto mt-4">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<?php include 'footer.php' ?>