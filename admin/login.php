<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
include "../config/db-connect.php";

if(isset($_POST['username'])) {
  $username = mysqli_real_escape_string($conn, $_POST['username']);
  $password = mysqli_real_escape_string($conn, $_POST['password']); 

  $sql = "SELECT * FROM admin WHERE username = '$username'";
  $result = $conn->query($sql);
  $user = $result->fetch_assoc();

  if (password_verify($password, $user['password'])) {
    $_SESSION['user'] = $username;
    header('Location: /admin/');
  }else{
    $_SESSION['error_message'] = 'Invalid Login Details';
  }
  $conn->close();
  // die;
}

?>

<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="/admin/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="/admin/favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Homepage - tabler.github.io - a responsive, flat and full featured admin template</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="/admin/assets/js/require.min.js"></script>
    <script>
      requirejs.config({
          baseUrl: 'admin'
      });
    </script>
    <!-- Dashboard Core -->
    <link href="/admin/assets/css/dashboard.css" rel="stylesheet" />
    <script src="/admin/assets/js/dashboard.js"></script>
    <!-- c3.js Charts Plugin -->
    <link href="/admin/assets/plugins/charts-c3/plugin.css" rel="stylesheet" />
    <script src="/admin/assets/plugins/charts-c3/plugin.js"></script>
    <!-- Google Maps Plugin -->
    <link href="/admin/assets/plugins/maps-google/plugin.css" rel="stylesheet" />
    <script src="/admin/assets/plugins/maps-google/plugin.js"></script>
    <!-- Input Mask Plugin -->
    <script src="/admin/assets/plugins/input-mask/plugin.js"></script>

    <link href="/admin/assets/css/style.css" rel="stylesheet" />
  </head>

  <body class="">
    <div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <!-- <img src="./demo/brand/tabler.svg" class="h-6" alt=""> -->
                <img src="/images/logo.png" class="header-brand-img" alt="tabler logo" >
              </div>
              <form class="card" action="" method="post">
                <div class="card-body p-6">
                  <div class="card-title">Login to your account</div>
                  <div class="form-group">
                    <label class="form-label">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="username">
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      Password
                      <!-- <a href="./forgot-password.html" class="float-right small">I forgot password</a> -->
                    </label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                  </div>

<!--                   <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" />
                      <span class="custom-control-label">Remember me</span>
                    </label>
                  </div>
 -->
                  <div class="form-footer">
                    <?php if(isset($_SESSION['error_message'])){ ?>
                    <div class="alert alert-danger">
                      <?php 
                        echo $_SESSION['error_message'];
                        unset($_SESSION['error_message']);
                      ?>
                    </div>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                  </div>
                </div>
              </form>
              <div class="text-center text-muted">
                <!-- Don't have account yet? <a href="./register.html">Sign up</a> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>