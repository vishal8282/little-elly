<?php
session_start();
ob_start();

include "header.php";

require_once "../config/db-connect.php";

if(!isset($_GET['id'])){
    header('Location: locate-centers');
}
$id = $_GET['id'];

if(isset($_POST['name'])){
	foreach($_POST as $key => $value){
		$_POST[$key] = mysqli_real_escape_string($conn, $value);
	}
	extract($_POST);

	if(!empty($map)){
		$sql = "UPDATE `locate_centers` SET `name` = '$name', `address` = '$address', `email` = '$email', `phone` = '$phone', `description` = '$description', `center_head_description` = '$center_head_description', `center_coordinator_description` = '$center_coordinator_description', `map` = '$map', `test1name` = '$test1name', `test1description` = '$test1description', `test2name` = '$test2name', `test2description` = '$test2description', `programs` = '$programs', `seo_url` = '$seo_url', `branch` = '$branch' WHERE `locate_centers`.`id` = $id";
	}else{
		$sql = "UPDATE `locate_centers` SET `name` = '$name', `address` = '$address', `email` = '$email', `phone` = '$phone', `description` = '$description', `center_head_description` = '$center_head_description', `center_coordinator_description` = '$center_coordinator_description', `test1name` = '$test1name', `test1description` = '$test1description', `test2name` = '$test2name', `test2description` = '$test2description', `programs` = '$programs', `seo_url` = '$seo_url', `branch` = '$branch' WHERE `locate_centers`.`id` = $id";
	}
	// echo $sql;die;

	if ($conn->query($sql) === TRUE) {
		$_SESSION['success_message'] = "Locate center updated successfully";
	} else {
		$_SESSION['error_message'] = "Error: " . $sql . "<br>" . $conn->error;
		// $_SESSION['error_message'] = "Error: Something went wrong!";
	}

	$conn->close();
	header('Location: /admin/');
}

$sql = "SELECT * FROM locate_centers where id=$id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $center = (object)$result->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();

// echo '<pre>';
// print_r($center);die;

?>

<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<form action="/admin/edit.php?id=<?php echo $id ?>" method="post" class="card">
			          <div class="card-header">
			            <h3 class="card-title">Update Center</h3>
			          </div>
							<div class="card-body">
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<div class="form-group">
									<input type="text" class="form-control" name="name" placeholder="Enter Center Name" value="<?php echo $center->name ?>">
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="address" placeholder="Enter Center Location"><?php echo $center->address ?></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="center_head" placeholder="Enter Center Head" value="<?php echo $center->center_head ?>">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="center_coordinator" placeholder="Enter Center Co-Ordinator" value="<?php echo $center->center_coordinator ?>">
								</div>
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Enter Email" value="<?php echo $center->email ?>">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="test1name" placeholder="Enter Testimonial 1 name" value="<?php echo $center->test1name ?>">
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="test1description" placeholder="Enter Testimonial 1 description"><?php echo $center->test1description ?></textarea>
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="programs" placeholder="Enter Available Programs (comma separated)"><?php echo $center->programs ?></textarea>
									<p class="text-muted">Available options: todler,playgroup,nursery,kindergarten,holiday_camps</p>
								</div>
							</div>
							<div class="col-md-6 col-lg-6">
								<div class="form-group">
									<input type="phone" class="form-control" name="phone" placeholder="Enter Phone" value="<?php echo $center->phone ?>">
								</div>
								<div class="form-group">
									<textarea class="form-control" name="description" placeholder="Enter Center Description"><?php echo $center->description ?></textarea>
								</div>
								<div class="form-group">
									<textarea class="form-control" name="center_head_description" placeholder="Enter Center Head Description"><?php echo $center->center_head_description ?></textarea>
								</div>
								<div class="form-group">
									<textarea class="form-control" name="center_coordinator_description" placeholder="Enter Center Co-Ordinator Description"><?php echo $center->center_coordinator_description ?></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="map" placeholder="Enter Map Url" value="">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="test2name" placeholder="Enter Testimonial 2 name" value="<?php echo $center->test2name ?>">
								</div>
								<div class="form-group">
									<textarea rows="4" class="form-control" name="test2description" placeholder="Enter Testimonial 2 description"><?php echo $center->test2description ?></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="seo_url" placeholder="Enter SEO Url" value="<?php echo $center->seo_url ?>">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="branch" placeholder="Enter Branch" value="<?php echo $center->branch ?>">
								</div>
							</div>
							<button type="submit" class="btn btn-primary ml-auto mr-auto mt-4">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<?php include 'footer.php' ?>