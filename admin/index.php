<?php
session_start();
include "header.php";
include "../config/db-connect.php";

$sql = "SELECT * FROM locate_centers ORDER BY id DESC";
$result = $conn->query($sql);

$centers = [];
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $centers[] = (object)$row;
  }
} else {
  echo "0 results";
}
$conn->close();

// echo '<pre>';
// print_r($centers);die;

?>

  <div class="container mt-4">
    <?php if(isset($_SESSION['success_message'])){ ?>
    <div class="alert alert-success">
      <?php 
        echo $_SESSION['success_message'];
        unset($_SESSION['success_message']);
      ?>
    </div>
    <?php } ?>

    <?php if(isset($_SESSION['error_message'])){ ?>
    <div class="alert alert-danger">
      <?php 
        echo $_SESSION['error_message'];
        unset($_SESSION['error_message']);
      ?>
    </div>
    <?php } ?>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?php echo count($centers) ?> Locate Centers</h3>
          </div>
                  <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                      <thead>
                        <tr>
                          <th class="text-center w-1">S. No</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Added On</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($centers as $key => $center){ ?>
                        <tr>
                          <td><?php echo $key+1 ?></td>
                          <td><?php echo $center->name ?></td>
                          <td><?php echo $center->email ?></td>
                          <td><?php echo $center->phone ?></td>
                          <td><?php echo $center->created_at ?></td>
                          <td class="w-1"><a href="/admin/edit.php?id=<?php echo $center->id ?>" class="icon"><i class="fe fe-edit"></i></a></td>
                          <td class="w-1"><a href="/admin/add_image.php?id=<?php echo $center->id ?>" class="icon"><i class="fe fe-image"></i></a></td>
                          <td class="w-1"><a onclick="return confirm('One item will be deleted')" href="/admin/delete.php?id=<?php echo $center->id ?>" class="icon"><i class="fe fe-trash"></i></a></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>


 <?php include "footer.php" ?>