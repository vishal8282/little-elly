<?php include 'header.php';?>
<!--Main Slider-->
<section class="main-slider">
    <div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_one_wrapper" data-source="gallery">
        <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
            <ul>
                <!-- Slide 1 -->
                <li data-description="Slide Description" data-easein="default" data-easeout="default"
                    data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1=""
                    data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                    data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off"
                    data-slotamount="default" data-thumb="images/main-slider/image-1.jpg" data-title="Slide Title"
                    data-transition="parallaxvertical">

                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center"
                        data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-1.jpg">

                    <div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="nowrap" data-width="none" data-hoffset="['0','0','0','0']"
                        data-voffset="['-130','-110','-110','-110']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h4>Welcome to Little Elly</h4>
                    </div>
                    <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="nowrap" data-fontsize="['64','40','36','24']" data-width="auto"
                        data-textalign="left" data-hoffset="['0','0','0','0']" data-voffset="['-60','-50','-50','-50']"
                        data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h2>Most Admired Pre School</h2>
                    </div>
                    <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="normal" data-fontsize="['64','40','36','24']" data-width="['650','650','650','600']"
                        data-textalign="left" data-hoffset="['0','0','0','0']" data-voffset="['20','20','30','40']"
                        data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h4>In your Neighbourhood</h4>
                    </div>

                    <div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="nowrap" data-width="auto" data-hoffset="['0','0','0','0']"
                        data-voffset="['100','100','120','140']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <a href="about.php" class="theme-btn btn-style-one"><span>Know More</span></a>
                        <a href="contact.php" class="theme-btn btn-style-two"><span>Enquire Now</span></a>
                    </div>
                </li>
                <!-- Slide 2 -->
                <li data-description="Slide Description" data-easein="default" data-easeout="default"
                    data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1=""
                    data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                    data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off"
                    data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title"
                    data-transition="parallaxvertical">

                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center"
                        data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-2.jpg">

                    <div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="nowrap" data-width="none" data-hoffset="['0','0','0','0']"
                        data-voffset="['-130','-110','-110','-110']" data-x="['right','right','right','right']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h4>Welcome to Little Elly</h4>
                    </div>
                    <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="nowrap" data-fontsize="['64','40','36','24']" data-width="auto"
                        data-textalign="right" data-hoffset="['0','0','0','0']" data-voffset="['-60','-50','-50','-50']"
                        data-x="['right','right','right','right']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h2>Most Admired Pre School</h2>
                    </div>
                    <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="normal" data-fontsize="['64','40','36','24']" data-width="['650','650','650','600']"
                        data-textalign="right" data-hoffset="['0','0','0','0']" data-voffset="['20','20','30','40']"
                        data-x="['right','right','right','right']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h4>In your Neighbourhood</h4>
                    </div>

                    <div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text"
                        data-height="none" data-whitespace="nowrap" data-width="auto" data-hoffset="['0','0','0','0']"
                        data-voffset="['100','100','120','140']" data-x="['right','right','right','right']" data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <a href="about.php" class="theme-btn btn-style-one"><span>Know More</span></a>
                        <a href="contact.php" class="theme-btn btn-style-two"><span>Enquire Now</span></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>
<!--End Main Slider-->

<!-- About Us -->
<section class="about-us no-border">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2><span>About</span> Little Elly</h2>
                    </div>
                    <div class="text">
                        <p><strong>Little Elly, the Concept Pre-school was created to nurture the child and lay
                                a healthy foundation for a learned society. It is a noble initiative of
                                Learning Edge India Pvt. Ltd in association with Glen Tree (UK).</strong></p>
                        <p> Glen Tree (UK) are pioneers in serving the learning needs of kids.
                            Little Elly started franchising from the year 2007. Today, Little Elly is present
                            in 6 states with 120+ franchise centers, 5000 students and well-qualified faculty.
                            Each centre is equipped with excellent facilities, crafted around the needs and
                            skill sets of each child, thereby giving him/her a stimulating environment that
                            combines learning and play.</p>
                    </div>
                    <div class="btn-box">
                        <a href="about.php" class="theme-btn btn-style-one">Read More</a>
                    </div>
                </div>
            </div>
            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box">
                        <figure class="image-1 wow fadeInRight"><img src="images/resource/image-1.png" alt=""></figure>
                        <figure class="image-2 wow zoomIn"><img src="images/resource/dots.png" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End About Us -->

<!-- Event Section -->
<section class="event-section" style="background-image: url(images/background/pattern-1.png);">
    <div class="auto-container">
        <div class="sec-title text-center">
            <h2>Our <span>Programs</span></h2>
        </div>
        <div class="row clearfix courseSection">
            <!-- Course Block -->
            <div class="course-block col-lg-6 col-md-12 col-sm-12 wow fadeInUp">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="toddler.php"><img src="images/resource/event-1.jpg" alt="todder"></a></div>
                        <div class="likes"><span class="flaticon-like"></span> 25</div>
                    </div>
                    <div class="lower-content">
                        <div class="price">1+</div>
                        <h3><a href="toddler.php">Toddler</a></h3>
                        <div class="text">
                            The Toddler Program is suitable for children of 1+
                            years
                        </div>
                    </div>
                    <!--<div class="other-info clearfix">-->
                    <!--    <div class="rating"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span-->
                    <!--            class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></div>-->
                    <!--    <div class="seats-available"><a href="toddler.php"><i class="fa fa-user-plus"></i>-->
                    <!--            15 Seat Avilable</a></div>-->
                    <!--</div>-->
                </div>
            </div>

            <!-- Course Block -->
            <div class="course-block col-lg-6 col-md-12 col-sm-12 wow fadeInUp">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="playgroup.php"><img src="images/resource/event-2.jpg" alt=""></a></div>
                        <div class="likes"><span class="flaticon-like"></span> 25</div>
                    </div>
                    <div class="lower-content">
                        <div class="price">2+ </div>
                        <h3><a href="playgroup.php">Playgroup</a></h3>
                        <div class="text">
                            The Playgroup Program is suitable for children of 2+
                            years
                        </div>
                    </div>
                    <!-- <div class="other-info clearfix">
                        <div class="rating"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span
                                class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></div>
                        <div class="seats-available"><a href="playgroup.php"><i class="fa fa-user-plus"></i>
                                25 Seat Avilable</a></div>
                    </div> -->
                </div>
            </div>

            <!-- Course Block -->
            <div class="course-block col-lg-6 col-md-12 col-sm-12 wow fadeInUp">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="nursery.php"><img src="images/resource/event-3.jpg" alt=""></a></div>
                        <div class="likes"><span class="flaticon-like"></span> 25</div>
                    </div>
                    <div class="lower-content">
                        <div class="price">3+</div>
                        <h3><a href="nursery.php">Nursery</a></h3>
                        <div class="text">
                            The Nursery Program is suitable for children of 3+ years
                        </div>
                    </div>
                    <!-- <div class="other-info clearfix">
                        <div class="rating"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span
                                class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></div>
                        <div class="seats-available"><a href="nursery.php"><i class="fa fa-user-plus"></i>
                                15 Seat Avilable</a></div>
                    </div> -->
                </div>
            </div>

            <!-- Course Block -->
            <div class="course-block col-lg-6 col-md-12 col-sm-12 wow fadeInUp">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="kindergarten.php"><img src="images/resource/event-4.jpg" alt=""></a></div>
                        <div class="likes"><span class="flaticon-like"></span> 25</div>
                    </div>
                    <div class="lower-content">
                        <div class="price">3+</div>
                        <h3><a href="kindergarten.php">Kindergarten</a></h3>
                        <div class="text">
                            The Kindergarten program is suitable for children of 3-5 years
                        </div>
                    </div>
                    <!-- <div class="other-info clearfix">
                        <div class="rating"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span
                                class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></div>
                        <div class="seats-available"><a href="kindergarten.php"><i class="fa fa-user-plus"></i>
                                35 Seat Avilable</a></div>
                    </div> -->
                </div>
            </div>

            <!-- Course Block -->
            <div class="course-block col-lg-6 col-md-12 col-sm-12 wow fadeInUp">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="holiday-camp.php"><img src="images/resource/event-5.jpg" alt=""></a></div>
                        <div class="likes"><span class="flaticon-like"></span> 25</div>
                    </div>
                    <div class="lower-content">
                        <div class="price">2+</div>
                        <h3><a href="holiday-camp.php">Holiday Camps</a></h3>
                        <div class="text">
                            Holiday Camps for children from 2 years to 10 years
                        </div>
                    </div>
                    <!-- <div class="other-info clearfix">
                        <div class="rating"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span
                                class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></div>
                        <div class="seats-available"><a href="holiday-camp.php"><i class="fa fa-user-plus"></i>
                                15 Seat Avilable</a></div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Event Section -->

<!-- Call To Action -->
<section class="call-to-action">
    <div class="anim-icons">
        <span class="icon icon-cloud wow shake"></span>
        <span class="icon butterfly-1 wow zoomIn"></span>
        <span class="icon butterfly-2 wow zoomIn"></span>
    </div>
    <div class="auto-container">
        <div class="outer-box clearfix">
            <div class="title-column">
                <h3>Take Your First Step For Your Little One's </h3>
                <h2>Know more about Little Elly</h2>
            </div>
            <div class="btn-column">
                <div class="btn-box">
                    <a href="contact.php" class="theme-btn btn-style-one">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action -->


<!-- Enrollment Section -->
<section class="enrollment-section">
    <div class="auto-container">
        <div class="sec-title text-center">
            <h2>Little Elly <span>Concept Areas</span></h2>
        </div>

        <div class="enrollment-area">
            <div class="image-box">
                <div class="image wow fadeIn">
                    <figure><img src="images/resource/image-22.gif" alt=""></figure>
                </div>
            </div>

            <div class="enrollment-box">
                <h4 class="title">Motor Skills Area <span class="count">01</span></h4>
                <div class="text">To Enhance practical and Sensorial experiences with the help of Montessori Material</div>
            </div>

            <div class="enrollment-box">
                <h4 class="title">Early Learning Area <span class="count">02</span></h4>
                <div class="text">A unique multi sensory learning area for early learners</div>
            </div>

            <div class="enrollment-box">
                <h4 class="title">Focussed Learning Area <span class="count">03</span></h4>
                <div class="text">Exposure to core learning in a formal classroom setup</div>
            </div>

            <div class="enrollment-box">
                <h4 class="title">Readng Area <span class="count">04</span></h4>
                <div class="text">Age appropriate reading program to inculcate reading skills</div>
            </div>

            <div class="enrollment-box">
                <h4 class="title">Art and Craft area <span class="count">05</span></h4>
                <div class="text">For creative and aesthetic expression</div>
            </div>
            <!-- <div class="enrollment-box">
                <h4 class="title">Stage Area <span class="count">06</span></h4>
                <div class="text">Music , movement, prayer and public speaking</div>
            </div> -->
        </div>
    </div>
</section>
<!--End Enrollment Section -->



<!-- Fun Fact -->
<section class="fun-facts-section" style="background-image:url(images/fun.jpg);">
    <div class="auto-container">

        <div class="row clearfix">
            <!--Column-->
            <div class="column count-box col-lg-3 col-md-6 col-sm-12">
                <div class="inner">
                    <div class="content">
                        <div class="icon-box"><span class="flaticon-abc-squares"></span></div>
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="3000" data-stop="7000">300 </span>
                        </div>
                        <div class="counter-title">Satisfied Parents & Children</div>
                    </div>
                </div>
            </div>

            <!--Column-->
            <div class="column count-box col-lg-3 col-md-6 col-sm-12">
                <div class="inner">
                    <div class="content">
                        <div class="icon-box"><span class="flaticon-stop"></span></div>
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="2000" data-stop="120">0 </span>
                        </div>
                        <div class="counter-title">Center Across the Globe</div>
                    </div>
                </div>
            </div>

            <!--Column-->
            <div class="column count-box col-lg-3 col-md-6 col-sm-12">
                <div class="inner">
                    <div class="content">
                        <div class="icon-box"><span class="flaticon-smiling-girl"></span></div>
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="2000" data-stop="12"></span>
                        </div>
                        <div class="counter-title">Years of Experiance</div>
                    </div>
                </div>
            </div>

            <!--Column-->
            <div class="column count-box col-lg-3 col-md-6 col-sm-12">
                <div class="inner">
                    <div class="content">
                        <div class="icon-box"><span class="flaticon-edit-2"></span></div>
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="3000" data-stop="600"></span>
                        </div>
                        <div class="counter-title">Experiance Teachers</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Fun Fact -->

<!--Gallery Section-->
<section class="gallery-section">
    <div class="auto-container">
        <div class="sec-title text-center pink-devider">
            <h2>Our <span>Gallery</span></h2>
        </div>
        <!--Galery-->
        <div class="sortable-masonry">
            <!--Filter-->
            <div class="filters clearfix">
                <ul class="filter-tabs filter-btns clearfix">
                    <li class="active filter" data-role="button" data-filter=".all">All</li>
                    <li class="filter" data-role="button" data-filter=".activities">Activities</li>
                    <li class="filter" data-role="button" data-filter=".annualday">Annual Day</li>
                    <li class="filter" data-role="button" data-filter=".celebration">Celebration</li>
                    <li class="filter" data-role="button" data-filter=".classroom">Classroom</li>
                    <li class="filter" data-role="button" data-filter=".events">Events</li>
                    <li class="filter" data-role="button" data-filter=".franchise">Franchise Meet</li>
                    <li class="filter" data-role="button" data-filter=".health">Health camp</li>
                    <li class="filter" data-role="button" data-filter=".playgound">Playgound</li>
                </ul>
            </div>

            <div class="items-container row clearfix">

                <!--Project Block Two-->
                <div class="project-block masonry-item game annualday franchise classroom events celebration all col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/1.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/1.jpg" style="background-image: url(images/gallery/1.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--Project Block Two-->
                <div class="project-block masonry-item game annualday health events activities classroom all col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/2.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/2.jpg" style="background-image: url(images/gallery/2.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--Project Block Two-->
                <div class="project-block masonry-item classroom franchise annualday celebration game all col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/3.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/3.jpg" style="background-image: url(images/gallery/3.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--Project Block Two-->
                <div class="project-block masonry-item events game annualday Playgound activities events all col-lg-8 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/4.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/4.jpg" style="background-image: url(images/gallery/4.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Gallery Section-->

<!-- Subscribe Section -->
<section class="subscribe-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="title-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <h2>Take The <span>First Step</span></h2>
                    <div class="text">Start your own Pre-School & Play School with Little Elly & Open the World of Joy</div>
                </div>
            </div>
            <div class="form-column col-lg-6 col-md-12 col-sm-12">
                <div class="subscribe-form">
                    <form method="post" action="franchise-opportunity.php">
                        <div class="form-group">
                            <button type="submit" class="theme-btn btn-style-six">Contact Us</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Subscribe Section -->

<!-- News Section -->
<section class="news-section" style="background-image: url(images/background/pattern-2.png);">
    <div class="auto-container">
        <div class="sec-title text-center blue-devider">
            <h2> Our News & <span>Events</span></h2>
        </div>
        <div class="row clearfix">
            <!-- News Block -->
            <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/news-1.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">31 <span>May</span></div>
                            <ul class="info">
                                <li>By Admin</li>
                                <li>01 comment</li>
                            </ul>
                            <h3><a href="blog-single-1.html">Play Is Our Brain's Favorite Way Of Learning</a></h3>
                        </div>
                        <div class="text">Timely deliverables for real-time schemas. Dramatically maintain
                            clicks-and-mortar lutions without functional solutions.</div>
                    </div>
                </div>
            </div>
            <!-- News Block -->
            <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/news-2.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">28 <span>May</span></div>
                            <ul class="info">
                                <li>By Admin</li>
                                <li>01 comment</li>
                            </ul>
                            <h3><a href="blog-single-1.html">Where Well Rounded Starts with Well Educated</a></h3>
                        </div>
                        <div class="text">Timely deliverables for real-time schemas. Dramatically maintain
                            clicks-and-mortar lutions without functional solutions.</div>
                    </div>
                </div>
            </div>
            <!-- News Block -->
            <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                <div class="inner-box">
                    <div class="image-box">
                        <span class="tag">News</span>
                        <div class="image"><a href="blog-single-1.html"><img src="images/resource/news-3.jpg" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <div class="title">
                            <div class="date">29 <span>May</span></div>
                            <ul class="info">
                                <li>By Admin</li>
                                <li>01 comment</li>
                            </ul>
                            <h3><a href="blog-single-1.html">An Inspired Approach To Education</a></h3>
                        </div>
                        <div class="text">Timely deliverables for real-time schemas. Dramatically maintain
                            clicks-and-mortar lutions without functional solutions.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End News Section -->

<!-- Testimonial Section -->
<section class="testimonial-section" style="background-image: url(images/background/5.jpg);">
    <div class="video-block">
        <video width="100%" height="100%" autoplay="autoplay" loop="loop" muted="">
            <source src="images/testimonial.mp4" type="video/webm">
        </video>
        <div class="video-overlay"></div>
    </div>
    <div class="auto-container">
        <div class="inner-container">
            <div class="single-item-carousel owl-carousel owl-theme">
                <!-- Testimonial Block Two -->
                <div class="testimonial-block-two">
                    <div class="inner-box">
                        <div class="thumb"><img src="images/resource/thumb-2.jpg" alt=""></div>
                        <span class="icon flaticon-right-quotes-symbol"></span>
                        <div class="text">Ethan is happy going to school. I see a lot of possitive changes in him – He
                            plays
                            independently at home and his confidence has increased.Teachers are very supportive.
                            And thanks a lot for taking good care of him.</div>
                        <div class="info">
                            <div class="name">Jeshna Renita Crasta<span class="designation">(Nursery)</span></div>
                        </div>
                    </div>
                </div>

                <!-- Testimonial Block Two -->
                <div class="testimonial-block-two">
                    <div class="inner-box">
                        <div class="thumb"><img src="images/resource/thumb-2.jpg" alt=""></div>
                        <span class="icon flaticon-right-quotes-symbol"></span>
                        <div class="text">Ahad enjoys coming to school and we have seen good improvement in his speech,
                            especially english. we are happy with over all improvement</div>
                        <div class="info">
                            <div class="name">Nikhath gadwale<span class="designation"> (Nursery) </span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Testimonial Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>