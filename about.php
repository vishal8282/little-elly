<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>About Us</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>About Us</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- About Us -->
<section class="about-us">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2><span>About</span> Little Elly</h2>
                    </div>
                    <div class="text">
                        <p><strong>Little Elly, the Concept Pre-school was created to nurture the child and lay
                                a healthy foundation for a learned society. It is a noble initiative of
                                Learning Edge India Pvt. Ltd in association with Glen Tree (UK).</strong></p>
                        <p> Glen Tree (UK) are pioneers in serving the learning needs of kids.
                            Little Elly started franchising from the year 2007. Today, Little Elly is present
                            in 6 states with 120+ franchise centers, 5000 students and well-qualified faculty.
                            Each centre is equipped with excellent facilities, crafted around the needs and
                            skill sets of each child, thereby giving him/her a stimulating environment that
                            combines learning and play.</p>
                    </div>
                </div>
            </div>

            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/image-1.png" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End About Us -->

<!-- Philospher Section -->
<section class="about-us" id="philosophy">
    <div class="auto-container">
        <div class="sec-title">
            <h2><span>Philosphy</span></h2>
        </div>
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <ul class="list-style-two">
                        <li>Little Elly is built around the strong belief that the childhood of a little one is
                            precious.</li>
                        <li>Research and in-depth studies have proven that the first five years of childhood are
                            the most formative.</li>
                        <li>Incredible learning and vital mental and cognitive growth occurs during this period.</li>
                        <li>Our programs are tailored to foster every child’s social, aesthetic and motor skills.</li>
                        <li>We develop age-appropriate activity based learning for the child’s all round
                            development.</li>
                    </ul>

                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/philosophy.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Our Vision and Mision Section -->
<section class="about-us" id="our-vision-and-mission">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column" style="padding-right: 50px;">
                    <div class="sec-title">
                        <h2>Our <span>Vision</span></h2>
                    </div>

                    <div class="text">To be the trendsetter and path paver in early childhood education
                        We believe that learning is a continuous and lifelong process. Every child achieves it at his
                        or her own pace. The child’s ability to learn and grow is unique and special. We ensure that we
                        provide each child with proper guidance to grow in his/her own way, in a mutually accepting and
                        nurturing setting.
                        Each child is guided into building a strong foundation of knowledge and skills. We believe in
                        making teaching and learning a harmonious process. Our stimulating combination of academic,
                        artistic and practical activities, scaffolded by a caring framework, ensures that the child
                        learns without actually being aware of the coaching process.
                        We share the responsibility with parents, in equipping the young citizens of the country with
						optimum values.
					</div>
                    <!-- <div class="image-box wow fadeInLeft">
                        <figure><img src="images/resource/vision.jpg" alt=""></figure>
                    </div> -->
                </div>
            </div>
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column" style="padding-left: 50px;">
                    <div class="sec-title">
                        <h2>Our <span>Mision</span></h2>
                    </div>

                    <div class="text">To create an institution which fosters strong skills and everlasting passion for
                        learning as the child grows and imbibes in a dynamic and challenging environment.</div>
                    <!-- <div class="image-box wow fadeInLeft">
                        <figure><img src="images/resource/mission.jpg" alt=""></figure>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Our origin-and-history Section -->
<section class="about-us" id="origin-and-history">
    <div class="auto-container">
        <div class="row clearfix">

            <!-- About Us -->
            <section class="about-us style-three">
                <div class="auto-container">
                    <div class="sec-title">
                        <h2>Origin & <span>History</span></h2>
                    </div>
                    <div class="row clearfix">
                        <div class="content-column col-lg-6 col-md-12 col-sm-12">
                            <div class="inner-column origin-inner">
                                <div class="text">
                                    <p>
                                        <strong>
                                            It is the aim of every parents to find the right school for their child,
                                            one
                                            that
                                            takes care of his learning, equips him with life skills as well as helps in
                                            his
                                            overall
                                            development.
                                        </strong>
                                    </p>
                                    <p>Several years ago, it was this same desire that propelled Mrs. Preeti Bhandary
                                        to
                                        look
                                        around her neighbourhood for an “outstanding pre-school” to enrol her child in.
                                        She
                                        came
                                        across many good schools, each offering something unique to the child. Yet,
                                        what
                                        was
                                        missing was a good school that gave her child EVERYTHING that the little one
                                        needed
                                        for her
                                        all round growth. As with any parent, she wanted nothing but the best for her
                                        child. Her
                                        aspiration to fulfil that dream led her to initiate one of the most respected
                                        group
                                        of
                                        institutions in India, a pioneer in early childhood education and care – the
                                        Little
                                        Elly
                                        chain of pre-schools.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Image-column -->
                        <div class="image-column col-lg-6 col-md-12 col-sm-12">
                            <div class="inner-column origin-inner">
                                <div class="image-box wow fadeInRight">
                                    <figure class="image-1"><img src="images/resource/origin-1.jpg" alt=""></figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End About Us -->

            <!-- About Us -->
            <section class="about-us style-three">
                <div class="auto-container">
                    <div class="row clearfix">
                        <!-- Image-column -->
                        <div class="image-column col-lg-6 col-md-12 col-sm-12">
                            <div class="inner-column origin-inner">
                                <div class="image-box wow fadeInRight">
                                    <figure class="image-1"><img src="images/resource/origin-2.jpg" alt=""></figure>
                                </div>
                            </div>
                        </div>
                        <div class="content-column col-lg-6 col-md-12 col-sm-12">
                            <div class="inner-column origin-inner">
                                <div class="text">
                                    <p>
                                        <strong>
                                            With the steadfast support of her husband, Mr. Vittal Bhandary, Preeti’s
                                            endeavour
                                            began to take shape. Combining the rich experience of her career as a
                                            kindergarten
                                            teacher with her love for teaching in innovative and unique ways, she set
                                            out
                                            to start
                                            “Salmiya Playschool”, a pre-school in Kuwait in 2000. From there on, there
                                            was
                                            no
                                            looking back.
                                        </strong>
                                    </p>
                                    <p>
                                        The following years of learning, research and experience were invaluable.
                                        Enriching
                                        her
                                        theoretical knowledge and practical exposure gave her a real-life understanding
                                        of
                                        the best
                                        practises in the field of early childhood education and care.
                                    </p>
                                    <p>
                                        Having amassed many years of skill in nurturing the individual needs of a
                                        child,
                                        her next
                                        calling was to pass on that know-how to a wider audience. She designed a
                                        superior
                                        educational program for the holistic development for children, mentored
                                        dedicated
                                        faculty
                                        and set high standards of excellence for adherence to children’s needs. This
                                        was
                                        the
                                        genesis of Little Elly – The Concept pre-school.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End About Us -->

            <!-- About Us -->
            <section class="about-us style-three">
                <div class="auto-container">
                    <div class="row clearfix">
                        <div class="content-column col-lg-6 col-md-12 col-sm-12">
                            <div class="inner-column origin-inner">
                                <div class="text">
                                    <p>
                                        <strong>
                                            Having amassed many years of skill in nurturing the individual needs of a
                                            child, her
                                            next calling was to pass on that know-how to a wider audience. She designed
                                            a
                                            superior
                                            educational program for the holistic development for children, mentored
                                            dedicated
                                            faculty and set high standards of excellence for adherence to children’s
                                            needs.
                                            This
                                            was the genesis of Little Elly – The Concept pre-school.
                                        </strong>
                                    </p>
                                    <p>In August 2005, Glentree UK collaborated with them to provide technical and
                                        curriculum
                                        inputs and support.</p>
                                    <p>To provide a greater thrust in reaching out to the community, the founders
                                        formed a
                                        new
                                        company M/s Learning Edge India Private Limited. It took over the preschool
                                        business from
                                        M/s Global Learning System Private Limited in the year 2007 .</p>
                                    <p>The first Little Elly pre-school was set-up in BTM Layout, Bengaluru in 2008.
                                        Today,
                                        the
                                        Little Elly group comprises of several franchisees and training centres that
                                        are
                                        spread all
                                        over the country. They provide individualized learning opportunities and well
                                        researched
                                        teaching methods to aid the overall development of a child.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Image-column -->
                        <div class="image-column col-lg-6 col-md-12 col-sm-12">
                            <div class="inner-column origin-inner">
                                <div class="image-box wow fadeInRight">
                                    <figure class="image-1"><img src="images/resource/origin-3.jpg" alt=""></figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End About Us -->

        </div>
    </div>
</section>

<!-- Chaiman Message Section -->
<section class="about-us" id="message-from-director">
    <div class="auto-container">
			<div class="sec-title">
					<h2>Director's <span>Message</span></h2>
				</div>
        <div class="row clearfix">
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2><span>Mr. Vittal Bhandary </span></h2>
                    </div>
                    <div class="text">
                        <p>
                            <strong>Vittal had a vision of nurturing great values in children through a conscious,
                                harmonious and fun-filled early childhood education system. With that discernment and
                                passion, he created the Little Elly chain of preschools.
                            </strong>
                        </p>
                        <p>
                            Vittal and his team have spent twenty five years of relentless focus and concerted effort
                            in developing a profound understanding of the dynamics and diverse aspects of early
                            childhood education and care. Leading an accomplished team of professionals, he has
                            developed a successful, recognized and respected brand and organization in this market
                            segment in India. The vast and ever-growing number of preschools and childcare environments
                            operating under the Little Elly flagship bear testament to this fact.
                        </p>
                        <p>
                            Vittal’s enterprising ideas, perseverance and steadfast belief in this concept, has led to
                            successful diversifications of the business into areas of children’s publications, creation
                            of learning aids and providing pedagogic support, early schooling, daycare and corporate
                            affiliate programs.
                        </p>
                    </div>

                </div>
            </div>

            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1 no-hover"><img src="images/resource/vital.jpg" alt=""></figure>
                        <figure class="image-1 on-hover"><img src="images/resource/vital-1.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="sec-title">
                    <h2><span> (Founder & Managing Director)</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="auto-container pad-top">
        <div class="row clearfix">
            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1 no-hover"><img src="images/resource/preeti.jpg" alt=""></figure>
                        <figure class="image-1 on-hover"><img src="images/resource/preeti-1.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="sec-title">
                    <h2><span> (Co-Founder & Curriculum Director)</span></h2>
                </div>
            </div>
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2><span>Mrs. Preeti Bhandary</span></h2>
                    </div>
                    <div class="text">
                        <p>
                            <strong>
                                Preeti is a veteran educationist and an entrepreneur. She has over 18 years of
                                experience in early childhood education and care. Her views on working with young minds
                                are radical, progressive and dynamic. Preeti considers each child as an individual.
                                She has over 18 years of experience in early childhood education and care. Her views on
                                working with young minds
                                are radical, progressive and dynamic.
                            </strong>
                        </p>
                        <p>
                            Preeti considers each child as an individual. She is passionate about unearthing and
                            cultivating a child’s potential through creative, stimulating and exclusive channels. She
                            has spent a large part of her career evolving unrivaled curriculums to foster the
                            individual needs of a child to mould him/her into an exclusive person. In this way, she has
                            guided the growth of thousands of children with distinguished determination and
                            effectuation. Her eminent standards of excellence have ensured that Little Elly is graded
                            among the best in pre-schools in India.
                        </p>
                        <p>
                            Preeti is also highly respected and regarded in the industry for being a patient and
                            encouraging mentor to those who share her insight of the vision, mission and philosophy of
                            Little Elly.
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Chaiman Message Section -->

<!-- Little Elly Ventures -->
<section class="about-us" id="little-elly-ventures">
    <div class="auto-container">
        <div class="row clearfix littleVenture" >
            <div class="inner-column">
                <div class="sec-title">
                    <h2><span>Learning Edge Ventures</span></h2>
                </div>
			</div>
		</div>
		<div class="row clearfix littleVenture" >
            <div class="features-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
					<a href="http://littleelly.com/" target="_blank">
                    <div class="course-features wow fadeIn">
                        <div class="title">
							<span class="icon">
								<img src="images/little_elly.png" alt="little elly" />
							</span> 
							<span>
								Little Elly
							</span>
						</div>
                        <div class="content">
                            <ul class="features-list">
                                <li>
                                    <strong>Initial Teacher’s Training (ITT) Program</strong>
                                </li>
                                <li>
										Little Elly is Bangalore’s favorite neighborhood preschool with 120 plus centers across Southern and Western India. It is passionate about preparing children for lifetime learning. The curriculum at Little Elly is based on Montessori and Steiner models and is a stimulating combination of academics and practical activities.
                                </li>
                            </ul>
                        </div>
					</div>
				</a>
                </div>
			</div>
			<div class="features-column col-lg-6 col-md-12 col-sm-12">
				<div class="inner-column">
					<a href="http://www.ellychildcare.com/" target="_blank">
						<div class="course-features wow fadeIn">
							<div class="title">
								<span class="icon">
									<img src="images/elly.png" alt="little elly" />
								</span> 
								<span>
										Elly Child Care (ECC)
								</span>
							</div>
							<div class="content">
								<ul class="features-list">
									<li>
										<strong>Elly Child Care (ECC) – Corporate Daycare</strong>
									</li>
									<li>
											offers working parents access to a convenient childcare solution without compromising on the quality of education and care. The program’s innovative curriculum allows children to learn through play in a safe and stimulating environment.
									</li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>
            <div class="features-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
					<a href="http://glentreeacademy.com/" target="_blank">
						<div class="course-features wow fadeIn">
							<div class="title">
								<span class="icon">
									<img src="images/glentree.png" alt="glentree" />
								</span>
								<span style="padding-top: 10px;">
									Glentree Academy
								</span>
							</div>
							<div class="content">
								<ul class="features-list">
									<li>
										<strong>Glentree Academy “Learning for Life”(K-12,
												CBSE School)</strong>
									</li>
									<li>
											Glentree Academy, the K-12 school is making a distinguished contribution to society. It focuses on making education relevant and structured for all children. Its dedicated team ensures that every child enjoys Learning For Life.
									</li>
								</ul>
							</div>
						</div>
					</a>
                </div>
            </div>
            <div class="features-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
					<a href="http://letter.org.in/" target="_blank">
						<div class="course-features wow fadeIn">
							<div class="title">
								<span class="icon">
									<img src="images/letter.png" alt="letter" />
								</span> 
								<span>
									Letter
								</span>
							</div>
							<div class="content">
								<ul class="features-list">
									<li>
										<strong>
												Letter
										</strong>
									</li>
									<li>
										The Teacher Training Program enhances teaching skills and provides good teaching staff to schools. LETT provides Training And Certification Programs for pre-primary and primary teachers. It also has Skill Enhancement Programs for aspiring and working teachers.
									</li>
								</ul>
							</div>
						</div>
					</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Little Elly Ventures -->

<!-- Awards Section -->
<section class="about-us" id="awards-recognition">
    <div class="auto-container">
        <div class="sec-title">
            <h2><span>Awards</span></h2>
        </div>
        <div class="row clearfix">
            <!-- History Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Indian Education Award 2015</a></h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Innovation In Early Learning/ Child Development</li>
                            </ul>
                            <div class="text">
                                The award recognizes early learning schools that have implemented programs/curriculum
                                to advance quality early educational opportunities for children.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Top 100 Franchise in India</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>2014 & 2015</li>
                            </ul>
                            <div class="text">
                                This award is to honor the achievements and the entrepreneurial spirit of Indian
                                franchisors.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Silicon India 2014</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Top 5 Most Promising Preschool In India</li>
                            </ul>
                            <div class="text">
                                This award is in recognition for exemplary contribution that has enhanced student
                                learning and achievement.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Indian Education Award 2014</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Best Preschool in South</li>
                            </ul>
                            <div class="text">
                                The award aims to recognize a regional pre-school chain for incorporating outstanding
                                quality in education delivery, childcare and hygiene.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>The Franchise World 2012 and 2013</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>Top 100 franchisee preschools in India</li>
                            </ul>
                            <div class="text">
                                The award aims to recognize achievers and innovators who have contributed significantly
                                towards the excellence and growth of the education sector.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column course-block">
                    <div class="inner-box">
                        <div class="lower-content">
                            <h3>Silicon India 2012</h3>
                            <ul class="info">
                                <li><i class="fa fa-calendar"></i>op 10 Preschools In India</li>
                            </ul>
                            <div class="text">
                                The award recognizes the most sought after preschool in India. It recognizes the
                                preschool that has incorporated outstanding education quality.
                            </div>
                        </div>
                        <div class="other-info clearfix">
                            <div class="rating">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Corporate Social Responsibility Section -->
<section class="about-us" id="corporate-social-responsibility">
    <div class="auto-container">
			<div class="sec-title">
					<h2>Corporate Social <span>Responsibility</span></h2>
				</div>
        <div class="row clearfix">
            <div>
                <h2 style="font-weight: bold; margin: 40px 10px 25px;"><span>Education is the most powerful weapon which you can use to change the world.</span></h2>
            </div>
            <div class="content-column accordion-column col-lg-12 col-md-12 col-sm-12 col-offset-5" style="margin-bottom: 50px;">
                <div>
                    <h2 class="nelson-title"><span>-</span>Nelson Mandela</h2>

                    <div class="text">
                        One of the most noteworthy values of social progress is education. This also plays a pivotal
                        role for a society to achieve competency and equal development.

                        Corporate social responsibility (CSR) is a form of corporate self-regulation integrated into
                        business models.

                        Little Elly leads its CSR initiative through the school project “Priyadarshini” in Bangalore.
                        The aim of this project is to improve the quality of education in rural schools in Bangalore.

                        The Little Elly corporate arranges teacher training and delivers curriculum for quality
                        enhancement of education in the targeted rural schools.

                        As part of the project, a curriculum based on textbook syllabus has been developed. The aim is
                        to help the vulnerable and disadvantaged children at these schools acquire fundamental skills
                        that are required for success in today’s world.
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>