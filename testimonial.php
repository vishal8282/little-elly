<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Testimonials</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Testimonials</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Toller Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="lower-content">
            <h3>Happy Kids Make Happy Parents</h3>

            <div class="text">
                <p>
                    Happy and satisfied parents are our biggest assets. They re-enforce our belief and the work that we
                    put in towards the all-round development of their children. It makes us happy when they vouch for
                    us and tell us that what we do is showing up in what their children speak and their overall
                    behavior. We as Little Elly staff, teachers and management feel really proud of our role in
                    nurturing the lives of little ones, and also for all the support, total love and trust that parents
                    have been extending to us, ever since the inception of this venture.
                </p>
            </div>
        </div>
        <div class="row clearfix facility-card">
            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Sneha Gupta
                    </h3>
                    <span>GE INDIA</span>
                    <div class="text">
                        I strongly recommend Little Elly, Mahadevapura, if you are looking for pre-school. Very clean,
                        ventilated place, good teachers, even the support staff always with a smile. It is very much
                        accessible since I work in ITPL and this is very close, it is on the way, easy to drop and pick
                        him. I can say my little son has grown in confidence because of them. He demands that I read
                        stories to him daily :) He sings a new rhyme every week. Good school overall, very happy.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Maanav
                    </h3>
                    <span>IBM</span>
                    <div class="text">
                        The school has excellent course curriculum. My daughter has picked up social skills, is very
                        confident and reads and writes well. The school has good means to communicate to parents at the
                        regular basis and keeps updated on child's progress. On the annual day, I had a chance to meet
                        other parents also who echoed the same sentiments. Thanks to school staff, I would recommend
                        this school to others as well.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Vishruthi's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        It was a great experience with Little Elly. Thanks for your step by step grooming of the kid.
                        The PTM was extraordinary experience. I have come to know many things which I never noticed in
                        my kid. Thank you.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Saachi's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        It was very nice experience with Little Elly. My child Saachi has improved and enjoyed a lot
                        here. We are going to miss Little Elly. Her performance in terms of language, confidence,
                        independence has changed drastically. At last but not least I would like to thank the teachers
                        for support & effort in teaching Saachi.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Dhruva's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        Really happy with the teaching and knowlwdge and transformation of Dhruva since the start day
                        till today. The teachers and the staff are really co-operative and sweet. Thank you for all the
                        coaching and help for transforming my son to the next growing bud.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Vedant's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        Extremely happy with the progress made by Vedant this year. His writing skills, life skills
                        have improved significantly. We are pleasantly surprised when he demonstrates his skills in
                        house. Overall extremely satisfied.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Suteja's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        We really enjoyed being a part of Little Elly. My son has learned a lot of activities and
                        really enjoys being at school with his teachers and friends. Thanks a lot to his teacher for
                        taking personal care and making him feel at home. Thank you to to the helper aunty also for
                        helping him to have his lunch.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Lekhana's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        As always, it is a great experience. We could see the differnece in our kid's behaviour,
                        language, vocabulary. Appreciate all of your efforts. Thank you all.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Aarav's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        It was really nice experience with Little Elly since the day I put Aarav here. I never had to
                        think about my kid. He was taken care very well. He has improved in different areas, writing,
                        speaking and extra activities. I wish the Elly staff and school all the best.
                    </div>
                </div>
            </div>

            <div class="service-block testimonial-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp animated">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="images/resource/icon-student.png" alt="">
                    </div>
                    <h3>
                        Arshith's Parents
                    </h3>
                    <span></span>
                    <div class="text">
                        We are quite happy with Arshith's progress at school. We feel his Ma'am has done wonderful job
                        as a teacher and we are thankful to her. The school admin and the support staff are excellent
                        in their work. We will be happy to continue Arshith's further education in this school.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>