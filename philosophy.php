<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Philosophy</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Philosophy</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <ul class="list-style-two">
                        <li>Little Elly is built around the strong belief that the childhood of a little one is
                            precious.</li>
                        <li>Research and in-depth studies have proven that the first five years of childhood are
                            the most formative.</li>
                        <li>Incredible learning and vital mental and cognitive growth occurs during this period.</li>
                        <li>Our programs are tailored to foster every child’s social, aesthetic and motor skills.</li>
                        <li>We develop age-appropriate activity based learning for the child’s all round
                            development.</li>
                    </ul>

                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/philosophy.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>