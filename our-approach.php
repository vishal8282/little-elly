<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Our Approach</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Our Approach</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Single Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="image-box">
            <div class="image wow fadeIn"><img src="images/resource/approach.jpg" alt=""></div>
           
        </div>
        <div class="lower-content material-bg">
            <h3>Early Learning Program Room</h3>
            <div class="text">
                <p>
                    <strong>
                        The Early Learning Program helps with language and literacy. It encourages the child to
                        :
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Communicate his/her experiences, ideas and feelings by speaking.
                    </li>
                    <li>
                        Listen with understanding, follow directions, conversations and stories.
                    </li>
                    <li>
                        Exhibit interest in reading.
                    </li>
                </ul>
            </div>
        </div>
        <div class="lower-content material-bg">
            <h3>Motor Skills Room</h3>
            <div class="text">
                <p>
                    <strong>
                        The motor skills room provides the child with opportunities to :
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Exhibit curiosity, creativity, self direction and persistence in learning situations.
                    </li>
                    <li>
                        Engage in activities that he/she selects or creates
                    </li>
                    <li>
                        Demonstrate self direction in use of materials
                    </li>
                    <li>
                        Make independent decisions about what materials to work with and will get and use the
                        materials he/she needs.
                    </li>
                </ul>
            </div>
        </div>
        <div class="lower-content material-bg">
            <h3>Art Corner and Stage Area</h3>
            <div class="text">
                <p>
                    <strong>
                        The art corner and stage area provide the child with opportunities to :
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Exhibit curiosity and explore functioning of materials.
                    </li>
                    <li>
                        Create (imagine, experiment, plan, make, evaluate, refine and present/exhibit) works
                        that express or represent experiences, ideas, feelings and fantasy using various media.
                    </li>
                    <li>
                        Represent fantasy and real-life experiences through pretend play.
                    </li>
                    <li>
                        Engage in musical and creative movement activities.
                    </li>
                    <li>
                        Describe or respond to his/her own creative work or that of others.
                    </li>
                </ul>
            </div>
        </div>
        <div class="lower-content material-bg">
            <h3>Classroom</h3>
            <div class="text">
                <p>
                    <strong>
                        The classroom discussions on various topics and themes help the child to :
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Express wonder, ask questions and seek answers about the natural world.
                    </li>
                    <li>
                        Recognize and solve problems through active exploration, including trial and error and
                        interacting with peers and adults.
                    </li>
                    <li>
                        Organize and express his/her understanding of common properties and attributes of
                        things.
                    </li>
                </ul>
            </div>
        </div>
        <div class="lower-content material-bg">
            <h3>Outdoor Play Area</h3>
            <div class="text">
                <p>
                    <strong>
                        The outdoor play area gives the child opportunities to :
                    </strong>
                </p>
                <ul class="list-style-two">
                    <li>
                        Engage in a wide variety of gross-motor activities that are selected by him/her and
                        initiated by the teacher.
                    </li>
                    <li>
                        Use a variety of materials that promote eye-hand coordination and small-muscle
                        development.
                    </li>
                    <li>
                        Demonstrate spatial awareness in gross-motor activities.
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End Course Single Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>