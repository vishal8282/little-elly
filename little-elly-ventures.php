<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>little-elly-ventures</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>About Us</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- About Us -->
<section class="about-us style-two">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span> Initial Teacher’s
                            Training</div>
                        <div class="content">
                            <ul class="features-list">
                                <li>
                                    <strong>Initial Teacher’s Training (ITT) Program</strong>
                                </li>
                                <li>
                                    Parents have high expectations from preschools in
                                    terms of compliance with the standards of teaching methods and delivery.
                                    But one of the major challenges faced by preschools in India is
                                    availability of quality teachers. Today, many preschools lack dedicated and
                                    trained teachers.
                                </li>
                                <li>
                                    The ITT curriculum is available in both classroom-coaching model as well as
                                    online. It covers:
                                </li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Different aspects of early childhood and the development there in</li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Models of education like the Montessori, Reggio Emilia, Steiner, Multiple
                                    Intelligence and the Kindergarten methods</li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Hands on training</li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Regular tests and assessments</li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Workshops on child psychology</li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Team building projects</li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Portfolio preparation</li>
                                <li>
                                    <i class="fa fa-trophy"></i>
                                    Spoken English module</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span> Glentree Academy </div>
                        <div class="content">
                            <ul class="features-list">
                                <li>
                                    <strong>Elly Child Care (ECC) – Corporate Daycare</strong>
                                </li>
                                <li>
                                    With the huge success of the Little Elly chain of
                                    preschools in India, it was the most natural decision to launch a K – 12
                                    school to enhance the value proposition and advance the vision. Glentree
                                    Academy operates on the philosophy of “Learning for Life”. The school
                                    distinguishes itself as a socially responsible institution. Through its
                                    core value system, it provides practical life skills that are integrated
                                    with academic learning. Here the child’s aptitude is built through intimate
                                    connections with nature. He/she happily acquires conventional competence
                                    whilst playing in a setting that encourages uninhibited and limitless
                                    ideas. Glentree Academy’s commitment is to set an example of sustainability
                                    and environmental awareness through its curriculum, recycling programs and
                                    purchasing decisions.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span> Elly Child Care (ECC)</div>
                        <div class="content">
                            <ul class="features-list">
                                <li>
                                    <strong>Glentree Academy “Learning for Life”(K-12,
                                        CBSE School)</strong>
                                </li>
                                <li>
                                    Today, corporates are doing all they can to
                                    empower their women workforce. With a focus on inclusion of women at
                                    workplace and helping them maintain a healthy work-life balance, many
                                    companies are considering options of providing corporate daycare for the
                                    children of their employees. Elly Child Care offers customized solutions
                                    for the employees. It ensures that the little one is in safe and
                                    professional care while the parents are away at work. The centres have a
                                    unique approach and an integrated curriculum to mentor meaningful,
                                    experiential and creative learning for the child.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End About Us -->

<!-- Call To Action -->
<section class="call-to-action">
    <div class="anim-icons style-two">
        <span class="icon icon-cloud wow shake"></span>
        <span class="icon butterfly-1 wow rotateIn"></span>
    </div>

    <div class="auto-container">
        <div class="outer-box clearfix">
            <div class="title-column">
                <h3>Welcome to Arans Kinder Garten School</h3>
                <h2>Best For Education</h2>
            </div>

            <div class="btn-column">
                <div class="btn-box">
                    <a href="contact.html" class="theme-btn btn-style-one">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action -->

<!-- Main Footer -->
<?php include 'footer.php';?>