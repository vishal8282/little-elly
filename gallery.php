<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Gallery</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Gallery</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!--Gallery Section-->
<section class="gallery-section alternate">
    <div class="auto-container">
        <!--Galery-->
        <div class="sortable-masonry">
            <!--Filter-->
            <div class="filters clearfix">
                <ul class="filter-tabs filter-btns clearfix">
                    <li class="active filter" data-role="button" data-filter=".all">All</li>
                    <li class="filter" data-role="button" data-filter=".education">Education</li>
                    <li class="filter" data-role="button" data-filter=".game">Game</li>
                    <li class="filter" data-role="button" data-filter=".design">Design</li>
                    <li class="filter" data-role="button" data-filter=".drawing">Drawing</li>
                    <li class="filter" data-role="button" data-filter=".book">Book</li>
                </ul>
            </div>

            <div class="items-container row clearfix">
                <!--Project Block Two-->
                <div class="project-block masonry-item game drawing book design all col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/1.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/1.jpg" style="background-image: url(images/gallery/1.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>

                <!--Project Block Two-->
                <div class="project-block masonry-item game book education drawing all col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/2.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/2.jpg" style="background-image: url(images/gallery/2.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>

                <!--Project Block Two-->
                <div class="project-block masonry-item drawing design game all col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/3.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/3.jpg" style="background-image: url(images/gallery/3.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>

                <!--Project Block Two-->
                <div class="project-block masonry-item book game education all col-lg-8 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/4.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/4.jpg" style="background-image: url(images/gallery/4.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>

                <!--Project Block Two-->
                <div class="project-block masonry-item book game education all col-lg-8 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/5.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/5.jpg" style="background-image: url(images/gallery/5.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>

                <!--Project Block Two-->
                <div class="project-block masonry-item book education all col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/gallery/6.jpg" alt="" />
                            <div class="overlay-box"><a href="images/gallery/6.jpg" style="background-image: url(images/gallery/6.jpg);"
                                    class="lightbox-image" data-fancybox="gallery"><span class="icon icon-plus"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Gallery Section-->

<!-- Main Footer -->
<?php include 'footer.php';?>