<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Toddler</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Toddler</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Toller Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="image-box">
            <div class="image wow fadeIn"><img src="images/resource/course-1.jpg" alt=""></div>
        </div>
        <div class="lower-content">
            <h3>The Toddler program is suitable for children of 1+ years</h3>
            
            <div class="text">
                <p>
                    Little Elly’s toddler program is designed to help the child transition from home to school. It
                    assists the child in getting accustomed to a structured, stimulating environment of learning and
                    play. This program is the foundation that understands the child. It builds on his/her skill sets
                    and works towards crafting play and learning that will enhance his/her growth.
                </p>

                <p>
                    At this age, the child relies on his/her sensory grasp of the immediate surroundings. The toddler
                    program is designed around weekly and monthly themes based on real sensory experiences that the
                    child can identify, relate with and understand. The teachers take the child through a journey of
                    singing, dancing, play and body movements; thereby inducing learning within the familiar
                    environment of play.
                </p>

                <p>
                    The themes develop the child’s cognitive functions, grasping power, motor and social skills and
                    builds his/her confidence and self-esteem. Extra-curricular activities such as sing-alongs, art and
                    craft, plays and puppet shows, ensure that the child explores his/her creativity, too.
                </p>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span> Our Approach</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Teaching of good habits and toilet training</li>
                                <li><i class="fa fa-angle-left"></i>Focused, thematic activities for fun learning and development of key skills</li>
                                <li><i class="fa fa-angle-left"></i>Using creativity and play to grow within the school environment</li>
                                <li><i class="fa fa-angle-left"></i>Developing cognitive skills and grasping power</li>
                                <li><i class="fa fa-angle-left"></i>Making them independent</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Key areas covered in the toddler curriculum</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Learning with art, music and play</li>
                                <li><i class="fa fa-angle-left"></i>Building communication and social skills</li>
                                <li><i class="fa fa-angle-left"></i>Encouraging positive peer interaction</li>
                                <li><i class="fa fa-angle-left"></i>Sensorial and cognitive development</li>
                                <li><i class="fa fa-angle-left"></i>Developing Language Skills</li>
                                <li><i class="fa fa-angle-left"></i>Stimulating fine and gross motor co-ordination skills</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Activities Involved</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Art, music and play activities</li>
                                <li><i class="fa fa-angle-left"></i>Sand and water play</li>
                                <li><i class="fa fa-angle-left"></i>Sensorial and cognitive activities</li>
                                <li><i class="fa fa-angle-left"></i>Festivals and special day celebrations</li>
                                <li><i class="fa fa-angle-left"></i>Field trips to the neighbour’s and/or friend’s
                                    home, park etc.</li>
                                <li><i class="fa fa-angle-left"></i>Puppet shows and skits</li>
                                <li><i class="fa fa-angle-left"></i>Free play with blocks and toys</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>