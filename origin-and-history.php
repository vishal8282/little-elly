<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Origin And History</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Origin and History</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- About Us -->
<section class="about-us style-two">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column origin-inner">
                    <div class="text">
                        <p>
                            <strong>
                                It is the aim of every parents to find the right school for their child, one that
                                takes care of his learning, equips him with life skills as well as helps in his overall
                                development.
                            </strong>
                        </p>
                        <p>Several years ago, it was this same desire that propelled Mrs. Preeti Bhandary to look
                            around her neighbourhood for an “outstanding pre-school” to enrol her child in. She came
                            across many good schools, each offering something unique to the child. Yet, what was
                            missing was a good school that gave her child EVERYTHING that the little one needed for her
                            all round growth. As with any parent, she wanted nothing but the best for her child. Her
                            aspiration to fulfil that dream led her to initiate one of the most respected group of
                            institutions in India, a pioneer in early childhood education and care – the Little Elly
                            chain of pre-schools.</p>
                    </div>
                </div>
            </div>

            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column origin-inner">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/origin-1.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End About Us -->

<!-- About Us -->
<section class="about-us style-two">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column origin-inner">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/origin-2.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column origin-inner">
                    <div class="text">
                        <p>
                            <strong>
                                With the steadfast support of her husband, Mr. Vittal Bhandary, Preeti’s endeavour
                                began to take shape. Combining the rich experience of her career as a kindergarten
                                teacher with her love for teaching in innovative and unique ways, she set out to start
                                “Salmiya Playschool”, a pre-school in Kuwait in 2000. From there on, there was no
                                looking back.
                            </strong>
                        </p>
                        <p>
                            The following years of learning, research and experience were invaluable. Enriching her
                            theoretical knowledge and practical exposure gave her a real-life understanding of the best
                            practises in the field of early childhood education and care.
                        </p>
                        <p>
                            Having amassed many years of skill in nurturing the individual needs of a child, her next
                            calling was to pass on that know-how to a wider audience. She designed a superior
                            educational program for the holistic development for children, mentored dedicated faculty
                            and set high standards of excellence for adherence to children’s needs. This was the
                            genesis of Little Elly – The Concept pre-school.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End About Us -->

<!-- About Us -->
<section class="about-us style-two">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column origin-inner">
                    <div class="text">
                        <p>
                            <strong>
                                Having amassed many years of skill in nurturing the individual needs of a child, her
                                next calling was to pass on that know-how to a wider audience. She designed a superior
                                educational program for the holistic development for children, mentored dedicated
                                faculty and set high standards of excellence for adherence to children’s needs. This
                                was the genesis of Little Elly – The Concept pre-school.
                            </strong>
                        </p>
                        <p>In August 2005, Glentree UK collaborated with them to provide technical and curriculum
                            inputs and support.</p>
                        <p>To provide a greater thrust in reaching out to the community, the founders formed a new
                            company M/s Learning Edge India Private Limited. It took over the preschool business from
                            M/s Global Learning System Private Limited in the year 2007 .</p>
                        <p>The first Little Elly pre-school was set-up in BTM Layout, Bengaluru in 2008. Today, the
                            Little Elly group comprises of several franchisees and training centres that are spread all
                            over the country. They provide individualized learning opportunities and well researched
                            teaching methods to aid the overall development of a child.</p>
                    </div>
                </div>
            </div>

            <!-- Image-column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column origin-inner">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/origin-3.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End About Us -->

<!-- Call To Action -->
<section class="call-to-action">
    <div class="anim-icons style-two">
        <span class="icon icon-cloud wow shake"></span>
        <span class="icon butterfly-1 wow rotateIn"></span>
    </div>

    <div class="auto-container">
        <div class="outer-box clearfix">
            <div class="title-column">
                <h3>Take Your First Step For Your Little One's</h3>
                <h2>Know more about Little Elly</h2>
            </div>

            <div class="btn-column">
                <div class="btn-box">
                    <a href="contact.php" class="theme-btn btn-style-one">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action -->

<!-- Main Footer -->
<?php include 'footer.php';?>