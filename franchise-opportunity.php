<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Franchise Opportunity</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Franchise Opportunity</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2>Franchise <span>Opportunity</span></h2>
                        <h5 style="font-size: 16px;">Start your own Pre-School with Little Elly & Open
                            the World of Joy</h5>
                    </div>
                    <div class="text">
                        The pre-school segment is the newest but a popular and flourishing entry
                        into the education industry of India. With increased awareness and affordability
                        amongst parents, the demand of good pre-schools is on a steep rise. The new-age parents
                        acknowledge the rise in competition in education and that prompts them to plan their
                        child’s future from early years on. There is also a growing global agreement that
                        preschool education is vital to a country's sound education system.
                    </div>
                    <div class="text">
                        Here is your chance to become part of one of the leading preschool chains
                        in India, Little Elly. Little Elly as a brand stands for loyalty, trust and faith and
                        helps drive performance while never compromising in the response to competitive
                        changes. As a Little Elly owner you will have a rewarding and challenging career where
                        you’ll see the results of your caring and creative efforts every day.
                    </div>
                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/franchise.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column franchise-accordion col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <!--Accordian Box-->
                    <ul class="accordion-box">
                        <!--Block-->
                        <li class="accordion block active-block">
                            <div class="acc-btn active">
                                <p>Why to start School?</p></div>
                            <div class="acc-content current">
                                <div class="content">
                                    <ul class="list-style-two">
                                        <li>
                                            A positive and one of the best working environment for you to work, with
                                            comfort.
                                        </li>
                                        <li>
                                            Not impacted by business cycles.
                                        </li>
                                        <li>
                                            All cash transactions, No credit.
                                        </li>
                                        <li>
                                            No stress of unsold inventory and wastage.
                                        </li>
                                        <li>
                                            Prime commercial location not required.
                                        </li>
                                        <li>
                                            Easy work timings.
                                        </li>
                                        <li>
                                            Holidays as per your children's holidays.
                                        </li>
                                        <li>
                                            Respect from the community.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="histroy-column franchise-accordion col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <!--Accordian Box-->
                    <ul class="accordion-box">
                        <!--Block-->
                        <li class="accordion block">
                            <div class="acc-btn active">
                                <p>Advantages with Little Elly Franchise</p>
                            </div>
                            <div class="acc-content current">
                                <div class="content">
                                    <ul class="list-style-two">
                                        <li>
                                            Fastest & most admired growing chain of preschool.
                                        </li>
                                        <li>
                                            Dedicated team to grow your center.
                                        </li>
                                        <li>
                                            Multiple franchise is not given in a particular area.
                                        </li>
                                        <li>
                                            150+ centers across India & Nominal Franchise fee with best support
                                        </li>
                                        <li>
                                            Unique innovative, international curriculum & affiliate program.
                                        </li>
                                        <li>
                                            Course materials, syllabus, support, teacher training & support
                                            program.
                                        </li>
                                        <li>
                                            Excellent Return on Investment(ROI), happiness & Joy with Little Elly.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Page Title -->

<!--End Contact Form Section -->
<section class="about-us style-two history-section pad-0">
    <div class="auto-container">
        <div class="event-table franchise-table">
            <div class="title-box">Why franchising with Little Elly?</div>
            <!-- Event Block -->
            <div class="event-block wow fadeInUp">
                <div class="inner-box clearfix">

                    <div class="image-column">
                        <div class="image"><img src="images/strategies.png" alt=""></div>
                    </div>
                    <div class="day-column">
                        <h3>The latest strategies</h3>
                    </div>
                    <div class="info-column">
                        <h4>The latest strategies, tools, and techniques that use the Montessori and Rudolf Steiner
                            methods</h4>
                    </div>
                </div>
            </div>
            <!-- Event Block -->
            <div class="event-block wow fadeInUp">
                <div class="inner-box clearfix">

                    <div class="image-column">
                        <div class="image"><img src="images/preschool.png" alt=""></div>
                    </div>
                    <div class="day-column">
                        <h3>India's leading pre-schools</h3>
                    </div>
                    <div class="info-column">
                        <h4>Ongoing support from Little Elly, India's leading group of pre-schools. know their little
                            World.</h4>
                    </div>
                </div>
            </div>
            <!-- Event Block -->
            <div class="event-block wow fadeInUp">
                <div class="inner-box clearfix">

                    <div class="image-column">
                        <div class="image"><img src="images/consulting.png" alt=""></div>
                    </div>
                    <div class="day-column">
                        <h3>Guidance and consulting</h3>
                    </div>
                    <div class="info-column">
                        <h4>Guidance and consulting when setting up your new preschool business.</h4>
                    </div>
                </div>
            </div>
            <!-- Event Block -->
            <div class="event-block wow fadeInUp">
                <div class="inner-box clearfix">

                    <div class="image-column">
                        <div class="image"><img src="images/our-support.png" alt=""></div>
                    </div>
                    <div class="day-column">
                        <h3>Our Support for you</h3>
                    </div>
                    <div class="info-column">
                        <h4>Pre – Launch Support, Setting up Support, Post launch support, Training support, Ongoing
                            support.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Contact Form Section -->
<section class="contact-form-section">
    <div class="auto-container">
        <div class="sec-title text-center light">
            <h2>Take The <span>First Step</span></h2>

            <div class="text">Start your own Pre-School & Play School with Little Elly & Open the World of Joy</div>
        </div>

        <!-- Contact Form -->
        <div class="contact-form">
            <form method="post" action="sendemail.php" id="contact-form">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-xs-12 form-group">
                        <input type="text" name="username" placeholder="Name" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <input type="email" name="email" placeholder="Email" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <input type="text" name="phone" placeholder="Phone" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <input type="text" name="subject" placeholder="Your Preferred Location" required>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <textarea name="message" placeholder="Your Comments"></textarea>
                    </div>

                    <div class="col-lg-12 col-md-6 col-sm-12 form-group text-center">
                        <button class="theme-btn btn-style-two" type="submit" name="submit-form">Submit Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End History Section -->


<!-- Main Footer -->
<?php include 'footer.php';?>