<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Play School in Bangalore, Top Preschools, Nursery, Kindergarten – Little Elly</title>
    <!-- Stylesheets -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="plugins/revolution/css/settings.css" rel="stylesheet" type="text/css">
    <link href="plugins/revolution/css/layers.css" rel="stylesheet" type="text/css">
    <link href="plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>

<body>
    <div class="page-wrapper">
        <!-- Preloader -->
        <div class="preloader"></div>

        <!-- Main Header-->
        <header class="main-header">
            <!--Header Top-->
            <div class="header-top">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <div class="top-left">
                            <ul class="contact-list clearfix">
                                <li><a href="#">help@littleelly.com</a></li>
                                <li>Call Us : 7676 38 0000</li>
                            </ul>
                        </div>
                        <!--<div class="top-right clearfix">-->
                        <!--    <ul class="clearfix">-->
                        <!--        <li>Language : </li>-->
                        <!--        <li class="language dropdownn"><a class="btn btn-default dropdown-toggle" id="dropdownMenu1"-->
                        <!--                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">Eng-->
                        <!--                <span class="icon fa fa-caret-down"></span></a>-->
                        <!--            <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu1">-->
                        <!--                <li><a href="#">Arabic</a></li>-->
                        <!--                <li><a href="#">Chinese</a></li>-->
                        <!--                <li><a href="#">German</a></li>-->
                        <!--                <li><a href="#">French</a></li>-->
                        <!--            </ul>-->
                        <!--        </li>-->
                        <!--    </ul>-->
                        <!--</div>-->
                    </div>
                </div>
            </div>
            <!-- End Header Top -->

            <!-- Header Upper -->
            <div class="header-upper">
                <div class="auto-container">
                    <div class="clearfix">

                        <div class="logo-outer">
                            <div class="logo"><a href="index.php"><img src="images/logo.png" alt="" title=""></a></div>
                        </div>

                        <div class="upper-right clearfix">
                            <!--<form method="post" action="blog.php">-->
                            <!--    <div class="form-group header-search">-->
                            <!--        <label for="search">-->
                            <!--            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"-->
                            <!--                viewBox="0 0 48 48" version="1.1" width="48px" height="48px">-->
                            <!--                <g id="surface1">-->
                            <!--                    <path style=" fill:#FFC107;" d="M 43.609375 20.082031 L 42 20.082031 L 42 20 L 24 20 L 24 28 L 35.304688 28 C 33.652344 32.65625 29.222656 36 24 36 C 17.371094 36 12 30.628906 12 24 C 12 17.371094 17.371094 12 24 12 C 27.058594 12 29.84375 13.152344 31.960938 15.039063 L 37.617188 9.382813 C 34.046875 6.054688 29.269531 4 24 4 C 12.953125 4 4 12.953125 4 24 C 4 35.046875 12.953125 44 24 44 C 35.046875 44 44 35.046875 44 24 C 44 22.660156 43.863281 21.351563 43.609375 20.082031 Z " />-->
                            <!--                    <path style=" fill:#FF3D00;" d="M 6.304688 14.691406 L 12.878906 19.511719 C 14.65625 15.109375 18.960938 12 24 12 C 27.058594 12 29.84375 13.152344 31.960938 15.039063 L 37.617188 9.382813 C 34.046875 6.054688 29.269531 4 24 4 C 16.316406 4 9.65625 8.335938 6.304688 14.691406 Z " />-->
                            <!--                    <path style=" fill:#4CAF50;" d="M 24 44 C 29.164063 44 33.859375 42.023438 37.410156 38.808594 L 31.21875 33.570313 C 29.210938 35.089844 26.714844 36 24 36 C 18.796875 36 14.382813 32.683594 12.71875 28.054688 L 6.195313 33.078125 C 9.503906 39.554688 16.226563 44 24 44 Z " />-->
                            <!--                    <path style=" fill:#1976D2;" d="M 43.609375 20.082031 L 42 20.082031 L 42 20 L 24 20 L 24 28 L 35.304688 28 C 34.511719 30.238281 33.070313 32.164063 31.214844 33.570313 C 31.21875 33.570313 31.21875 33.570313 31.21875 33.570313 L 37.410156 38.808594 C 36.972656 39.203125 44 34 44 24 C 44 22.660156 43.863281 21.351563 43.609375 20.082031 Z " />-->
                            <!--                </g>-->
                            <!--            </svg>-->
                            <!--        </label>-->
                            <!--        <input type="search" id="search" name="search-field" value="" placeholder="Little Elly Near Me"-->
                            <!--            required="">-->
                            <!--        <button type="submit"><span class="icon flaticon-magnifying-glass"></span></button>-->
                            <!--    </div>-->
                            <!--</form>-->
                            <ul>
                                <li>
                                    <button class="theme-btn btn-style-one">
                                        Our Little Elly Ventures
                                    </button>
                                </li>
                                <li>
                                    <button class="theme-btn btn-style-one">
                                        ADMISSION INQUIRY
                                    </button>
                                </li>
                                <li>
                                    <a href="franchise-opportunity" class="theme-btn btn-style-one">
                                        FRANCHISE INQUIRY
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header-lower-->
            <div class="header-lower">
                <div class="auto-container">
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-md navbar-dark">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="flaticon-menu"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="current dropdown">
                                        <a href="about">About us</a>
                                        <ul>
                                            <li>
                                                <a href="about#philosophy">Philosophy</a>
                                            </li>
                                            <li>
                                                <a href="about#our-vision-and-mission">Our Vision &#038; Mission</a>
                                            </li>
                                            <li>
                                                <a href="about#origin-and-history">Origin &#038; History</a>
                                            </li>
                                            <li>
                                                <a href="about#message-from-director">Directors&#8217; Profile</a>
                                            </li>
                                            <li>
                                                <a href="about#little-elly-ventures">Little Elly Ventures</a>
                                            </li>
                                            <li>
                                                <a href="about#awards-recognition">Awards</a>
                                            </li>
                                            <li>
                                                <a href="about#corporate-social-responsibility">Corporate Social
                                                    Responsibility</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#">The School</a>
                                        <ul>
                                            <li>
                                                <a href="programs">Programs</a>
                                            </li>
                                            <li>
                                                <a href="curriculum">Curriculum Framework</a>
                                            </li>
                                            
                                            <li>
                                                <a href="testimonial">Testimonials</a>
                                            </li>
                                        
                                            <li>
                                                <a href="child-behaviour-policy">Child Behaviour Policy</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="our-approach">Our Approach</a> </li>
                                    <li><a href="gallery">Gallery</a></li>
                                    <li><a href="#">The Media</a></li>
                                    <li class="dropdown">
                                        <a href="contact">Contact Us</a>
                                        <ul>
                                            <li>
                                                <a href="">India</a>
                                            </li>
                                            <li>
                                                <a href="">International</a>
                                            </li>
                                            <li>
                                                <a href="">locate-center</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        <!-- Outer Box -->
                        <div class="outer-box">
                            <ul class="social-icon-one">
                                <li><a href="https://www.facebook.com/littleellypreschool"><i class="fa fa-facebook-official"></i></a></li>
                                <li><a href="https://plus.google.com/+Littleellyindia"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="https://twitter.com/le_littleelly"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://www.youtube.com/user/PreschoolLittleElly?sub_confirmation=1"><i
                                            class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Header Upper-->
            <!-- Sticky Header -->
            <div class="sticky-header">
                <div class="auto-container clearfix">
                    <!--Logo-->
                    <div class="logo pull-left">
                        <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                    </div>
                    <!--Right Col-->
                    <div class="pull-right">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-collapse show collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li class="current dropdown">
                                        <a href="about">About us</a>
                                        <ul>
                                            <li>
                                                <a href="about#philosophy">Philosophy</a>
                                            </li>
                                            <li>
                                                <a href="about#our-vision-and-mission">Our Vision &#038; Mission</a>
                                            </li>
                                            <li>
                                                <a href="about#origin-and-history">Origin &#038; History</a>
                                            </li>
                                            <li>
                                                <a href="about#message-from-director">Directors&#8217; Profile</a>
                                            </li>
                                            <li>
                                                <a href="about#little-elly-ventures">Little Elly Ventures</a>
                                            </li>
                                            <li>
                                                <a href="about#awards-recognition">Awards</a>
                                            </li>
                                            <li>
                                                <a href="about#corporate-social-responsibility">Corporate Social
                                                    Responsibility</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#">The School</a>
                                        <ul>
                                            <li>
                                                <a href="programs">Programs</a>
                                            </li>
                                            <li>
                                                <a href="curriculum">Curriculum Framework</a>
                                            </li>
                                            <li>
                                                <a href="facilities-faculty">Facilities &#038; Faculty</a>
                                            </li>
                                            <li>
                                                <a href="testimonial">Testimonials</a>
                                            </li>
                                            <li>
                                                <a href="assessment">Assessment</a>
                                            </li>
                                            <li>
                                                <a href="child-behaviour-policy">Child Behaviour Policy</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="our-approach">Our Approach</a> </li>
                                    <li><a href="gallery">Gallery</a></li>
                                    <li><a href="#">The Media</a></li>
                                    <li class="dropdown">
                                        <a href="contact">Contact Us</a>
                                        <ul>
                                            <li>
                                                <a href="">India</a>
                                            </li>
                                            <li>
                                                <a href="">International</a>
                                            </li>
                                            <li>
                                                <a href="">locate-center</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav><!-- Main Menu End-->
                    </div>
                </div>
            </div>
        </header>
        <!--End Main Header -->