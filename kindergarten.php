<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Kindergarten</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Kindergarten</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Course Toller Section -->
<section class="course-single">
    <div class="anim-icons">
        <span class="icon icon-flower wow zoomIn"></span>
        <span class="icon icon-flower-2 wow zoomIn"></span>
        <span class="icon icon-flower-3 wow zoomIn"></span>
    </div>

    <div class="auto-container">
        <div class="image-box">
            <div class="image wow fadeIn"><img src="images/resource/course-3.jpg" alt=""></div>
        </div>
        <div class="lower-content">
            <h3>The Kindergarten program is suitable for children of 3-5 years.</h3>

            <div class="text">
                <p>
                    Little Elly’s kindergarten is a program to prepare the child for primary school education. It
                    facilitates the smooth transition from pre-school to a structured learning environment and formal
                    school.
                </p>
                <p>
                    It is an extension of the learning and assimilation of information gained by the child in the
                    preceding two years in nursery and playgroup. The two kindergarten years focus on enhanced writing
                    and reading skills, simple and complex everyday maths skills, science, social and extra-curricular
                    activities.
                </p>
                <p>
                    Little Elly’s LKG program focuses on the child’s social skills. It builds his/her ability to
                    interact and deal with peers, teachers and others around him/her. Curriculum activities are centred
                    on monthly themes and provide an organised atmosphere for learning. The encouraging and nurturing
                    environment promotes confidence and independence in the child.
                </p>
                <p>
                    Little Elly’s UKG program is a comprehensive, focused and skill-based program. Specific activities
                    are tailored towards the individual child’s skill sets. Working in a group, sharing ideas,
                    listening, interacting with peers and teachers form the basis of the curriculum. Continuous
                    development of language skills and learning makes this program the perfect way to groom the little
                    one for his/her big debut into full-time formal schooling.
                </p>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span> Our Approach</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Focused, theme based activities and development of
                                    key academic skills</li>
                                <li><i class="fa fa-angle-left"></i>Developing social skills, interaction with peers
                                    and teachers using free play</li>
                                <li><i class="fa fa-angle-left"></i>Enhancing reading and writing skills</li>
                                <li><i class="fa fa-angle-left"></i>Advanced curriculum for smooth transition to a
                                    structured learning and formal school program</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Key areas covered in the
                            Kindergarten curriculum</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Reading and writing</li>
                                <li><i class="fa fa-angle-left"></i>Optimizing language skills</li>
                                <li><i class="fa fa-angle-left"></i>Grasping complex language skills</li>
                                <li><i class="fa fa-angle-left"></i>Understanding of mathematical concepts</li>
                                <li><i class="fa fa-angle-left"></i>Cognitive development</li>
                                <li><i class="fa fa-angle-left"></i>Scientific literacy and focus on creative arts</li>
                                <li><i class="fa fa-angle-left"></i>Learning with art, music and play</li>
                                <li><i class="fa fa-angle-left"></i>Encouraging sports and extra-curricular activities</li>
                                <li><i class="fa fa-angle-left"></i>Building team management skills</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="course-features wow fadeIn">
                        <div class="title"><span class="icon flaticon-notebook"></span>Activities Involved</div>
                        <div class="content">
                            <ul class="features-list">
                                <li><i class="fa fa-angle-left"></i>Science and Social Sciences</li>
                                <li><i class="fa fa-angle-left"></i>Art, music and play</li>
                                <li><i class="fa fa-angle-left"></i>Drama, puppet shows and skits</li>
                                <li><i class="fa fa-angle-left"></i>Dance and body movement</li>
                                <li><i class="fa fa-angle-left"></i>Festivals and special day celebrations</li>
                                <li><i class="fa fa-angle-left"></i>Field trips to the zoo, vegetable market,
                                    planetarium etc.</li>
                                <li><i class="fa fa-angle-left"></i>Public speaking</li>
                                <li><i class="fa fa-angle-left"></i>Free play with technology tools</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>