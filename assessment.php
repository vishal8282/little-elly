<?php include 'header.php';?>

<!--Page Title-->
<section class="page-title">
    <div class="auto-container">
        <h1>Assessment</h1>
        <ul class="page-breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li>Assessment</li>
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- History Section -->
<section class="about-us style-two history-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- History Column -->
            <div class="histroy-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2>Assessment is defined as the process of observing, recording and documenting what
                            your child does, <span>knows and understands.</span></h2>
                    </div>
                    <ul class="list-style-two">
                        <li>Throughout the year, we oversee the child and document his/her progress. Our
                            knowledge of your child helps us to plan an appropriate and challenging curriculum
                            that is tailored to meet his/her strengths, needs and interests.</li>
                        <li>Under the direction and supervision of the centre head, all staff at the preschool
                            are involved in the assessment of children. They receive on-going training and have
                            access to teaching resources to support their understanding.</li>
                        <li>Progress is observed and documented in the following developmental and academic
                            content areas: Cognitive, Language, Social, Emotional, Physical, Literacy, Math,
                            Science, Social Studies, Art, Music and Dance.</li>
                        <li>The children are assessed three times a year, in August, December and March.
                            Assessments are based on utilization of teaching strategies that include on-going
                            observations and samples of classroom work. Kindergarten students go through
                            informal and stress-free tests for assessments.</li>
                        <li>Parent-teacher conferences are typically conducted at least thrice during the
                            school year to encourage parental involvement and discuss your child’s current
                            performance and progress.</li>
                        <li>All gathered information is kept confidential.</li>
                    </ul>

                </div>
            </div>

            <!-- Images Column -->
            <div class="accordion-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/assessment-1.jpg" alt=""></figure>
                    </div>
                                        <div class="image-box wow fadeInRight">
                        <figure class="image-1"><img src="images/resource/assessment.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End History Section -->

<!-- Main Footer -->
<?php include 'footer.php';?>